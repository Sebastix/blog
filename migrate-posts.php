<?php

error_reporting(E_ALL);

require '../../application/vendor/autoload.php';

use League\HTMLToMarkdown\HtmlConverter;

try {
  // Connect to database
  $db = mysqli_connect('sebastix_db', 'root', 'root', 'sebastix_blog', 3306);
  if ($db->connect_error) {
    throw new Exception($db->connect_error);
  }
  //
  // Select data from wp_posts table (io.sebastix.nl)
  //
  $query = 'SELECT * FROM `wp_posts` WHERE post_status = "publish"';
  $stmt = $db->prepare($query);
  $stmt->execute();
  $result = $stmt->get_result();
  $data = $result->fetch_all(MYSQLI_ASSOC);
  foreach($data as $i => $row) {
    //echo format_post($data[$i]);
    //echo '<hr />';
    $markdown_post_string = format_post($data[$i]);
    // create directory in development/content/blog with name $data[$i]['post_name']
    $targetDir = 'development/content/blog/io/' . $data[$i]['post_name'];
    if (!mkdir($concurrentDirectory = $targetDir) && !is_dir($concurrentDirectory)) {
      throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
    }
    // write file index.md with $markdown_post_string
    file_put_contents($targetDir.'/index.md', $markdown_post_string);
  }

  //
  // Select data from ioblog_posts table (interactieontwerpen.sebastix.nl)
  //
  $query = 'SELECT * FROM `ioblog_posts` WHERE post_status = "publish"';
  $stmt = $db->prepare($query);
  $stmt->execute();
  $result = $stmt->get_result();
  $data = $result->fetch_all(MYSQLI_ASSOC);
  foreach($data as $i => $row) {
    $markdown_post_string = format_post($data[$i]);
    // create directory in development/content/blog with name $data[$i]['post_name']
    $targetDir = 'development/content/blog/interactieontwerpen/' . $data[$i]['post_name'];
    if (!mkdir($concurrentDirectory = $targetDir) && !is_dir($concurrentDirectory)) {
      throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
    }
    // write file index.md with $markdown_post_string
    file_put_contents($targetDir.'/index.md', $markdown_post_string);
  }
} catch (Exception $e) {
  echo $e->getMessage();
}

/**
 * @param $data
 * @return string|string[]
 * @throws Exception
 */
function format_post($data){
  $markdown_template =
'---
title: "%post_title"
date: "%post_date"
description: "%description"
categories: [internet]
comments: true
---

%post_content
';
  // Format date from 2011-09-21 18:28:29 to 2011-09-21T18:28:29.000Z
  $post_date = new DateTime($data['post_date']);
  $post_date = $post_date->format('Y-m-d\TH:i:s.000\Z');
  /*
   * Post_content rewrites from HTML to Markdown
   * https://github.com/thephpleague/html-to-markdown gebruiken
   * Links
   * Images
   * remove: <!--more--> strings
   */
  // Replace newlines with <br />
  $data['post_content'] = nl2br($data['post_content']);
  $converter = new HtmlConverter();
  // Will strip all HTML tags which have no Markdown tag, such as <span> of <div> tags
  // $converter->getConfig()->setOption('strip_tags', true);
  $markdown_content = $converter->convert($data['post_content']);
  // Replace <img> url starting with http://www.sebastix.nl/blog/wp-content/uploads/ to ../../../images/
  // Replace <img> url starting with http://interactieontwerpen.sebastix.nl/uploads/ to ../../../images/
  // Replace <img> url starting with /uploads/ to ../../../images/
  $markdown_content = str_replace(array('http://www.sebastix.nl/blog/wp-content/uploads/', 'http://interactieontwerpen.sebastix.nl/uploads/', '/uploads/'), '../../../images/', $markdown_content);
  // TODO replace internal links starting with http://www.sebastix.nl or http://interactieontwerpen.sebastix.nl
  // TODO replace tags [flash http://youtube.com/id] with embedded iFrames
  // TODO replace html flash objects like <object height="344" width="425"><param name="movie" value="http://www.youtube.com/v/pcLaB0_eWmM&hl=nl&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed allowfullscreen="true" allowscriptaccess="always" height="344" src="http://www.youtube.com/v/pcLaB0_eWmM&hl=nl&fs=1" type="application/x-shockwave-flash" width="425"></embed></object>

  $args = [
    '%post_title' => str_replace('"', '',$data['post_title']),
    '%post_date' => $post_date,
    '%description' => substr(strip_tags($data['post_content']), 0, 120) . '...',
    '%post_content' => $markdown_content
  ];
  return str_replace(array_keys($args), array_values($args), $markdown_template);
}
