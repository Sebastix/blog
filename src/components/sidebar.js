import React from "react";

const Sidebar = ({ header }) => {

  return (
    <div
      className="md:h-screen cover-content"
      style={{ backgroundColor: "var(--sideBarBg)" }}
    >
      sidebar test
      {header}
    </div>
  );
};

export default Sidebar;
