---
layout: blog
title: Mijn vijf favoriete nieuwsbrieven die ik wekelijks lees Q32022
date: 2022-09-28T06:54:22.283Z
description: Welke nieuwsbrieven lees ik op dit moment wekelijks? Elk kwartaal
  update ik deze lijst en deel ik deze op mijn blog.
categories: nieuwsbrieven
comments: "true"
---
## [Tibor.nl](http://tibor.nl/)

## [F﻿reedom Lab](https://www.freedomlab.com/)

## [M﻿ark Hurst](https://creativegood.com/)

## [T﻿he Pragmatic Engineer](https://blog.pragmaticengineer.com/)

## [Markt Tuitert](https://marktuitert.nl/stoicijnse-mindset-en-filosofie/)