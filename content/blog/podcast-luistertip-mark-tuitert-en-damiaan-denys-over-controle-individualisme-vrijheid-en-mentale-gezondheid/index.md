---
layout: blog
title: 'Podcast luistertip: Mark Tuitert en Damiaan Denys "mensen die in
  controle zijn, hebben geen behoefte aan controle"'
date: 2021-05-14T13:43:00.000Z
description: Damiaan Denys is filosoof, psychiater en werkzaam aan de
  Universiteit van Amsterdam als hoogleraar. Mark Tuitert sprak met hem over de
  hang naar controle die we willen hebben over ons leven. Over wat vrijheid en
  individualisme is. En hoe kunnen we het best onze mentale gezondheid in de
  gaten houden?
categories: podcast filosofie vrijheid
comments: "true"
---
Luister hier de podcast DRIVE #50 Damiaan Denys en het tekort van het teveel:
<audio controls style="width:100%">
    <source src="https://rss.art19.com/episodes/3bf28a70-736b-43bd-bf8c-c75602536df6.mp3" type="audio/mpeg">
</audio>

Wat mij ontzettend intigreert, is het antwoord (op 29:15 in de podcast) wat Damiaan geeft op deze vraag van Mark:

> Je wilt toch dingen controleren als mens? Dat is toch fijn?

Damiaan antwoordt daarop:

> Wel, dat heb ik me altijd afgevraagd waarom je iets zou willen controleren. Dat snap ik dus niet, volgens mij berust dat op een enorm misverstand. \
> **Wat mij opvalt, is dat als je naar de mensen kijkt die in controle zijn nooit behoefte hebben om iets te controleren. Het zijn alleen de mensen die niet in controle zijn die daar behoefte aan hebben.**

<video controls>
    <source src="/blog/video/mensen_die_in_controle_zijn_hebben_geen_behoefte_aan_controle.MP4" type="video/mp4"></source>
</video>