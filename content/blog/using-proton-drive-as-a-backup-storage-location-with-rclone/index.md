---
layout: blog
title: Using Proton Drive as a backup storage location with Rclone
date: 2024-03-13T10:48:31.780Z
description: I'm using Proton as my default email service provider since 2022. I’ve more than 500GB storage available. This is quite a lot for just email. So I asked myself, how can I use the remaining amount of storage as an extra backup storage solution with their service Proton Drive?
categories: proton, rclone, backup
comments: "true"
---

Quite a while ago I discovered a free and open-source tool called [Rclone](https://rclone.org). It pretends to be a swiss knife for file management between different remote storage locations (more than 50 so-called cloud providers) including Proton Drive.
The following instructions are intended for Ubuntu which was my environment to test it out. They could apply for other Linux distros as well. Please follow the [Rclone docs](https://rclone.org/docs/) for more details.  

### Installing Rclone
Follow the latest install instructions [here](https://rclone.org/install/ ).  
Execute this command on your terminal
```bash
sudo -v ; curl https://rclone.org/install.sh | sudo bash
```  
and Rclone will be installed on your machine. To check if the installation succeeded, run `rclone --version` and a version will be return.  
_You can update Rclone easily with `rclone selfupdate`._  
_Execute `rclone --help` to return a full list with available commands._

### Configure Rclone and Proton Drive

Run `rclone config` and choose Proton Drive as a remote (option number 40 in my case).
After the config, get the name of the remote with `rclone listremotes`.  
_If this doesn't work out for you, please check the latest Proton Drive docs on Rclone [here](https://rclone.org/protondrive/)._

### Copy or sync files to Proton Drive

Let's create a simple file called copy-me.txt in the current directory.
```bash
echo 'this file will be copied to proton drive with rclone' > copy-me.txt
```
Execute `rclone -v copy copy-me.txt protondrive:` and this will copy the backups.txt file to your Proton Drive.  
The output will look something like this:

![](rclone-copy.png)

You can check if the file is in your Proton Drive by viewing your list of files at https://drive.proton.me/:  

![](proton-drive-list.png)

### Automated backups with cronjobs
We can use `rclone sync` to perform backup actions.  
The full command will look something like this:  
```bash
rclone sync -v --create-empty-src-dirs ~/backups protondrive:/backups
```  
Execute `crontab -e` to edit your crontab file with cronjobs and add the following line:  
```bash
3 0 * * * rclone sync -v --create-empty-src-dirs ~/backups protondrive:/backups
```
Now every night at 3am this command is executed.

### Backing up docker volumes
As a non-root user, you can’t access the directory with docker volumes at `/var/lib/docker/volumes`. You must login as root (`sudo su`) and config your rclone installation again with `rclone config` as we did earlier. After you’re done, you can copy/sync the docker volumes.
```bash
rclone sync -v --create-empty-src-dirs /var/lib/docker/volumes protondrive:/backups/docker-volumes
```
You could add this command with a cronjob as well with `crontab -e` under the user root.

### Email output of the cronjobs
To check if the cronjobs are running correctly, you can send an email with the output of the cronjob. Please read some solutions [here](https://askubuntu.com/questions/536766/how-to-make-crontab-email-me-with-output) how to manage that.

## Solid solution? How long will it work?
For now let's see how these Rclone commands with several cronjobs will run with Proton Drive for the coming period. I can imagine when a change is made on the Proton side, things could break on our side with Rclone. I hope I don't have to update Rclone too regular (let's say weekly) to keep things working.

**Used resources**

* https://dev.to/itsbetma/automated-backups-with-cron-and-rclone-3do4
* https://blog.rymcg.tech/blog/linux/rclone_sync/