---
layout: blog
title: Zoeken met Whoogle
date: 2022-05-03T19:46:22.810Z
description: Sinds vorige maand maak ik gebruik van Whoogle als mijn zoekmachine. Zoals de naam doet vermoeden, werkt deze zoekmachine met Google om dezelfde zoekresultaten terug te geven.
categories: whoogle google selfhosted degoogle bigtech
comments: false
---
Ik hoor je denken: 'waarom gebruik je dan geen Google als de zoekresultaten hetzelfde zijn?'. 

> **No ads or sponsored content**\
> No JavaScript*\
> **No cookies**\*\*\
> **No tracking/linking of your personal IP address**\*\**\
> No AMP links\
> No URL tracking tags (i.e. utm=%s)\
> **No referrer header**\
> Tor and HTTP/SOCKS proxy support\
> Autocomplete/search suggestions\
> POST request search and suggestion queries (when possible)\
> View images at full res without site redirect (currently mobile only)\
> Light/Dark/System theme modes (with support for [custom CSS theming](https://github.com/benbusby/whoogle-search/wiki/User-Contributed-CSS-Themes))
> Randomly generated User Agent\
> Easy to install/deploy\
> DDG-style bang (i.e. `!<tag> <query>`) searches\
> Optional location-based searching (i.e. results near <city>)\
> Optional NoJS mode to view search results in a separate window with JavaScript blocked  
>
> *\*No third party JavaScript. Whoogle can be used with JavaScript disabled, but if enabled, uses JavaScript for things like presenting search suggestions.*\
> *\*\*No third party cookies. Whoogle uses server side cookies (sessions) to store non-sensitive configuration settings such as theme, language, etc. Just like with JavaScript, cookies can be disabled and not affect Whoogle's search functionality.*\
> *\*\**If deployed to a remote server, or configured to send requests through a VPN, Tor, proxy, etc*

Ik sta sceptisch tegenover het gebruikmaken van Google en andere BigTech. Ze verdienen immers geld met jouw data en ze hebben zeker niet het beste met jou voor. Google is niet meer de zoekmachine van vroeger, maar is uitgegroeid bedrijf dat simpelweg advertenties verkoopt. 

![](schermafbeelding-2022-05-03-om-08.17.49.png)

Enkele bronnen die de ware aard van Google laten zien:

* [Google censuur & manipulatie Amerikaanse verkiezingen 2016 en 2020](https://mygoogleresearch.com/) (onderzoek Dr. Robert Epstein) of [check deze video](https://099b286b23d8a2f9529f-ec541110d0b03f9950b9c176a7222fa9.ssl.cf1.rackcdn.com/EPSTEIN_et_al_2021-Large-Scale_Monitoring_of_Big_Tech_Political_Manipulations-FINAL_w_AUDIO.mp4) 👀
* [Podcast Joe Rogan met Dr. Robert Epstein over hoe Google ons manipuleert](https://open.spotify.com/episode/4q0cNkAHQQMBTu4NmeNW7E)[](https://www.technologyreview.com/2021/11/20/1039076/facebook-google-disinformation-clickbait)
* [Google (+ Facebook) financieren misinformatie & clickbaits](https://www.technologyreview.com/2021/11/20/1039076/facebook-google-disinformation-clickbait) (MIT technology review)
* [How do Big Tech giants their billions?](https://www.visualcapitalist.com/how-big-tech-makes-their-billions-2022/)

Voordat ik ben overgestapt naar Whoogle, heb ik korte tijd gebruik gemaakt van Brave Search. Daarvoor heb ik meer dan een jaar DuckDuckGo gebruikt. Met de zoekresultaten van die zoekmachines heb ik nooit echt problemen gehad, al kostte het me af en toe wel meer moeite om te vinden wat ik zocht. Ik werd gedwongen om zoekopdrachten nog beter te formuleren, in plaats van dat de zoekmachine (Google is er heel goed in) dit voor me doet. 

![](img_a9260a78a405-1.jpeg)

Met Whoogle ervaar ik het beste van twee werelden: 
1. de beste zoekresultaten op basis van mijn zoekopdrachten 
2. privacy + geen advertenties.

Voor de toekomst is het de vraag hoe lang dit blijft werken. Als meer mensen gebruik gaan maken van Whoogle, verwacht ik dat Google er alles aan zal doen om de tool te blokkeren. 

Hier kun je mijn Whoogle installatie vinden die jij ook kan gebruiken om te zoeken: <https://whoogle.sebastix.dev/>