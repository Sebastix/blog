---
title: "Mediaplayers grafzerken Woord&Wapen expositie Grote Kerk Breda"
date: "2010-01-18T15:59:47.000Z"
description: "

In opdracht van de Grote Kerk Breda heb ik een stuk software ontworpen en gerealiseerd om meer dan 220 grafzerken digi..."
categories: [internet]
comments: true
---

<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="390" width="640"><param name="id" value="3250512a-0b22-4813-b90f-a8072b33166d"></param><param name="name" value="3250512a-0b22-4813-b90f-a8072b33166d"></param><param name="allowfullscreen" value="true"></param><param name="fullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="wmode" value="transparent"></param><param name="FlashVars"></param><param name="src" value="http://www.upvideo.nl/v.mp/3250512a-0b22-4813-b90f-a8072b33166d"></param><embed allowfullscreen="true" allowscriptaccess="always" fullscreen="true" height="390" id="3250512a-0b22-4813-b90f-a8072b33166d" name="3250512a-0b22-4813-b90f-a8072b33166d" src="http://www.upvideo.nl/v.mp/3250512a-0b22-4813-b90f-a8072b33166d" type="application/x-shockwave-flash" width="640" wmode="transparent"></embed></object>  
  
In opdracht van de Grote Kerk Breda heb ik een stuk software ontworpen en gerealiseerd om meer dan 220 grafzerken digitaal te ontsluiten. Van deze grafzerken zijn meer dan 1500 afbeeldingen verzameld welke via het aanraken van scherm kunnen worden bekeken.  
  
\[flickrset id="72157623109348229" thumbnail="square" overlay="true" size="medium"\]  
  
Voor de opening had ik nog een persoonlijk interview door Bredavandaag.nl:  
> BREDA - Sebastian Hagens, een 23-jarige zelfstandig ondernemer uit Breda, mag donderdagmiddag Prins Willem-Alexander vertellen over zijn digitale project voor de expositie “Woord en Wapen, in dienst van de Nassaus” in de Grote Kerk van Breda.

  
> Hagens studeert in juni af aan de Academie voor Kunst-en Vormgeving (AKV) in Breda. Voor de expositie in de Grote Kerk heeft hij een applicatie ontwikkeld voor twee touchscreens. Op de digitale schermen kan het publiek 220 grafmonumenten en bijna 1700 afbeeldingen bekijken.  
>   
> “Toen ik begon aan de opdracht, had ik nog geen idee hoe groot en belangrijk de expositie zou zijn”, lacht Hagens.”De digitale expositie was eerst toebedeeld aan de NHTV, maar zij konden het niet bolwerken. Vervolgens ben ik benaderd.” Toen na een tijdje bleek dat de expositie van een grote omvang was, moest Hagens even slikken, “Maar het was meteen een extra motivatie”, vertelt hij.  
>   
> De zelfstandig ondernemer kreeg welgeteld twee maanden om zijn project af te maken, de tijdsdruk deed hem echter niet veel. “Ik had alleen wat gezonde stress, maar verder viel het heel erg mee.” Hagens werkt vanuit het Blushuis, een verzamelgebouw voor creatieve bedrijven, en kreeg veel hulp van Walter van de Garde, gastconservator van de Grote Kerk. “Alleen mijn studie is een beetje onder het project gaan lijden”, zegt Hagens.  
>   
> Hagens is achteraf heel erg blij met de opdracht. “Maar ik ben een perfectionist, het zal nooit honderd procent mijn goedkeuring krijgen. Ik ben wel heel erg blij dat het tot een goed resultaat heeft geleid voor mij, en ook voor de Kerk.” Prins Willem-Alexander heeft donderdagmiddag een kwartier uitgetrokken om met de medewerkers van de expositie te praten. “Ik vind het reuze spannend”, lacht Hagens. “Maar ik laat het maar gewoon gebeuren.”  
> Bron: <http://www.bredavandaag.nl/nieuws/cultuur/2010-01-13/%27mijn-studie-is-er-een-beetje-onder-gaan-lijden%27>

  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="505" width="640"><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://www.youtube.com/v/pqi8tea8ngg&hl=nl_NL&fs=1&"></param><embed allowfullscreen="true" allowscriptaccess="always" height="505" src="http://www.youtube.com/v/pqi8tea8ngg&hl=nl_NL&fs=1&" type="application/x-shockwave-flash" width="640"></embed></object>  
  
Graag wil ik bedanken:  
  
- [Het Licht](http://www.hetisaan.net/), Victor Freriks | technische ondersteuning
  
- [Amanda Butterworth](http://www.amandabutterworth.com/) | Grafisch ontwerp
  
- [Starterslift](http://www.starterslift.nl), Heleen Verhoeff | PR
  
- [Grote Kerk Breda](http://www.grotekerkbreda.nl/), Walter van de Garde en Willem van der Vis
  
- [Hans Kuiper](http://www.hanskuiper.nl/) | Fotografie grafzerken
  

  
*Software mediaplayer: Adobe AIR (HTML, Javascript, Flash)*  
  
Bronnen:[  
http://www.bndestem.nl/regio/breda/6087617/Kroonprins-neemt-tijd-voor-tentoonstelling-Grote-Kerk.ece  
http://www.bredavandaag.nl/nieuws/2010-01-14/prins-willem-alexander-als-cadeautje-video  ](http://www.bredavandaag.nl/nieuws/2010-01-14/prins-willem-alexander-als-cadeautje-video)[http://www.youtube.com/watch?v=pqi8tea8ngg](http://www.deweekkrant.nl/artikel/2010/januari/15/kroonprins_opent_woord_wapen)[  
http://www.deweekkrant.nl/artikel/2010/januari/15/kroonprins\_opent\_woord\_wapen  ](http://www.blikopnieuws.nl/bericht/106595/Prins_opent_tentoonstelling_Woord_&_Wapen.html)[http://www.blikopnieuws.nl/bericht/106595/Prins\_opent\_tentoonstelling\_Woord\_&amp;\_Wapen.html  ](http://www.youtube.com/watch?v=BuEqXxfGB08)[http://www.youtube.com/watch?v=BuEqXxfGB08  
http://www.uitzendinggemist.nl/index.php/aflevering?aflID=10538254&amp;md5=304c4bb492e3e7c61ad3d19c10bb2441](http://www.uitzendinggemist.nl/index.php/aflevering?aflID=10538254&md5=304c4bb492e3e7c61ad3d19c10bb2441)
