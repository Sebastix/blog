---
title: "Mobiele applicatie Holla Advocaten"
date: "2012-10-29T09:22:57.000Z"
description: "Sinds enkele weken is de eerste mobiele applicatie van Sebastix een feit. In zowel de App Store voor iOS apparaten als i..."
categories: [internet]
comments: true
---

Sinds enkele weken is de eerste mobiele applicatie van Sebastix een feit. In zowel de App Store voor iOS apparaten als in de Google Play store voor Android toestellen staat de applicatie voor advocatenbureau Holla Advocaten live.

> <small>Bij Holla Advocaten zijn ze betrokken bij de zaken die ze behandelen. Speciaal voor haar cliënten, relaties en geïnteresseerden ontwikkelde Holla Advocaten daarom de Holla app. Via deze app heb je vanaf nu met een klik op de knop alle gegevens van je advocaat bij de hand. Dat noemen ze bij Holla Advocaten Hoogst Persoonlijk.  
>  Maarten Letschert, bestuursvoorzitter van Holla Advocaten: “Er bestaan al verschillende juridische apps, maar wij hebben er voor gekozen om het persoonlijke karakter te benadrukken. Naast de profielen van de advocaten hebben we de app nog persoonlijker gemaakt, doordat gebruikers zelf kunnen aangeven over welke onderwerpen ze graag op de hoogte gehouden willen worden, desgewenst door middel van pushberichten.” De Holla app is gebruiksvriendelijk en biedt gemak. Zo worden via de expertises van Holla Advocaten direct de gespecialiseerde advocaten en de nieuwsberichten en evenementen binnen die specialisatie getoond. “Deze koppeling zorgt voor een helder overzicht, niet alleen voor bestaande relaties. Ook geïnteresseerden kunnen in één oogopslag zien welke advocaat ze kunnen benaderen.”  
>  Bron: <http://www.holla.nl/NL/nieuws/nieuws-overzicht/de-holla-app-uw-advocaat-altijd-bij-de-hand></small>

  
 In mei dit jaar benaderde [Imagobureau TINK!](http://www.tinkaboutit.nl/) mij voor dit project. TINK! is als opdrachtnemer verantwoordelijk voor de ontwikkeling van het concept, het functionele ontwerp en de grafische vormgeving van de applicatie. Ik heb zorg gedragen voor de technische realisatie. Om dubbel werk zoveel mogelijk te voorkomen, heb ik de applicatie ontwikkeld in het [Appcelerator](http://www.appcelerator.com) Framework. Met de bestaande kennis van web-techniek was het schrijven van de applicatie in Javascript geen probleem. De grootste uitdaging lag in het opzetten van een technisch ontwerp, want tussen iOS en Android zitten op sommige punten behoorlijke verschillen in hoe bijvoorbeeld de navigatie eruit moet zien op basis van wat eindgebruikers verwachten. Het uitgangspunt was zoveel mogelijk code te hergebruiken, uiteindelijk is dit voor zo’n 70% gelukt.  
<table>  
<tbody>  
<tr>  
<td>[![](../../../images/2012/10/iOS-simulator-iPhone-iOS-6.0-10A403.png "iOS-simulator - iPhone - iOS 6.0 (10A403)")](../../../images/2012/10/iOS-simulator-iPhone-iOS-6.0-10A403.png)</td>  
<td>[![](../../../images/2012/10/iOS-simulator-iPhone-iOS-5.1-9B176-5.png "iOS-simulator - iPhone - iOS 5.1 (9B176) 5")](../../../images/2012/10/iOS-simulator-iPhone-iOS-5.1-9B176-5.png)</td>  
<td>[![](../../../images/2012/10/iOS-simulator-iPhone-iOS-5.1-9B176-3.png "iOS-simulator - iPhone - iOS 5.1 (9B176) 3")](../../../images/2012/10/iOS-simulator-iPhone-iOS-5.1-9B176-3.png)</td>  
</tr>  
<tr>  
<td> [![](../../../images/2012/10/device-2012-08-27-202432.png "device-2012-08-27-202432")](../../../images/2012/10/device-2012-08-27-202432.png)</td>  
<td>[![](../../../images/2012/10/device-2012-08-27-202545.png "device-2012-08-27-202545")](../../../images/2012/10/device-2012-08-27-202545.png)</td>  
<td>[![](../../../images/2012/10/device-2012-08-27-201943.png "device-2012-08-27-201943")](../../../images/2012/10/device-2012-08-27-201943.png)</td>  
</tr>  
</tbody>  
</table>

  
  
### CMS (content management system)

  
 Holla Advocaten publiceert regelmatig nieuwsberichten en deze wilden zij ook via de mobiele app beschikbaar maken. Naast deze nieuwsberichten had Holla ook de wens andere inhoud in de mobiele applicatie direct aan te kunnen passen. Om dit te realiseren, is de mobiele app ook met een compacte CMS omgeving opgeleverd. Via een API haalt de app altijd de meest actuele content op.  
### Push berichten

  
 Dankzij de push mogelijkheden die mobiele apparaten tegenwoordig bieden, is het ook mogelijk dat gebruikers zich kunnen abonneren op nieuwsberichten. Zodra er een nieuw nieuwsbericht is geplaatst door Holla Advocaten, ontvang je hierover automatisch een melding op je telefoon waarna je het volledige bericht kunt lezen.  
  
 Sebastix is bijzonder trots dat onze eerste mobiele applicatie vooral een veelzijdige app is geworden met verschillende componenten. Nu Sebastix steeds meer mogelijkheden voor mobiele applicaties tot zijn beschikking heeft, ziet hij in dat apps voorlopig niet zullen verdwijnen in het digitale landschap. Uiteindelijk is een mobiele applicatie net als een website, een middel wat aan moet sluiten bij bepaalde doeleinden. Omdat mobiele applicaties in andere contexten bewegen dan bijvoorbeeld websites, kun je deze ook voor andere doeleinden inzetten. Tegelijkertijd zorgt dit er ook voor dat websites een andere rol gaan vervullen. Het landschap van verschillende apparaten verandert razendsnel en samen met alle technische veranderingen ziet Sebastix het ook als een uitdaging om in deze dynamiek interessante toepassingen te ontwerpen en ontwikkelen.  
  
[Download de iPhone app in de App store  ](http://itunes.apple.com/nl/app/holla-advocaten/id537651671?mt=8&uo=4 "Download de iPhone app in de App store")[Download de Android app in Google Play](https://play.google.com/store/apps/details?id=com.sebastix.holla "Download de Android app in Google Play")  
  
 Ook meer te lezen hier: <http://www.tinkaboutit.nl/portfolio/holla-advocaten/>
