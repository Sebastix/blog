---
layout: blog
title: Jouw data opslaan of beveiligen met Bitcoin
date: 2021-04-05T20:36:31.539Z
description: Kunnen we digitale informatie (zoals je persoonlijkse foto's)
  opslaan of beveiligen met Bitcoin? Ja dat kan. Althans, volgens Marc van der
  Chijs.
categories: Bitcoin, personal data, privacy, cloud
comments: "true"
---
Volgens Marc van der Chijs is de Bitcoin blockchain de beste plek om data op te slaan. 

https://youtu.be/p_bfbzjZSxk?t=4709

Dat inzicht bleef bij mij blijven hangen en via de chat (tijdens de livestream van deze video) stelde ik daarom de volgende vraag:

> Marc, op welke manier zou de blockchain van Bitcoin gebruikt kunnen worden om data (zoals mijn digitale foto's) op te slaan? Via 2nd layer oplossingen?

Niet veel later volgde een antwoord:

https://youtu.be/p_bfbzjZSxk?t=6734

Het antwoord is dus iets genuanceerder. Technisch gezien gebruik je dus jouw private sleutel (your keys!) om jouw data te versleutelen.

Zou het Bitcoin netwerk dan een perfecte tool kunnen zijn om je persoonlijke data te beveiligen? Zou dit een manier zijn die aansluit bij het onderzoek wat ik 10 jaar geleden deed hoe je het beste jouw digitale eigendommen kunt bewaren? Ik noemde het toen 'Jouw personal graph', zie deze blogpost: <https://sebastix.nl/blog/io/jouw-personal-graph/>. In deze blog noem ik een aantal projecten die een digitale kluis aanbieden waar je jouw persoonlijke data kunt bewaren. Nu ik steeds beter de techniek achter Bitcoin (als trustless consensus netwerk) begrijp, begin ik ook in te zien hoe het netwerk van Bitcoin jouw data kan beveiligen. 

Het bezorgt mij het nodige denkwerk en het vraagt om het uitwerken van een technische proof-of-concept die bewijst hoe dit kan werken. To be continued...