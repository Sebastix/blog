---
layout: blog
title: "DeGoogle: de overstap van Gmail naar Proton"
date: 2022-10-21T18:46:13.452Z
description: Op 1 april 2004 lanceerde Google de email dienst Gmail. Op
  kerstavond 24 december 2004 ontving ik mijn uitnodiging om met Gmail aan de
  slag te gaan. Tot februari 2007 is de dienst invite-only beschikbaar gebleven.
  Na 17 jaar is het tijd voor mij om afscheid te nemen van Gmail.
categories: degoogle gmail protonmail
comments: "true"
---
![](gwwce9hw.png)

Mijn eerste mails verstuurde ik in januari 2005. Inmiddels heb ik bijna 17GB aan data in mijn Gmail staan. In 2005 was ik nog niet actief als ondernemer, maar zat ik nog op de middelbare school. Pas later heb ik dus mijn mails van *@sebastix.nl aan Gmail gekoppeld. Sindsdien heb ik altijd Gmail gebruikt als mijn primair email programma voor al mijn mailadressen. Dit zijn er gelukkig niet veel, dus het migreren van alle mails naar Proton zou niet bijzonder complex moeten zijn.

![](schermafbeelding-2022-10-18-om-09.44.33.png)

### Alles importeren met Easy Switch naar ProtonMail

Met de Easy Switch functie kun je je mails, contacten en kalender in 1 keer importeren van je Google account. Jammer genoeg zitten daar niet alle filters bij. Nadat ik de Easy Switch functie had geactiveerd, duurde het 2 tot 3 dagen voordat alle mails waren geïmporteerd. Althans zo leek het. Ondanks dat ik had aangeven om alle mails te archiveren, stonden er toch meer dan 12.000 mails in mijn inbox in plaats van het archief. Deze heb ik allemaal handmatig moeten archiveren om weer met een vertrouwde lege inbox te werken. Na de import viel het me wel op dat slechts 8GB aan data in gebruik was. Dat is de helft minder dan wat Gmail aan opslag aangaf. Na enkele checks zag ik dat de import nog steeds bezig was die ik had gestart op 11 oktober. Na een week heb ik de import geannuleerd nadat ik contact had opgenomen met de klantenservice. Op dit moment heb ik nog steeds contact met een engineer om de import te doen slagen. Deze lijkt namelijk te blijven hangen en mis ik op dit moment nog alle mails in de periode van 2009 t/m 2004.

![](schermafbeelding-2022-10-25-om-10.14.08.png)

### L﻿abels overzetten

Door de jaren heen heb ik behoorlijk wat labels met sublabels aangemaakt om mails te categoriseren. Mijn inbox houd ik namelijk altijd zo goed als leeg. De labels in mijn Gmail heb ik nooit opgeruimd dus voor Protonmail leek het me een mooi moment om een nieuwe lijst met labels uit te werken. Daarnaast kun je in Proton ook folders aanmaken waarin je de mails kunt plaatsen. Dit is een feature die Gmail niet heeft. Het verschil tussen folders en labels? Een mail kun je van van meerdere labels voorzien en diezelfde mail kun je maar in één folder plaatsen.

### F﻿ilters overzetten

Het overzetten van alle filters was handwerk. Dit heb ik gedaan op basis van al bestaande mails in mijn inbox. Sinds kort is ook de optie ‘apply filter on existing mails’ toegevoegd, waardoor je een nieuwe filter kunt toepassen voor bestaande mails.

Ik heb mezelf de gewoonte aangeleerd om een nieuwe filter aan te maken als deze past in mijn label / folder structuur. Stel dat ik een nieuwsbrief ontvang in mijn inbox, dan maak ik een filter aan op basis van deze mail (de criteria hiervoor is de afzender) en stel ik in dat deze mail moet worden verplaatst naar de folder ‘newsletter’.

### **Kosten**

Ik heb het business model gekozen voor € 9,99 komende 24 maanden. Met korting was dat een investering van € 239,76. Is dat veel? Dat is maar net hoe je het bekijkt hoe waardevol email voor jou is. Je krijgt er in ieder geval wel 500GB opslagruimte voor terug. Naast e-mail krijg je ook volledige toegang tot de kalender, drive en VPN diensten van Proton. Niet iets wat ik direct zou gebruiken, maar wie weet gaat ProtonDrive handig zijn in de toekomst waar je documenten encrypted kunt opslaan. 

### **Overal mijn mailadres aanpassen**

Hoe weet ik nu waar ik allemaal mijn Gmail adres gebruik bij een account? Middels mijn wachtwoordmanager Bitwarden kan ik een mooi begin maken. Hier kan ik in één overzicht alle accounts met mijn Gmail adres als gebruikersnaam tonen. Verder zal het nog wel een hele poos duren voordat ik overal mijn mailadres heb aangepast. Het fijne is nu dat mijn Gmail inbox helemaal leeg is en blijft. De mails die daarop binnenkomen, zijn of de moeite niet waard (nieuwsbrief afmelden of account verwijderen) of ik weet dat ik bij die afzender mijn mailadres moet aanpassen.

### **Alternatieven**

Zijn er nog andere email providers waar ik naar gekeken heb als alternatief voor Gmail? Ja, dat zijn:

* Tutanota
* Fastmail
* Startmail