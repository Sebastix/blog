---
layout: blog
title: Tips om meer digitaal humanist te worden. Zet menselijkheid centraal,
  niet de techniek.
date: 2021-10-08T07:39:31.652Z
description: Drie waardevolle tips van Fredo de Smet om jouw eigen menselijkheid
  meer centraal te zetten in een snel veranderende digitale wereld.
categories: digitalisering
comments: "true"
---
# 1. Waardeer je aandacht

# 2. Kies voor competentie in plaats van competitie

# 3. Break your bubble

Bekijk zijn talk hier: <https://www.brainwash.nl/bijdrage/curator-fredo-de-smet-laat-je-niet-te-veel-door-een-machine-bepalen>