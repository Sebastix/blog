---
layout: blog
title: Deploy via Gitlab met Netlify CMS
date: 2020-12-19T20:51:34.216Z
description: Netlify, een CMS waarmee je statische pagina's genereert in
  Markdown formaat en vervolgens via Gitlab CI live zet. Dat gebruik ik om
  berichten te plaatsen op mijn blog.
categories: Gitlab, Netlify CMS
comments: "true"
---
Met behulp van [Gatsby](https://www.gatsbyjs.com/) en de [Leonids](https://www.gatsbyjs.com/starters/renyuanz/leonids) template heb ik deze blog opgebouwd. Er draaien geen backend scripts en/of een database achter om de berichten te beheren en op te slaan. De berichten worden opgeslagen in [Markdown](https://nl.wikipedia.org/wiki/Markdown) bestanden.

### Netlify CMS

Het schrijven van blog berichten in Markdown bestanden in je editor (in mijn geval is dat PHPStorm) is niet altijd even praktisch dus ik zocht naar een CMS (content management system) voor mijn blog. Normaal gesproken ben ik gewend om een CMS middels PHP en een database te realiseren.  Met [Netlify CMS](https://www.netlifycms.org/) hoeft dit niet. Dit CMS draait volledig 'client-side' zonder backend scripts/database. Dit CMS heeft een API koppeling met je Git repository (bv op Github, Bitbucket, Gitlab of op je eigen Git server). Via deze koppeling worden wijzigingen rechtstreeks als commits naar je repository verzonden. Deze Git repository is dus in feite de backend voor alle content.

![](schermafbeelding-2020-12-22-om-11.48.03.png)

*Screenshot van het Netlify CMS voor mijn blog.*

### Gitlab

De autorisatie om toegang tot het CMS te krijgen vind plaats met je Git account. Het toevoegen, wijzigen of verwijderen van je blog berichten wordt direct als wijziging in je repository opgeslagen. In het gitlab-ci.yml bestand heb ik ingesteld zodra er een wijziging is op mijn blog, dat er direct een deployment plaatsvindt naar mijn hosting van mijn blog. Met andere woorden, als ik in het CMS een nieuw bericht opsla wordt dit direct via dit script live gezet.

### Voordelen

* Geen PHP backend nodig.
* Geen database nodig.
* Toegang tot je CMS met jouw Git account.
* Wijzigingen live zetten via Github Workflow of Gitlab CI.
* Optie om berichten ook als concept op te slaan in het CMS ([editorial workflow](https://www.netlifycms.org/docs/deploy-preview-links/)).

### Nadelen

* Een deployment duurt even, zo'n 10 minuten in mijn geval. Wijzigingen die je doet in het CMS zijn dus niet direct zichtbaar, maar pas na deze tijd.

De code van mijn blog kun je hier bekijken in deze repository op Gitlab: <https://gitlab.com/Sebastix/blog>

Dit is de snippet van gitlab-ci.yml waarmee wijzigingen via Netlify CMS online worden gezet op mijn blog:

```
image: node:latest

cache:
  paths:
    - node_modules/
    # Enables git-lab CI caching. Both .cache and public must be cached, otherwise builds will fail.
    - .cache/
    - public/

blog:build:
  script:
    - npm install
    - ./node_modules/.bin/gatsby build --prefix-paths
  artifacts:
    paths:
      - public
  only:
    refs:
      - master
    changes:
      - content/blog/**/*

blog:deploy:
  image: alpine
  stage: deploy
  before_script:
    - 'which ssh-agent || ( apk add --update openssh )'
    - apk add --update bash
    - eval $(ssh-agent -s)    
    - echo "$SSH_PRIVATE_KEY" | ssh-add -
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - scp -r public/. sebastix@sebastix.nl:/home/sebastix/public_html/blog
  only:
    refs:
      - master
    changes:
      - content/blog/**/*
```

Netlify CMS documentatie: [https://www.netlifycms.org/docs/intro/ ](https://www.netlifycms.org/docs/intro/)