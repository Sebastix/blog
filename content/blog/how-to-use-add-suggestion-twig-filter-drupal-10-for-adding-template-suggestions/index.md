---
layout: blog
title: How to use |add_suggestion Twig filter in Drupal 10 for adding template suggestions
date: 2023-04-26T13:38:32.328Z
description: Thanks to Lullabots podcast I discovered a new Twig filter add_suggestion I immediately started using in a Drupal 10 project I'm building.
categories: drupal twig theme frontend alter suggestions
comments: "true"
---

In most of my Drupal projects I've a theme alter function `mythemename_theme_suggestions_alter` to generate Twig template file name suggestions. Here is a snippet of the that function:

```php
  // Forms.
  if ($hook === 'form' & !empty($variables['element']['#id'])) {
    $suggestions[] = 'form__' . str_replace('-', '_', $variables['element']['#id']);

    if (isset($variables['element']['#theme'])) {
      if (is_array($variables['element']['#theme']) === false) {
        $suggestions[] = 'form__' . str_replace('-', '_', $variables['element']['#theme']);
      } else {
        foreach ($variables['element']['#theme'] as $theme) {
          $suggestions[] = 'form__' . str_replace('-', '_', $theme);
        }
      }
    }
  }
  // Form elements.
  if ($hook === 'form_element' && isset($variables['element']['#id'])) {
    $suggestions[] = 'form_element__' . $variables['element']['#type'] . '__' . str_replace('-', '_', $variables['element']['#id']);
  }
  if ($hook === 'container' && isset($variables['element']['#attributes']['data-drupal-selector'])) {
    $suggestions[] = 'container__' . str_replace('-', '_', $variables['element']['#attributes']['data-drupal-selector']);
  }
  if ($hook === 'input' && isset($variables['element']['#id'])) {
    $suggestions[] = 'input__' . str_replace('-', '_', $variables['element']['#id']);
  }
  if ($hook === 'select' && isset($variables['element']['#attributes']['data-drupal-selector'])) {
    $suggestions[] = 'select__' . str_replace('-', '_', $variables['element']['#attributes']['data-drupal-selector']);
  }
  if ($hook === 'form_element_label' && isset($variables['element']['#id'])) {
    $suggestions[] = 'form_element_label__' . str_replace('-', '_', $variables['element']['#id']);
  }
  if ($hook === 'fieldset') {
    if (isset($variables['element']['#form_id'])) {
      $suggestions[] = 'fieldset__' . str_replace('-', '_', $variables['element']['#form_id']);
      $children = Element::children($variables['element']);
      if (isset($children[0]) && isset($variables['element'][$children[0]]['#type'])) {
        $type = $variables['element'][$children[0]]['#type'];
        foreach ($suggestions as $suggestion) {
          $suggestions[] = $suggestion . '__element_type__' . $type;
        }
        $suggestions[] = 'fieldset__element_type__' . $type;
      }

    }
  }
  // Views.
  if ($hook === 'views_view') {
    $suggestions[] = 'views_view__' . $variables['view']->id();
  }
  if ($hook === 'views_view_table') {
    $suggestions[] = 'views_view_table__' . $variables['view']->id();
  }
  if ($hook === 'views_view_unformatted') {
    $suggestions[] = 'views_view_unformatted__' . $variables['view']->id();
  }
  if ($hook === 'views_view_fields') {
    $suggestions[] = 'views_view_fields__' . $variables['view']->id();
  }
  if ($hook === 'views_view_field') {
    /** @var \Drupal\views\Plugin\views\field\EntityField $field */
    $field = $variables['field'];
    $suggestions[] = 'views_view_field__' . $variables['view']->id() . '__' . $field->options['field'];
  }
```

While listening to Lullabots podcast [Drupal X: Front End and Beyond](https://overcast.fm/+B8w_ER7c4), I heart of a new Twig filter called `add_suggestion`. At this moment I'm working on a Drupal 10 project, so I thought to give this a quick try.

In a Twig file where the content element with several fields is rendered, I changed   
`{{ content }}`  
to  
`{{ content.comment_body|add_suggestion('cchs-comment-body') }}`.

After this change I checked my template suggestions output in the browser to check if the file name appeared in the list of file name suggestions of templates.

![twig-file-name-suggestions.png](twig-file-name-suggestions.png)

It didn't show up, bummer. This is not working yet, see [this issue](https://www.drupal.org/project/drupal/issues/2118743).

The template file name for this suggestion name should be `field--cchs-comment-body.html.twig` so I created this file in my theme's `templates/field` directory and cleared my Drupal cache with `drush cr`.

Yay! Now it showed up!

![twig-file-name-suggestion-with-add-suggestion-filter.png](twig-file-name-suggestion-with-add-suggestion-filter.png)

To sum it all up, here is a screenshot of my editor where you can see the code with `|add_suggestion` to render a body field of a comment with the suggested Twig template file. 

![phpstorm-add-suggestion-twig-filter-drupal-10.png](phpstorm-add-suggestion-twig-filter-drupal-10.png)

I'm looking forward to even more handy Twig filters coming up in Drupal 10.1!

* `|add_class`

* `|remove_class`

* `|set_attribute`

* `|value`

--- 

Resources:

* [Drupal X: Front End and Beyond &mdash; Lullabot Podcast &mdash; Overcast](https://overcast.fm/+B8w_ER7c4)

* [New |add_suggestion Twig Filter Added to Drupal 10! | Herchel.com](https://herchel.com/articles/new-addsuggestion-twig-filter-added-drupal-10) 

* [New Twig |add_suggestion filter for adding a theme suggestion | Drupal.org](https://www.drupal.org/node/3301862) 
