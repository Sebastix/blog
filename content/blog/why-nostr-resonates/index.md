---
layout: blog
title: Why Nostr resonates
date: 2023-10-23T23:09:11.463Z
description: After the Nostr hype begin this year I was not very active on the network this summer like many others (the same applies to Mastodon). Still, a nice group of power users is still very active and developers are still building clients and improving NIPs. This holds my believe in Nostr strong. 
categories: nostr, freedom tech
comments: "true"

---

For those who are new to Nostr, here is a short explainer what it is.

> **Nostr** is a communication protocol and an acronym which stands for **<u>N**otes and **O**ther Stuff **T**ransmitted by **R**elays</u>. It is an open, independent standard upon anyone can build. Nostr is designed for simplicity and enables censorship-resistant, decentralized and global publishing on the web. All data is sent in events packaged as plain JSON which are shared between applications and relays. 

For me, Nostr resonates with many trends and signals I follow on the internet. Plus, it matches many of my own personal values around online privacy and digital autonomy. Nostr can fix the web in a similar way how bitcoin can fix the money system. And try to image when the two merge together.  

For me it makes sense that Nostr will evolve into a future-proof protocol. I will try to clarify that in my article below. 

## Nostr clients are <u>local-first</u>

In the last decade our devices have transferred to very powerful small computers, like your smartphone. This means applications can do a lot of smart stuff without the need of someone else's computer, the so called cloud with a huge backend of programs and services. Thanks to Nostr we can fade away from the classic "many-clients-to-one-server" ecosystem used by the tech monopolies for their benefits. 

## <u>Data portability</u> between clients

Have a look at the long-form content with Nostr (kind 30023 from NIP-23) so called blog content. These two websites are the most popular which integrated NIP-23: blogstack.io and habla.news. Let's compare a piece of content to see what data portability means: 

| Habla.news                                                                               | Blogstack.io                                                                                                                                                                                 |
| ---------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![https://habla.news/u/k00b@stacker.news/284021](Screen-Shot-2023-10-15-14-25-19.96.png) | ![https://blogstack.io/naddr1qqrrywp5xqerzqg5waehxw309aex2mrp0yhxgctdw4eju6t0qgsqtyeas7pdz4w3pnu2qmehjchn9xz4rzqx8ypaxvn3f77csxavgmsrqsqqqa287k7ljz](Screen-Shot-2023-10-15-14-26-23.48.png) |

Or look at my profile on all of these different clients:

| client                                      | client                                      |
| ------------------------------------------- | ------------------------------------------- |
| ![](Screen-Shot-2023-10-17-15-44-43.97.png) | ![](Screen-Shot-2023-10-17-15-45-01.57.png) |
| ![](Screen-Shot-2023-10-17-15-45-18.31.png) | ![](Screen-Shot-2023-10-17-15-45-40.98.png) |
| ![](Screen-Shot-2023-10-17-15-45-55.66.png) | ![](Screen-Shot-2023-10-17-15-46-19.68.png) |
| ![](Screen-Shot-2023-10-17-15-46-58.70.png) | ![](Screen-Shot-2023-10-17-15-47-16.25.png) |
| ![](Screen-Shot-2023-10-17-15-47-37.05.png) | ![](Screen-Shot-2023-10-24-10-44-25.95.png) |

## You really <u>own your identity</u>

Anyone can create keys which represents an identity. You have to save and secure your secret key which is used for signing your events with content. <u>Please be aware</u>: ***with this ownership of your signing-keys comes responsibility for your identity and data***.

## <u>Privacy</u> by default

The signing of events are done with the same public-key cryptography as you can find in bitcoin for signing transactions. Like Bitcoin, Nostr is not anonymous. Your identity is a pseudonym. You have full control of what you share with this pseudonym. If you desire full anonymity, you have to use clients which use the Tor network for example. Or use the Tor network on your behalf while using Nostr clients. 

## <u>Permissionless</u>

Like with many other protocols you are free to start building. Just build a client, connect with some relays and start requesting notes (kind 1) for example for making a feed with the latest posted notes. 

## Going <u>trustless</u>

You can use the Nostr network with a lot less trust than other services, especially compared to the BigTech in the social media ecosystem. The Nostr protocol is not owned by any third party or authority, just like the http protocol for websites or smtp protocol for email. It's open and independent by default. 
The trustless way is to build and run your own client, create your keys on your behalf and run your own relay. Than others clients can connect to your relay to see the events you're broadcasting. 

## It’s <u>easy</u> and developer friendly

Any junior developer could set up a client within a couple of hours. There are plenty of simple projects written in different languages on Github you can use as a starting pint. At this time of writing, there are more than 700 public repositories tagged with Nostr on Github.

## <u>Censorship resistance</u>

Nostr is a perfect match for building freedom technology, because the open and independent character of the protocol. Each client can connect to multiple relays to broadcast events. When a relay decides to block you, you can easily use other relays to send your content to others. 

## Nostr is <u>agnostic</u> and for any data you would like to share

The protocol can be used for any data you would like to transfer. Have a look at this list of different types of content called kinds (there are many more, this is just a selection):

| Kind  | Description               |
| ----- | ------------------------- |
| 0     | Metadata                  |
| 1     | Short note                |
| 4     | Direct message            |
| 6     | Repost                    |
| 7     | Reaction                  |
| 1984  | Report                    |
| 9735  | Zap                       |
| 30023 | Long-form content         |
| 30078 | Application specific data |

The majority of content are notes with kind 1 which are immutable. Long form content (kind 30023) is not immutable, it's editable. 
Another example is kind 7 which is used for comments (or kind 1) or other types of reactions on content.

## Integrating the <u>value4value</u> model

Zaps are so cool! **Let's pay with sats instead of our personal data and attention**. The value4value philosophy is really a game-changer on monetizing content. Nostr has adopted this model (just like Podcasting 2.0) and is demonstrating how real digital value (backed by bitcoin sats) works. You have to option to enable zaps so you can send and receive sats on content you share. 

## Backed by a <u>community</u>

Nostr is being bootstrapped by the bitcoin community. There is a huge advantage of early bitcoiners who have less financial worries so they can back the Nostr community. There are funds available for many type of projects for attracting more people into the Nostr ecosystem.

## It's still very, very early

Compared to a new discovered territory, we've just started building small shelters. The Nostrverse is still very small with maybe 100 builders who are tinkering their way around the protocol. This means new clients and features popup every week.
As now, many clients (and relays) are suffering from different performance issues. Most applications out there are doing one thing incredibly well. This means you need to use many different applications to understand and feel the Nostr experience. That's currently how I'm moving through the Nostrverse. For now that's ok. We need that friction to innovate and drive discovery. There is no freedom without restrictions. I'm sure the future will bring many more great applications with features which will show a completely new paradigm on how the web can work for us: the individuals. 

## My quest on contributing

As a developer I'm looking for ways how I can contribute to the Nostrverse. Currently I'm maintaining and building on these projects in my spare time.

* [nostr-php](https://github.com/swentel/nostr-php) - a PHP helper library
* Drupal module: [Nostr internet identifier NIP-05](https://www.drupal.org/project/nostr_id_nip05)
* Drupal module: [Nostr Simple Publish](https://www.drupal.org/project/nostr_simple_publish)
* Drupal module: [Nostr long-form content NIP-23](https://www.drupal.org/project/nostr_content_nip23)

I also have some other technical ideas:

* how Nostr people and applications can benefit more from NIP-65 (many people are loosing their contacts if they start using more than one application)
* how Drupal can be used as a RMS (relay management system for e.g. moderating content) on existing relay implementations

I hope I can find some time in the future to outline those ideas on https://nostrver.se.

If you would like help me out with my quest, please reach out to me on Nostr. My pubkey is
`npub1qe3e5wrvnsgpggtkytxteaqfprz0rgxr8c3l34kk3a9t7e2l3acslezefe` or search for my Nostr handle `sebastian@sebastix.dev`. Or just contact me the old fashioned way through my [contact page](https://sebastix.nl/contact).

**Used resources:**

* [Local-first software](https://www.inkandswitch.com/local-first/static/local-first.pdf)

* [NIP-23](https://github.com/nostr-protocol/nips/blob/master/23.md)

* [NIP-65](https://github.com/nostr-protocol/nips/blob/master/65.md)

* https://nostr.how/ 

* [List with NIPS, event kinds, message types and tags](https://nostr-nips.com/) 

* [nostr · GitHub Topics · GitHub](https://github.com/topics/nostr)

* https://value4value.info/

* [Nostr: A simple, open protocol enabling global, decentralized, and censorship-resistant social media - Invidious](https://yewtu.be/watch?v=8mSyMCJlSwA) 

* [The Nostr Fund - OpenSats](https://opensats.org/blog/opensats-receives-additional-funding-of-dollar10m-from-startsmall)

* [A Vision for a Value-Enabled Web](https://dergigi.com/2022/12/18/a-vision-for-a-value-enabled-web/)

* https://www.whynostr.com/explore 