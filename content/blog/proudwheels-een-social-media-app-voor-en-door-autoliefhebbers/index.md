---
layout: blog
title: Proudwheels - petrolheads community en een social media app
date: 2022-02-12T14:42:14.267Z
description: Al ruim een jaar ben ik betrokken bij het mooie initiatief
  Proudwheels waarbij een social media app wordt opgezet voor en door
  petrolheads. Daar wil ik graag iets meer over vertellen in dit artikel.
categories: Proudwheels, automotive, social media
comments: "true"
---
## Het begin

In januari 2021 nam Ronald van 402Online (voorheen 402automotive en 402events) contact met me op met de vraag of ik hem kon helpen met het opzetten van een social media platform. Ronald was een van mijn eerste klanten bij Sebastix waar ik langdurig mee heb samengewerkt. In de periode van 2012 tot 2019 heb ik veel technisch werk verricht. Ik heb onder andere alle websites voor verschillende evenementen mogen realiseren en onderhouden in combinatie met een maatwerk content management systeem.

## Even terug in de tijd met Carage

![](carage-logo-index.png)

In oktober 2012 schreef ik op mijn blog het artikel [Carage - social network als applicatie voor autoliefhebbers](https://sebastix.nl/blog/io/carage-social-network-applicatie-voor-autoliefhebbers/) en in maart 2021 schreef ik er nog eens over in [Carage: een automotive community platform in 2012](https://sebastix.nl/blog/carage-automotive-community-platform-in-2012/). Een citaat uit mijn artikel uit 2012:

> Sinds mijn 18e bezoek ik vele fora op internet die met auto’s te maken hebben, met name Honda. Deze fora hebben een hoge aantrekkingskracht, vooral als je zelf ook iets gaat bijdragen. Op auto fora ligt het voor de hand dat je naast iets over jezelf, met name iets over je eigen auto deelt. Auto’s kunnen op vele manieren een passie vormen bij mensen en wat is er mooier dan deze passie met elkaar te kunnen delen? Het internet biedt hier verschillende mogelijkheden voor.

Carage was een nieuw sociaal platform voor autoliefhebbers waar jij met je auto's centraal staat. Dit project startte ik samen met 402automotive op als co-creatie naast de reguliere werkzaamheden die ik voor hen uitvoerde.

![Carage in het AutoMaxx magazine juni 2012](412250_423044271051879_499035346_o.jpg "Carage in het AutoMaxx magazine juni 2012")

In 2012 was Facebook sterk in opkomst in Nederland. Op dat moment was het voor mij duidelijk dat steeds meer mensen Facebook zouden gaan gebruiken. Het idee met Carage was om met een Facebook webapplicatie **jouw auto een eigen profiel te geven**. Ik integreerde dat technisch met [Timeline Apps](https://developers.facebook.com/blog/post/2012/02/15/early-success-stories--timeline-apps-and-open-graph/) en [Facebook Open Graph objects & actions](https://engineering.fb.com/2012/07/06/web/under-the-hood-timeline-apps-behind-facebook-engineering/). Onder elk autoprofiel kon men berichten plaatsen die voor ieder ander weer zichtbaar werden. 

![](545096_411754932180813_514090448_n.jpg)

Carage op Facebook:

![](timeline-facebook.png)

![](12.png)

Carage als webapplicatie:

![](13.png)

![](20121107-1.png)

In mijn blog van oktober 2012 sloot ik af met 'Mobile is next' waarmee ik aangaf dat een mobiele applicatie een logische volgende stap was voor Carage. Zover is het helaas nooit gekomen. Het project Carage verdween naar de achtergrond en ging uiteindelijk offline. 

Met de komst van de mobiele app Proudwheels zie ik deze volgende stap alsnog werkelijkheid worden.

## Launch party op International Airport Breda

https://youtu.be/UN18rKbZc1Y

Op zaterdagmiddag 16 oktober was het zover: een exclusief feestje voor alle founding members en partners om samen het startschot te geven voor Proudwheels en de lancering van de mobiele applicatie. 

![](1765cbbda9a522aa054575f25530fdfb7b569379cd52845399e8e1a823355623.jpg)

## Product owner

Vanaf december 2021 verricht ik voor Proudwheels dagelijks diverse werkzaamheden als product owner. Wat houdt dat precies in?

* Nieuwe wensen vertalen naar de backlog voor het development team
* Backlog voorzien van gedetaileerde epics, user stories, features en bugs
* Prioriteiten bepalen van alle backlog items samen met de business owners 
* Feedback verzamelen over het gebruik van de app
* Mobiele app testen op diverse iOS en Android toestellen
* Wekelijks overleg met de business owners over het development proces
* Dagelijks overleg met het development team in standups, refinement sessies, retrospectives, sprint planning meetings

Daarnaast mag ik actief meedenken met het uitwerken van nieuwe ideeen voor de community. Dit hoeft niet altijd iets te zijn met betrekking tot de mobiele app. Dit kunnen ook acties of campagnes zijn waarvoor we diverse digitale middelen inzetten, zoals grote social media platformen. De meeste autoliefhebbers bevinden zich nu daar en daarom is het van belang dat ze daar kennismaken met Proudwheels.

## De Proudwheels app

Op dit moment komen er elke week een paar honderd petrolheads bij in de Proudwheels app. Sinds afgelopen december zien we een gezonde groei in de dagelijkse interactie tussen petrolheads.

![](app_screens.png)

Mijn profiel kun je vinden onder @sebastix en zo ziet mijn garage er momenteel uit:

<video controls>
    <source src="/blog/video/sebastix_pw_profile.MP4" type="video/mp4"></source>
</video>

Waarom ik geloof in het succes van Proudwheels?

* Het is een niche community rondom 1 hobby / passie waardoor het voor mensen heel leuk is om hier samen te komen.
* Ik vind het leuk om te zien dat een idee werklijkheid wordt dat al meer dan 10 jaar in mijn hoofd zit.
* Het team achter Proudwheels kent de petrolhead-markt van binnen en buiten. Zij weten als geen ander hoe je Proudwheels waardevol kunt maken op die markt.

Ben je benieuwd naar de app?\
Download deze dan in de [Apple App Store](https://apps.apple.com/nl/app/proudwheels/id1589070344) of [Google Play Store](https://play.google.com/store/apps/details?id=com.proudwheels.app).