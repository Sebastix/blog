---
layout: blog
title: Ubuntu Touch installeren op een OnePlus 6T via MacOS
date: 2021-12-07T21:00:48.390Z
description: Benieuwd naar hoe je Ubuntu Touch installeert op een OnePlus 6T?
categories: degoogle, ubuntu touch, linux
comments: "true"
---
In mijn vorige twee blogs 'Nieuwe mobiele telefoon kiezen zonder iOS of Google Android' (klik hier voor [deel 1](https://www.sebastix.nl/blog/nieuwe-mobiele-telefoon-kiezen-zonder-ios-of-android-deel-1/) of [deel 2](https://www.sebastix.nl/blog/nieuwe-mobiele-telefoon-kiezen-zonder-ios-of-google-android-deel-2/)) heb ik in de breedte onderzocht welke alternatieve toestellen en besturingssystemen er zijn voor Google Android en Apple's iOS. In deze blog ga ik de diepte in met de mobiele telefoon OnePlus 6T en hoe je hier [Ubuntu Touch](https://ubuntu-touch.io/nl/) op installeert.

Tip: zet de taal van je OnePlus 6T op Engels mocht je met onderstaande instructies aan de slag gaan. 

Installeer of downgrade Android versie 9 op je OnePlus 6T. In mijn geval moest ik downgraden van Android 11 naar Android 9. Hiervoor heb ik de volgende stappen gevolgd:

1. *[Download the OnePlus 6T rollback zip](https://oxygenos.oneplus.net/Fulldowngrade_wipe_18801_181024_2027_user_MP2_release.zip) and wait for the agonizingly slow download to completeMD5: 54ede6b7887f295cf8564904693cda34*
2. *Backups, backups, backups!*
3. *Copy the zip to the root of your internal storage*
4. *Go to Settings > System > System Updates*
5. *Tap the gear icon top right, followed by 'Local upgrade'*
6. *Tap the "Fulldowngrade..." package you just downloaded*
7. *Read the warning and tap confirm*
8. *Wipe away a little tear as you see your precious data being viciously destroyed by a progress bar*
9. *Start fresh! Hmm, that does feel pretty good...*

Bron: <https://forum.xda-developers.com/t/guide-rollback-downgrade-from-oos-android-10-q-to-stable-9-pie.4000645/>

Bij stap 3 liep ik vast en om het ZIP-bestand in de root van het bestandssysteem van de OnePlus te zetten, hier heb ik het programma [OpenMTP](https://openmtp.ganeshrvel.com/) voor gebruikt.

![](schermafbeelding-2021-12-03-om-08.22.48.png)

Middels de instructies op <https://devices.ubuntu-touch.io/device/enchilada/> heb ik het programma UBports installer geïnstalleerd op mijn Mac.

![](schermafbeelding-2021-12-03-om-08.27.25.png)

Volg de instructies in UBports installer om de volgende stappen uit te voeren in Android:

* Developers modus inschakelen
* OEM unlocking inschakelen
* USB debugging inschakelen

Wanneer je telefoon vraagt om USB debugging connection toe te staan, kies je hier voor 'allow'.

Zodra de installatie startte vanuit de UBports installer, liep ik al snel tegen deze foutmelding aan:

```bash
Error: fastboot:format: Error: formatting failed: Error: {"error":{"code":1,"cmd":"fastboot format:ext4 system"},"stdout":"Creating filesystem with 732160 4k blocks and 183264 inodes\nFilesystem UUID: 56d194bb-f629-4204-b12b-25ac9a4f9f53\nSuperblock backups stored on blocks: \n\t32768, 98304, 163840, 229376, 294912\n\nAllocating group tables:  0/23\b\b\b\b\b     \b\b\b\b\bdone                            \nWriting inode tables:  0/23\b\b\b\b\b     \b\b\b\b\bdone                            \nCreating journal (16384 blocks): done\nWriting superblocks and filesystem accounting information:  0/23\b\b\b\b\b     \b\b\b\b\bdone","stderr":"&lt; waiting for any device &gt;\nmke2fs 1.45.4 (23-Sep-2019)\nSending 'system_b' (92 KB)                         OKAY [  0.004s]\nWriting 'system_b'                                 FAILED (remote: 'Flashing is not allowed in Lock State')\nfastboot: error: Command failed"}
```

Dit zie je ook terug in de fastboot modus op de telefoon:

![](img_4601.jpg)

De device state staat daar op locked en hierdoor lukt het niet om het toestel te flashen / booten met de software die UBports installer probeert te installeren.

Na wat zoeken op internet kwam ik al snel terecht bij berichten van mensen met hetzelfde probleem: [](https://oneplus.gadgethacks.com/how-to/unlock-bootloader-your-oneplus-6-0185473/)<https://oneplus.gadgethacks.com/how-to/unlock-bootloader-your-oneplus-6-0185473/>

In de Telegram groep [UBports NEDERLANDS](https://t.me/UBports_NL) was ik ook al gewezen op het volgende issue:

> Ik had moeite met de Op5t, daarbij moest ik eerst TWRP sideloaden en daarmee dan de partities formatteren zodat de encryptie uitgeschakeld was. Anders kon de installer de bestanden van UT niet kwijt.

 *💡 **What is TWRP?** TWRP is an open source, community project. TWRP development is done by roughly 4 people at this point. We also have a large support community with many people who are willing to answer questions and help people with their devices either through our [Zulip channel](https://rebrand.ly/teamwin-recovery-zulip-community) or on forums like [xda-developers](http://forum.xda-developers.com/).*

Via [deze](https://twrp.me/oneplus/oneplus6t.html) en [deze link](https://forum.xda-developers.com/t/recovery-3-4-0-11-fajita-official-unofficial-twrp-recovery-for-oneplus-6t-stable.3861482/) staan de instructies hoe je TRWP installeert op je telefoon. Je hebt hier de software (commandline) tools Adb en Fastboot voor nodig.

De instructies om deze software te installeren en te gebruiken heb ik [hier](https://technastic.com/adb-fastboot-command-mac-terminal/) gevonden.

1. Unzip de gedownloadde ABD en Fastboot platform-tools (in mijn situatie heb ik dat in de Downloads map gedaan). Via terminal navigeer je naar deze map via `cd ~/Downloads/platform-tools`.
2. Nu kun je de volgende commando's uitvoeren:

   `./adb reboot bootloader` – this will reboot the phone in fastboot mode

   `./fastboot oem unlock` – this will unlock the bootloader and completely wipe your phone

![](img_4622.jpg)

Kies voor **unlock the bootloader** en je hebt nu de bootloader unlocked op je telefoon! Ga terug naar de instructies in de UBports installer om Ubuntu Touch te installeren.

Met het toestel in de recovery modus (UBPorts gaf aan dat ik het toestel in deze modus moest opstarten) liep ik tegen deze error aan:

```diff
Error: adb:preparesystemimage: Error: {"error":{"code":1,"cmd":"adb -P 5037 shell mkdir -p /cache/recovery"},"stderr":"mkdir: '/cache/recovery': Required key not available"}
```

Onderstaande instructies kun je overslaan, want als je verder leest onder het kopje 'De oplossing?' zul je lezen dat de installatie van Ubuntu Touch is gelukt zonder TRWP.

Hmmm, wat ben ik vergeten? **TRWP installeren!** Even terug dus. Open de pagina <https://twrp.me/oneplus/oneplus6t.html> voor de instructies hoe je TRWP te installeert.

Met `fastboot` start je het toestel op met de TRWP image. Zorg ervoor dat je nog steeds in de platform-tools map zit om het volgende commando uit te voeren:

`./fastboot boot ~/Downloads/twrp-3.6.0_9-0-fajita.img`

Let op! Het versie nummer in de bestandsnaam van de .img file kan afwijken indien je een nieuwere versie hebt gedownload.

De output als je het commando uitvoert ziet er als volgt uit:

```diff
$ > ./fastboot boot ~/Downloads/twrp-3.6.0_9-0-fajita.img
Sending 'boot.img' (32628 KB)                      OKAY [  0.704s]
Booting                                            OKAY [  0.078s]
Finished. Total time: 0.81
```

Nu wordt het ingewikkeld. Je telefoon start nu namelijk op vanaf de image die je zojuist hebt ingeladen. Nadat deze image file naar mijn OnePlus was verzonden, kreeg ik onderstaande crashdump te zien op mijn scherm.

![](img_4625.jpg)

Debugging time! [Deze discussie](https://forum.xda-developers.com/t/recovery-3-4-0-11-fajita-official-unofficial-twrp-recovery-for-oneplus-6t-stable.3861482/page-218) heb ik gevonden met 218 pagina's waarin oa dit probleem door meerdere mensen wordt gemeld.

## De oplossing?

Op een of andere manier had het toestel weer Android versie 11 geïnstalleerd. De eerdere stappen om te downgraden naar versie 9 heb ik dus opnieuw uitgevoerd.

Een aantal uur ben ik bezig geweest om TRWP te installeren. Ik kwam alleen niet verder en liep elke keer vast in boot screen. Het toestel startte niet op en het enige scherm waar ik toegang tot had, was de fastboot modus. HIer kun je komen door het toestel op starten met de powerknop + volume-up knop + volume-down knop tegelijk ingedrukt te houden.

Twee dagen heb ik het laten rusten en bedacht me toen ineens: wat gebeurt er als ik het toestel aan mijn laptop koppel in fastboot modus via USB én de UBports installer open?\
Op mijn Macbook stond de melding van UBports installer het toestel sdm845 niet werd herkend. In UBports installer kun je ook kiezen om handmatig het toestel te kiezen waarop je Ubuntu Touch wilt installeren. Via deze weg heb OnePlus 6/6T gekozen en alle instructies gevolgd. Binnen vijf minuten kreeg ik het volgende scherm te zien:

![](camphoto_1804928587.jpg.jpg)

Vervolgens startte mijn toestel opnieuw op en daar was tot mijn grote verrassing een Ubuntu Touch logo in beeld!

![](camphoto_684387517.jpg.jpg)

Gelukt! Ubuntu Touch is geinstalleerd op mijn OnePlus 6T! Eigenlijk zou ik de hele installatie nog eens helemaal vanaf scratch moeten uitvoeren met de bootloader status op locked. In de poging om TRWP te installeren heb ik namelijk veel verschillende handelinge uitgevoerd die noodzakelijk of overbodig waren om Ubuntu Touch te installeren.

![](ed775388-8900-4caa-a052-336006d671ed.jpg.jpg)

Wat zijn nu de volgende stappen nu ik een een OnePlus 6T met Ubuntu Touch heb? Dat zijn de (on)mogelijkheden van het besturingssysteem ontdekken. Bijvoorbeeld welke applicaties ik zoal kan installeren, onderzoeken hoe ik een Android applicatie kan gebruiken en of *[convergence](https://docs.ubports.com/en/latest/humanguide/other-design-considerations/convergence.html)* kan werken. To be continued!