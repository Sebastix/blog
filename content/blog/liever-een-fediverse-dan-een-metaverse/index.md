---
layout: blog
title: Liever een fediverse dan een metaverse
date: 2022-11-07T20:16:28.186Z
description: "Heb ik je aandacht te pakken? Ik ga het namelijk niet hebben over
  de metaverse, want daar ben ik geen fan van. De metaverse is een wereld die
  momenteel wordt gekapitaliseerd door Meta en andere grote tech bedrijven. Het
  wordt een gesloten wereld met een paar grote centrale spelers die de rol als
  poortwachter, arbiter en moderator op zich zullen nemen. In deze blog wil ik
  je iets vertellen over de wereld van de fediverse die hier lijnrecht tegenover
  staat. "
categories: "fediverse "
comments: "true"
---
## Het begon met [Diaspora](https://fediverse.party/en/diaspora/)

In [september 2010](https://www.kickstarter.com/projects/mbs348/diaspora-the-personally-controlled-do-it-all-distr) leerde ik Diaspora kennen en registreerde ik een account op joindiaspora.com. Het was een alternatief, decentraal, privacy-vriendelijk sociaal netwerk dat werkte met verschillende zogenaamde pods. Dit waren de servers waar je een account op kon aanmaken. Ik schreef er zelfs een [blog](https://sebastix.nl/blog/interactieontwerpen/diaspora-eerste-indruk/) over (Diaspora eerste indruk). Vandaag de dag plukken we de vruchten van het gedachtegoed en de technische uitwerking die toen zijn ontstaan. Diaspora bestaat 12 jaar later nog steeds (al [stopt](https://pod.diaspora.software/posts/4984491) de pod op [joindiaspora.com](http://joindiaspora.com/) er wel mee) en vanuit hier zijn allerlei nieuwe projecten ontstaan, waaronder open standaarden die zijn uitgewerkt door het W3C: [social web protocols](https://www.w3.org/TR/social-web-protocols/). Dankzij deze protocollen is de fediverse ontstaan.

## Hoe werkt het?

Op dit moment is de fediverse heel actueel. Zo wordt er er in de media en op verschillende blogs veel geschreven over Mastodon dat als alternatief voor Twitter wordt gezien. Mastodon maakt gebruik van de hierboven genoemde social web protocols en kun je daarom zien als onderdeel van de fediverse. Hoe het werkt wordt door [Tweakers goed uitgelegd](https://tweakers.net/nieuws/202870/mastodon-twitter-alternatief-wacht-op-doorbraak-na-musk-overname.html) 

> Mastodon is een decentraal sociaal netwerk en bestaat sinds 2016. 'Decentraal' betekent dat er diverse servers zijn, waarbij gebruikers ook een eigen server kunnen opzetten, en dat gebruikers daarop publiceren. Het geheel van servers heet de 'fediverse', een samentrekking van 'federated' en 'universe'. Gebruikers van andere servers kunnen de berichten ophalen van jouw server en ze op hun eigen feed zien. \
> … \
> Het idee achter decentrale sociale media is dat er geen centrale server is en dus geen centrale controle. Beheerders van servers bepalen zelf met welke andere servers hun gebruikers kunnen communiceren. Dat kan zo open en zo gesloten als de beheerders zelf willen. Dat geldt als een van de voordelen van decentrale sociale media. \
> … \
> Een ander voordeel dat veel naar voren komt, is controle. Gebruikers hebben meer controle over hun content en feed en hoeven geen rekening te houden met de algoritmen van grote techbedrijven. Dat geeft meer vrijheid over welke content gebruikers plaatsen. Bovendien zijn decentrale sociale media economisch onafhankelijk; als de ene server stopt, kun je op de andere weer door.

Je kunt ook deze video bekijken: 

https://youtu.be/S57uhCQBEk0

## De fediverse maakt social media interoperabel

Deze blog sluit aan bij mijn eerdere [blog ‘Maak BigTech interoperabel’](https://www.sebastix.nl/blog/maak-bigtech-interoperabel/) die ik schreef in februari 2021. Hierin pleit ik voor het bij wet interoperabel maken van data bij grote techbedrijven. Inmiddels is de wet DMA (Digital Markets Acts) actief in de Europese Unie. Toch verwacht ik niet dat grote techbedrijven nu onze data interoperabel gaan maken.\
Ik acht de kans reëler dat de fediverse op een toekomstig moment zoveel adoptie kent dat de grote techbedrijven niets anders kunnen dan de social web protocollen (zoals ActivityPub) te integreren. Daar heb ik mijn hoop op gevestigd. Een van de belangrijkste eigenschappen van deze protocollen is dat jouw data interoperabel wordt. Daarmee wordt voldaan aan een van mijn grote persoonlijke wensen maar ook die van de EU: dat jouw data van jou blijft.

## Landschap van applicaties

Deze visualisatie is uitgewerkt door [Per Axbom](https://axbom.com/)

![Fediverse](https://axbom.com/content/images/size/w2000/2022/11/fediverse-branches-axbom-12.webp)

Niet alle genoemde applicaties zijn mij bekend, maar een aantal is mij al langer bekend zoals Mastodon, Friendica en Pleroma. Allen zijn alternatieve social media toepassingen die gebruik maken van een decentrale netwerk infrastructuur gebouwd middels het ActivityPub protocol. Van andere applicaties zoals Nextcloud weet ik dat ze gebruik maken van ActivityPub om bepaalde social features in hun toepassing mogelijk te maken. Eigenlijk is de fediverse nog ontzettend nieuw en daarom is het landschap nog heel erg klein. Toch wil ik een aantal toepassingen uit dit landschap uitlichten en zelfs verder denken over toepassingen die nog niet bestaan.

**Forum revival met Lemmy** \
Lemmy is een toepassing die hetzelfde werkt als Reddit. Ofwel een forum waar je binnen topics op elkaar kunt reageren. Nog voordat social media mainstream werd, heb ik zelf uren doorgebracht op verschillende fora. Bijvoorbeeld op het forum van Civic Club Holland waar ik ook technisch beheerder van was. In de hoogtij dagen van dit forum zaten we elke avond met honderden tegelijk met elkaar te slowchatten in [Offtopic](http://www.hondacommunity.nl/forum/viewtopic.php?f=329&t=55090).\
Stiekem hunker ik al jaren naar deze goede oude tijd, al is het maar om de zeer waardevolle technische informatie die we met elkaar uitwisselde op het forum. Dit is namelijk iets wat ik niet terugvindt op de huidige populaire social media. Daarom hoop ik heel erg dat Lemmy een heuse forum wederopstand kan bewerkstelligen binnen de fediverse. Met het project [LemmyBB](https://join-lemmy.org/news/2022-11-02-_First_release_of_LemmyBB) is het zelfs een regelrechte kopie van phpBB, de forumsoftware waar ik vroeger heel veel mee heb gewerkt.

**Kindvriendelijke Mastodon / Peertube met moderatie door ouders** \
Volgens mij bestaat dit nog niet. Zelf ben ik vader van twee dochters. De oudste is bijna vier en leert razendsnel hoe een iPad of telefoon werkt. Ze vraagt regelmatig of ze een filmpje mag kijken. YouTube probeer ik zoveel mogelijk te vermijden, maar wat zijn dan kwalitatief goede, kindvriendelijke alternatieven? Een alternatief dat ik zonder zorgen door mijn dochter kan laten gebruiken zonder advertenties of dark patterns die aanzetten tot het kopen van exclusieve features? Hier zouden educatieve content makers de handen ineen kunnen slaan met ouders die toezicht willen houden op de content. Hiervoor zouden ze prima een Peertube of Mastodon instance kunnen gebruiken. Wie neemt hier de handschoen op?

**Federated search, is dat er al?** \
Een ander idee is een zoekfunctie die alle content doorzoekt in de fediverse. Dat lijkt er nu nog niet te zijn. Ik heb wel enkele projecten gevonden die het al proberen:

* <https://mastochist.nl/zoekaccount>
* <https://fedsearch.io/>

**Pixelfed, een Instagram killer** \
Ook Instagram ontkomt niet aan een fediverse versie: Pixelfed. Deze software heeft dezelfde look & feel als Instagram en daarom verwacht ik dat het overstappen van Insta naar Pixelfed snel zal verlopen.

## Op zoek naar niche communities

Ik geloof in de kracht van niche communities en daarvoor heb ik al jarenlang verschillende accounts in online groepen rondom bepaalde onderwerpen. Hieronder een lijstje van onderwerpen, hobby's en andere interesses waar ik me online binnen communities begeef.

**Honda Civic**\
Al vanaf mijn 18e bezit ik een Honda Civic waar ik werkelijk alles aan heb aangepast om er snelle rondjes mee te kunnen rijden op het circuit. Deze hobby deel ik al jaren met vele andere Honda enthousiastelingen op verschillende fora en socials.

**Wielrennen & gravel**\
In mijn (schaarse) vrije tijd maak ik graag veel kilometers op de weg en onverharde wegen op mijn Cervélo Aspero.

**Drupal**\
Met Drupal realiseer ik complexe webapplicaties en websites. Het is voor mij als een Zwitsers zakmes waarmee je alles voor elkaar krijgt.

**Bitcoin**\
In 2020 ben ik me gaan verdiepen in de technische werking van Bitcoin en blockchain. Deze zoektocht heeft me bitcoin-first gemaakt ten opzichte van alle andere cryptoprojecten. Ik meen dat Bitcoin de beste optie is voor digitaal geld in de toekomst.

**Homelab / selfhosted**\
De ambitie om minder BigTech te gebruiken zorgt ervoor dat ik digitaal zoveel mogelijk autonoom wil worden. Dit doe ik nu door een eigen computer als server te gebruiken waarop verschillende applicaties staan die ik dagelijks gebruik.

Aangezien de fediverse nog heel klein is, ben ik actief op zoek naar instances met communities die hierop aansluiten. Dit zal er waarschijnlijk toe leiden dat ik meerdere accounts zal hebben op verschillende instances om onderscheid in onderwerp te maken. Op mijn account binnen bijv. een Honda community zul je alleen Honda Civic gerelateerde content vinden.

## T﻿ot slot

De fediverse maakt social media weer sociaal. Dat is natuurlijk ook het hele idee achter de protocollen die hiervoor zijn ontwikkeld. Ik voorzie dat de adoptie van deze infrastructuur een enorme vlucht zal gaan nemen. Het wordt straks zo groot, dat zelfs de grote tech bedrijven zullen beslissen om de fediverse te omarmen. In dat scenario zullen ze deels afscheid moeten nemen van hun verdienmodel omwille van hun bestaansrecht. Zover zijn we echter nog niet, dat kan nog jaren gaan duren. De weg er naartoe zal een bumpy ride worden met veel weerstand. Want zolang BigTech de technologie van de fediverse niet adopteert, kunnen wij ook niet communiceren met een ander die er wél voor kiest om op Facebook of Instagram te blijven.\
De toekomst van social media ziet er met deze eigenschappen als volgt uit:

* Decentraal dankzij de social web protocollen
* Verbonden niche communities die elk op een eigen server actief zijn
* Privacy vriendelijk dankzij opensource en transparantie
* Chronologische tijdlijnen zonder algoritmes
* J﻿ouw data blijft van jou

W﻿anneer zie ik jou in de fediverse? Dit zijn op het moment mijn actieve accounts:

* <https://nwb.social/@sebastian/>
* <https://mastodon.sebastix.dev/@sebastix/>

Geraadpleegde bronnen / meer naslagwerk: 

* <https://fedi.foundation/2022/09/social-networking-reimagined/>
* <https://notes.smallcircles.work/c8QVB3mkQByLgNZf_HotIQ#>
* <https://www.w3.org/TR/social-web-protocols/>
* <https://publicspaces.net/2022/11/03/een-how-to-mastodon-en-fediverse-voor-publieke-organisaties/>
* <https://axbom.com/mastodon-tips/>
* <https://doctorow.medium.com/how-to-leave-dying-social-media-platforms-9fc550fe5abf>
* <https://www.nrc.nl/nieuws/2022/11/04/waar-blijft-de-europese-tiktok-a4147293> 
* <https://bitcoinhackers.org/@mastodonusercount>
* <https://axbom.com/fediverse/>
* <https://archive.ph/oGwsP>