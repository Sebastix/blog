---
layout: blog
title: Wat maakt mij een autodidact? Leren in het publieke domein
date: 2022-10-18T11:04:20.136Z
description: Technische vaardigheden leer ik hoofdzakelijk door veel te lezen en
  te doen. Niet door cursussen of opleidingen te volgen. Ik gebruik informatie
  die te vinden is in het publieke domein om te leren.
categories: autodidact
comments: "true"
---
<https://www.swyx.io/learn-in-public/>

Eind 2020 ben ik na ~8 jaar opnieuw gaan schrijven op mijn blog. Dit doe ik ter zelfreflectie en om mijn hoofd leeg te maken. Ik kies ervoor om het publiekelijk te delen, ik maak het ‘opensource’. Wat ik evan leer?

* Je doet het in de eerste plaats voor jezelf ipv voor anderen. 
* Doe je best om zo correct mogelijk te zijn, want dan doe je het nooit ‘fout’. Wees bereid om kritiek van anderen te ontvangen en ga met hen in dialoog. Hier leer je veel van, het maakt je minder fragiel.
* Koester je beginnersmentaliteit, hierdoor blijf je nieuwsgierig en bekijk je alles met een open vizier.

### P﻿raktische wijsheid

L﻿eren doe ik door te doen. Op de manier vergroot ik mijn praktische wijsheid.

> ***Phronèsis*** is morele bedachtzaamheid of praktische wijsheid bij het proberen te sturen van het eigen handelen en dat van anderen.

Bron: <https://nl.wikipedia.org/wiki/Phron%C3%A8sis>

W﻿ist je dat doen ook de beste manier van denken is?  

https://youtu.be/GUTM8xnh7DM