---
layout: blog
title: Aan de slag met Ubuntu Touch op een OnePlus 6T
date: 2021-12-24T08:45:00.000Z
description: Aan de slag met Ubuntu Touch op een OnePlus 6T
categories: Ubuntu Touch
comments: "true"
---
## Open Store

<https://open-store.io/>\
Dit is de app store voor Ubuntu Touch. In deze app store heb ik de volgende apps gevonden die ik nu gebruik:

* [TELEports](https://open-store.io/app/teleports.ubports) als app voor Telegram
* [Waydroid Helper](https://open-store.io/app/waydroidhelper.aaronhafer)
* [FluffyChat](https://open-store.io/app/fluffychat.christianpauly) als Matrix messenger
* [TTRSS-reader](https://open-store.io/app/net.gsantner.ut.ttrssreadermobile) om RSS feeds (blogs) van websites uit te lezen

## Fix voor de notch

Handig, een visuele fix voor de notch op je OnePlus 6T!\
<https://github.com/JamiKettunen/unity8-notch-hax>

## SSH

Inloggen via SSH op je toestel, volg deze instructies: <https://docs.ubports.com/en/latest/userguide/advanceduse/ssh.html>

## Android apps installeren met Waydroid en F-Droid

<https://waydro.id/>\
<https://docs.waydro.id/usage/install-on-desktops>

Uiteindelijk heeft [deze link](https://forums.ubports.com/topic/6764/anbox-halium-or-waydroid-no-support-for-ut/11) me verder geholpen om Waydroid te installeren:

```bash
sudo -s
sudo mount -o remount,rw /
apt update
apt install waydroid -y
waydroid init
```

Mocht je tegen een foutmelding aanlopen, probeer de init dan opnieuw met `waydroid init -f`.

Helaas is het me niet gelukt om Waydroid werkend te krijgen. Nadat ik op Telegram navraag had gedaan of het bij iemand werkt op een OnePlus 6T werd me al snel duidelijk dat ik niet de enige ben:

![](schermafbeelding-2021-12-23-om-16.35.45.png)

Zonder werkende Android apps moet ik helaas concluderen dat de OnePlus 6T met Ubuntu Touch voor mij nog niet geschikt is om dagelijks te gebruiken als vervanger voor mijn iPhone.

Pas als ik zicht heb op welke Android apps ik kan gebruiken, kan ik mijn oordeel heroverwegen. Voor nu rest mij het wachten op een werkende Waydroid installatie. Hopelijk is dat binnen een paar weken het geval, zodat ik jullie meer kan vertellen over het gebruik van Android apps op Ubuntu Touch.