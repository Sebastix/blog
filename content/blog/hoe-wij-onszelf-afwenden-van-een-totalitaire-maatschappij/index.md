---
layout: blog
title: Hoe wij onszelf afwenden van een totalitaire maatschappij
date: 2022-08-16T20:17:43.572Z
description: In januari 2021 uitte ik mijn zorgen over hoe we langzaam afglijden
  naar een totalitair systeem. Dit omdat we veel massasurveilliance toelieten in
  ons leven wat te wijten is aan een collectieve angst voor een virus. Nu
  anderhalf jaar later zie ik langzaam een ommekeer in deze trend.
categories: totalitarisme systeem
comments: "true"
---
Het lijkt erop dat onze maatschappelijke, collectieve wens naar meer controle en autoriteit lijkt af te nemen. Om mij heen zie ik juist een groeiende behoefte aan zelfredzaamheid en autonomie. Het techno-optimisme lijkt plaats te maken voor techno-realisme. In [mijn blog vorig jaar](https://www.sebastix.nl/blog/duwt-massa-surveillance-ons-naar-een-totalitair-systeem/) schreef ik:

> Volgens mij is het tijd voor meer realisme. Zelf óók nadenken in plaats van het denkwerk altijd aan anderen overlaten. Dus niet zomaar die algemene voorwaarden accepteren. Wees kritisch, luister aandachtig en stel vragen. Bemoei je niet met de ander maar neem je eigen verantwoordelijkheid.

Daar wil ik nu iets aan toevoegen: **spreek je uit**. Dat is de kernboodschap die ik heb gelezen in het boek 'De psychologie van het totalitarisme' van klinisch-psycholoog Mattias Desmet. Zodra niemand zich meer uitspreekt, dan is het totalitairisme compleet. Gelukkig zie, ontmoet en hoor ik steeds meer mensen die zich uitspreken tegen het huidige beleid. Een beleid waar het mechanistisch denken centraal staat. Een manier van denken en doen binnen een ideologie die vrijwel alleen gebaseerd is op de rede. Een ideologie waar onze welvaart - *growth at any cost* - veel belangrijker is dan ons welzijn. 

![](photo_2022-08-17-09.24.16.jpeg)

Ondanks dit positief inzicht, ben ik niet zorgeloos over de toekomst. Ik acht de kans vrij groot dat we over een paar maanden opnieuw in een angstcrisis zitten. Die angst zal weer zorgen voor meer surveillance en dat zorgt weer voor meer angst, aldus Mattias Desmet. Daar ben ik het mee eens. Ik kan je van harte aanbevelen om zijn boek eens te lezen. Als klinisch-psycholoog weet hij feilloos een haarscherpe uitleg te geven over hoe totalitarisme werkt. In zijn boek verwijst hij ook met enige regelmaat naar het werk [The Origins of Totalitarianism van Hannah Arendt](https://en.wikipedia.org/wiki/The_Origins_of_Totalitarianism) uit 1951. De schrijfster waarschuwde toen al voor een totalitair systeem onder leiding van een technocratische elite.

Wat je ook kunt doen, is deze [uitgebreide samenvatting](https://koneksa-mondo.nl/2022/08/12/mattias-desmet-over-de-psychologie-van-totalitarisme) van het boek van Marco Derksen lezen.

Nog korter om te lezen is [dit interview](https://www.vprogids.nl/boeken/artikelen/2022/Smachten-naar-controle-en-autoriteit.html) met Mattias Desmet in de VPRO gids.

Mocht je het boek, bovenstaande samenvatting of interview gelezen hebben, dan is mijn vraag aan jou - net als in mijn blog in 2021 - of jij in een totalitaire samenleving zou willen leven? Ik hoor het graag!