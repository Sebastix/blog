---
title: "www.sebastix.nl door de jaren heen"
date: "2010-01-20T00:00:10.000Z"
description: "Via webarchive.org:


As we speak, wordt er momenteel ook gewerkt aan een frisse nieuwe update op het gebied van vorm..."
categories: [internet]
comments: true
---

Via webarchive.org:  
[![My website line](http://farm3.static.flickr.com/2682/4286292616_13829b83cb_b.jpg)](http://www.flickr.com/photos/sebastix/4286292616/ "My website line")  
  
 As we speak, wordt er momenteel ook gewerkt aan een frisse nieuwe update op het gebied van vormgeving. Als eerste op de planning staan mijn visitekaartjes en zal daarna proberen het vernieuwde ontwerp door te trekken in mijn website en andere uitingen.
