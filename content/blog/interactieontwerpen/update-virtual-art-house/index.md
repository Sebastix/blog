---
title: "Update Virtual Art-House"
date: "2009-07-17T14:30:55.000Z"
description: "

Hierbij enkele screenshots van het Virtual Art-House nadat de ruimte flink vergroot is en we er dus een heleboel wer..."
categories: [internet]
comments: true
---

![](../../../images/vah-logo.jpg "Virtual Art-House")  
  
 Hierbij enkele screenshots van het Virtual Art-House nadat de ruimte flink vergroot is en we er dus een heleboel werken (zonder inhoud) in hebben kunnen zetten. Het begint al ergens op te lijken ;-)  
  
[![Screenshot_17](http://farm4.static.flickr.com/3516/3728680377_a6764f1b23_t.jpg)](http://www.flickr.com/photos/sebastix/3728680377/ "Screenshot_17")[![Screenshot_16](http://farm3.static.flickr.com/2533/3729482890_2d6b0281b9_t.jpg)](http://www.flickr.com/photos/sebastix/3729482890/ "Screenshot_16")[![Screenshot_14](http://farm3.static.flickr.com/2581/3728680135_f3ef137d81_t.jpg)](http://www.flickr.com/photos/sebastix/3728680135/ "Screenshot_14")[![Screenshot_11](http://farm3.static.flickr.com/2480/3729482276_a9537bd4fe_t.jpg)](http://www.flickr.com/photos/sebastix/3729482276/ "Screenshot_11")[![Screenshot_9](http://farm4.static.flickr.com/3491/3728679563_d984651a0b_t.jpg)](http://www.flickr.com/photos/sebastix/3728679563/ "Screenshot_9")[](http://www.flickr.com/photos/sebastix/3728679103/ "Screenshot_6")  
  
[![Screenshot_6](http://farm3.static.flickr.com/2641/3728679103_3fb231229f_t.jpg)](http://www.flickr.com/photos/sebastix/3728679103/ "Screenshot_6")[![Screenshot_4](http://farm3.static.flickr.com/2441/3728678867_018d69e9fa_t.jpg)](http://www.flickr.com/photos/sebastix/3728678867/ "Screenshot_4")[![Screenshot_7](http://farm3.static.flickr.com/2518/3728679259_d5b29360e1_t.jpg)](http://www.flickr.com/photos/sebastix/3728679259/ "Screenshot_7")[![Screenshot_1](http://farm3.static.flickr.com/2494/3728678531_2b07fa5c34_t.jpg)](http://www.flickr.com/photos/sebastix/3728678531/ "Screenshot_1")[![Screenshot_3](http://farm4.static.flickr.com/3475/3729481358_9b17426aea_t.jpg)](http://www.flickr.com/photos/sebastix/3729481358/ "Screenshot_3")
