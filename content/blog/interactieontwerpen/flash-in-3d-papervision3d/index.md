---
title: "Flash in 3D: Papervision3D"
date: "2009-03-06T19:13:28.000Z"
description: "Voor mijn stage ben ik wat onderzoek aan het doen in virtuele ruimtes en mogelijke benaderingen in een drie dimensionale..."
categories: [internet]
comments: true
---

Voor mijn stage ben ik wat onderzoek aan het doen in virtuele ruimtes en mogelijke benaderingen in een drie dimensionale wereld. Hieronder heb ik wat onderzoekresultaten van het web onder elkaar gezet met bronnen en uitleg over de Papervision3D engine in Flash.  
  
  
  
**Officiele website:**  
<http://www.papervision3d.org/>  
  
**Weblog:**  
<http://blog.papervision3d.org/>  
  
**Developer:**  
<http://dev.papervision3d.org/>  
  
**Documentatie:**  
<http://papervision3d.googlecode.com/svn/trunk/as3/trunk/docs/index.html>  
  
**Nieuws:**  
<http://dailypv3d.wordpress.com/>  
  
**Tutorials:**  
<http://pv3d.org/>  
<http://www.papervision2.com>  
<http://pv3world.com/blog/>  
  
**Examples:**  
<http://pv3d.org/category/examples/>  
<http://blog.zupko.info/>  
  
 Voorbeelden met codes:  
**Grid:**  
<http://pv3d.org/2009/02/01/grid/>  
  
**3D grid:**  
<http://pv3d.org/2009/02/01/3d-grid/>  
  
**Spelen met bollen:**  
<http://pv3d.org/2009/01/15/rolling-a-sphere-with-keyboard-and-box2dflash-physics/>  
  
**Camera tweening:**  
<http://pv3d.org/2009/01/03/tweening-the-camera-and-tweening-lookat/>  
  
**Tweening tussen planes:**  
<http://pv3d.org/2008/12/28/click-then-tween-camera-to-plane/>  
  
**Drag and drop interactie:**  
<http://pv3d.org/2008/12/20/papervision3d-with-box2dflash-part-3-adding-mouse-interaction/>  
  
**Drag and drop planes:**  
<http://pv3d.org/2008/12/19/dragging-planes-and-lookat-camera/>  
  
**Camera movement:**  
<http://pv3d.org/2008/12/04/neat-camera-and-arrows-thingy/>  
  
**Interactieve kubus:**  
<http://papervision2.com/10-advanced-interactivity/>  
  
**Camera control met WASD**:  
<http://papervision2.com/8-keyboard-interaction-to-move-the-camera/>  
  
**Inspiratie voorbeelden:**  
<http://www.kt28.net/>  
<http://www.whitevoid.com/application>  
<http://www.tbc.us/>  
 en nog veel meer...
