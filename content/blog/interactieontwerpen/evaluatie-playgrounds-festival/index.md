---
title: "Evaluatie Playgrounds Festival"
date: "2009-10-11T09:00:46.000Z"
description: "

Afgelopen 8 &amp; 9 oktober heb ik het jaarlijks terugkomend Playgrounds Festival bezocht in Tilburg. Ik wil Leon va..."
categories: [internet]
comments: true
---

![](/images/playgrounds2009-top.jpg "Playgrounds")  
  
 Afgelopen 8 &amp; 9 oktober heb ik het jaarlijks terugkomend Playgrounds Festival bezocht in Tilburg. Ik wil Leon van Rooij (festival directeur) bedanken voor het beschikbaar stellen van een vrije ticket en het prachtig neerzetten van een zeer inspiratievolle festival op het hoogste niveau!  
  
**Donderdagochtend AV netwerkconferentie**  
 Op donderdagochtend tijdens een besloten netwerkconferentie werd de vraag gesteld welke rol Audiovisueel werk het beste kan krijgen in de provincie Brabant. De provincie wil zich namelijk creatief onderscheidend gaan profileren met Audiovisueel werk en is zoekende naar een juiste vorm voor dit idee. Zelf ben ik natuurlijk geen AV'er, maar interessant was wel dat de meningen over het idee en de uitwerking hiervan heel erg divers zijn en er nog een heleboel tekortkomingen zijn (blijkbaar) vanuit de beroepspraktijk. Vooral de presentatie van TNO met een heleboel cijfers leverde veel gespreksstof op. Uit deze discussie heb ik wel een conclusie getrokken dat er een sterke behoefte is naar een soort platform of middel waarmee verschillende disciplines gerelateerd aan de AV-industrie kunnen netwerken. Hierop aansluitend noemde Leon ook dat er gewerkt wordt aan een community platform (virtueel) gelinked aan het Playgrounds Festival. Ergens wel mooi maar ook drukverhogend \*in positieve zin\* naar ons projectgroepje toe aangezien wij hier nu druk mee bezig zijn.  
*Belangrijkste conclusie na deze discussie:*  
 Provincie Brabant wilt AV makers en bijbehorende relaties binnen Brabant bundelen en hiermee de provincie ook op de kaart zetten. Kritiek punt was dat er veel van deze markt zich buiten Brabant bevindt. En terecht vind ik; creatieve makers kennen geen grenzen dus ze zullen ook hoe dan ook zich ook altijd buiten Brabant bevinden met hun eigen connecties. Ik stel dat geen enkele creatieve kunstenaar/discipline zich zomaar in een geografisch kader laat duwen; dit beperkt namelijk de creativiteit.  
  
**Vrijdagochtend E-Culture werkconferentie**  
 In mijn ogen was dit het meest interessante van de gehele festival voor mij persoonlijk. De gehele vraagstelling van wat en waar mijn vakgebied (interactie ontwerper, nieuwe media\*kunst\*) zich precies moet bevinden werd gesteld door het BKKC (<span>Brabants Kenniscentrum Kunst en Cultuur). Er is een beleid nodig en een platform voor werken die worden gemaakt. Omdat het karakter van deze 'nieuwe' vorm vaak multidisciplinair, dynamisch van aard is, is het dus bijzonder lastig om hier enkele kaders omheen te zetten. Daar ben ik zelf ook al wat langer bewust van omdat het keer op keer vrij lastig is om uit te leggen wat je nu precies doet. Daan Roosengaarde was ook spreker in de aanloop naar deze paneldiscussie en liet veel eigen werk zien die precies passen binnen het karakter wat zich dan E-Arts of E-cultuur noemt. Deze definitie wordt nu gegeven aan nieuwe mediakunsten. Ik moet er nog even aan wennen maar aangezien ik zelf geen betere definitie kan geven nu, zal ik deze ook vaker gaan gebruiken (net zoals ik de term interactie ontwerper voor mezelf definier, moet ook de context een naam krijgen).  
 Het werk van Daan is super, dat wist ik al. Maar ik had Daan nog nooit horen spreken, via mede-student Rob wist ik al wel dat hij een vernieuwende visie op techniek en natuur heeft. Deze visie werd me nu pas eindelijk echt duidelijk en het viel me vooral dat ook Daan vooral zich laat inspireren en motiveren door het proces! Het werk is namelijk nooit af, je bezoekers van je installaties worden onderdeel van en creëren nieuwe inzichten op je eigen werk en uiteindelijk ook op de omgeving eromheen. Ik had wel even een kippenvel momentje op dat moment, omdat ik dat precies hetzelfde ook vaak ervaar als ik anderen zie spelen/interacteren met mijn eigen gemaakt werk. Dat stukje kippenvel bevestigde wel een soort onderbewust gevoel bij mij.  
 Later in de discussie werd ook duidelijk dat er ergens bij Daan een bepaalde frustratie zat als het gaat om het stukje beleidsbepaling rondom E-Cultuur. Hoe geef je deze vorm en hoe dient een stukje overheid/beleidsinstanties om te gaan met deze nieuwe vorm van kunst (ofwel KOENST aldus Daan). Eigenlijk was heel het panel er wel over eens dat er meer vragen zijn dan antwoorden rondom dit thema. Het BKKC weet het ook niet en vraagt ook letterlijk wat ze ermee aan moeten.  
 Ergens heb ik hier ook wel een eigen visie op; omdat E-cultuur met zoveel disciplines werkt is het ook belangrijk om met een bepaalde visie (nog nader te bepalen) hiermee een dialoog aan te gaan op dat vlak. Net zoals de kunstenaars in dit vakgebied, dienen ook de instanties zich zeer flexibel op te stellen om iets nieuws te kunnen creëren. Val in ieder niet teveel terug op bestaande profielen of structuren die van toepassing zijn voor andere x'en. De visie en voorstellen van Hans van Driel (UvT) vond ik hierin ook bijzonder vruchtbaar voor een vernieuwend stukje beleid welke gevormd moet worden.</span>  
  
**Festival programma**  
 Naast deze twee bijeenkomsten heb ik ook bijna het volledige festivalprogramma gezien. Zelf vond ik de presentatie van Daan Roosengaarde het hoogtepunt van Playgrounds, maar dat wil niet zeggen dat al het andere geen indruk op me heeft gemaakt. Integendeel, ik heb veel werk gezien en hoe innovatief sommige makers wel niet kunnen zijn bij het vinden van nieuwe concepten en uitvoeren ervan. Fantastisch, vooral het werk van DVEIN vond ik er perfect op aan sluiten.  
  
 Goed, naast dit korte verslag wil ik even mijn eigen bevindingen en conclusies op een rijtje zetten:  
  
 1. Er wordt bijzonder veel gewerkt met zogenaamde agencies; bedrijfjes die bedrijven aan creatievelingen koppelen na een zoektocht (why!?)  
 2. ineens bijzonder veel Brabant in beeld; blijkbaar ziet de provincie een enorm belang op innovatief, creatief cultureel vlak  
 3. had graag wat meer diversiteit gezien (75% CG animatie/visual effects) + kleine expo!?  
 4. Playgrounds STAAT ALS EEN HUIS (citaat Rob welke ik volledig deel) zeker tov vorig jaar  
 5. er is behoefte aan een netwerktool tijdens zo'n festival (wie zijn die mensen nu eigenlijk om me heen en wat doen ze?)  
  
 En nog een samenvatting van enkele websites die ik wil delen:  
[http://www.studioroosegaarde.net/](http://onesize.nl/)  
<http://www.impakt.nl/>  
<http://www.dish2009.nl/>  
<http://www.baltanlaboratories.org/>  
<http://www.virtueelplatform.nl/>  
<http://www.bkkc.nl/>  
<http://www.dvein.com/>  
<http://loulouandtummie.com/>  
<http://www.onedotzero.com/>  
<http://onesize.nl/>
