---
title: "SIARR - werking"
date: "2007-09-26T14:29:09.000Z"
description: "De slaaf van de toekomst in het huis van de toekomst heeft een naam: SIARR (Slave in Augmented Reality Robot).
Hoe hand..."
categories: [internet]
comments: true
---

De slaaf van de toekomst in het huis van de toekomst heeft een naam: **SIARR** (Slave in Augmented Reality Robot).  
*Hoe handelt SIARR?*  
  
  
 Wanneer je SIARR aanschaft, is zijn geheugen helemaal leeg (net zoals een kind die wordt geboren). Vanaf het moment dat je SIARR aanzet, begint SIARR's ontwikkeling. Hij begint te leren. SIARR leert van een ouder. De ouder is zijn eigenaar, de persoon die SIARR heeft aangeschaft. SIARR moet deze wel eerst herkennen, dit gebeurt aan de hand van een foto die wordt genomen bij de setup van hem. *(zie handleiding setup 1.0)* Na de setup moet de eigenaar de bijgeleverde bril gaan gebruiken om SIARR verschillende omgevingen, objecten te laten herkennen. *stap 1 **meten*** Wanneer SIARR een object in een omgeving herkent *(stap 2 **visualiseren**)* kan de volgende stap genomen worden; *stap 3 **analyseren***. Na deze analyse volgen stap 1 en 2 weer als de eigenaar een handeling voordoet. Dan pas kan SIARR overgaan tot *stap 4 **interacteren***.  
 Dit proces dient voor elk object te worden herhaalt om SIARR zo zelfstandig mogelijk te kunnen maken.  
  
\- Handleiding SETUP  
  
**Hoe werkt SIARR?**

2. 1 meten
  
 Meten gebeurt met sensoren; lens, licht, warmte  
4. 2 visualiseren
  
 Visualiseren gebeurt met de software, gemeten waardes worden digitaal  
6. 3 analyseren
  
 Analyseren gebeurt met de software, visualisaties worden onderverdeeld in variabelen  
8. 4 interacteren
  
 Interacteren gebeurt fysiek door SIARR met zijn technische hulpmiddelen  
  
![SIARR flowchart](http://www.interactieontwerpen.nl/_sebastian/siarr/flowchart_werking.jpg)  
  
 Meer onderzoek:  
<http://www.se.rit.edu/~jrv/research/ar/index.html>  
<http://www.se.rit.edu/~jrv/research/ar/video.html>  
<http://www.friz.com/projects/telerobot/>  
[http://www.waarmaarraar.nl/pages/re/19231/Seks\_met\_een\_robot\_in\_de\_toekomst.html](http://www.waarmaarraar.nl/pages/re/19231/Seks_met_een_robot_in_de_toekomst.html)  
<http://thoughtware.tv/videos/show/831>
