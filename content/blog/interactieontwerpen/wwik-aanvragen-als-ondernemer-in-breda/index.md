---
title: "WWIK aanvragen als ondernemer in Breda"
date: "2010-12-20T15:09:25.000Z"
description: "Deze week heb ik besloten om toch een uitkering aan te vragen en in dit geval wel een kunstenaarsuitkering genaamd de WW..."
categories: [internet]
comments: true
---

Deze week heb ik besloten om toch een uitkering aan te vragen en in dit geval wel een kunstenaarsuitkering genaamd de WWIK (Wet werk en inkomen kunstenaars). De kans is groot dat deze regeling wordt afgeschaft door de huidige regering onder leiding van de heer Rutte vanaf 1-1-2012. Vanuit het bezuinigingsperspectief wordt er behoorlijk in de culturele sector gesneden. Ik heb daar geen uitgesproken mening over, maar vanuit mijn persoonlijke situatie stel ik vast dat ik mezelf nog meer ondernemend moet gaan opstellen met de activiteiten die ik verricht in deze sector.  
  
 Sinds ik afgelopen zomer ben afgestudeerd aan de kunstacademie AKV/St.joost heb ik gekozen voor het volledig zelfstandig ondernemerschap vanuit mijn eenmanszaak Sebastix. Helaas heb ik nog niet de inkomsten die ik graag zou willen verdienen en het is zelfs zo als ik afga op 2010, heb ik ook recht op een WWIK als academie-verlater omdat mijn gemiddelde verdiensten niet meer zijn dan de norm die je maximaal mag bijverdienen om in aanmerking te komen voor een WWIK.  
  
 Als net begonnen zelfstandig ondernemer benut ik nu het middel genaamd de WWIK om een extra financieel inkomen voor mij als persoon te genereren. Netto betekent dat in mijn geval dat ik (dit gaat wellicht veranderen op 1-1-2011) netto € 636,53 ontvang per maand. Bovenop dat bedrag mag ik maximaal € 1525,24 bijverdienen als inkomen. Deze bedragen zijn per situatie anders, in mijn situatie is dit het minste wat je kunt 'krijgen'. Meer info: [http://www.kunstenaarsenco.nl/wwik/normbedragen/](http://www.kunstenaarsenco.nl/wwik/normbedragen/ "Klik hier")  
  
 Momenteel ben ik bezig met de aanvraag. Dit loopt via de centrumgemeente Breda. Meer informatie vond ik hierover op de website van gemeente Breda: [klik hier](http://www.breda.nl/index.php?simaction=content&mediumid=1&pagid=73&pagid=73&type=product&product_type=simloket&product=81089514-86B4-424E-8362-792C335C1E3F). *Lees dat goed door! Daar staat precies welke regels er van toepassing zijn als je de WWIK hebt en waar je rekening mee dient te houden. Ik adviseer om hierover belastingtechnisch / sociaal-juridisch advies voor in te winnen.* **Maandag 21-12-2010:**  
 Zojuist ben ik langsgeweest bij het Wegwijs Loket bij de gemeente en daar heb ik een telefoonnummer 076-5299281 gekregen van mevrouw A van den Broek van sociale zaken. Zij doet daar de WWIK aanverwante zaken. Helaas is ze niet altijd aanwezig en bereikbaar, maar wel tussen 09.00 en 10.00 op dinsdag, donderdag en vrijdag.  
**Dinsdag 22-12-2010:**  
Na telefonisch contact met mevrouw A van den Broek komt er nu een aanvraagformulier voor de WWIK naar mijn adres toe. Nadat ik dit formulier weer terug is bij de gemeente zal ik uitgenodigd worden voor een gesprek waarin alle ins en outs worden besproken.  
 **Januari:**  
 Heel mijn hebben en houden met alle bewijsstukken opgestuurd naar de gemeente; dit is een flink dossier geworden. Hierop aansluitend een kort gesprek gehad bij de gemeente en daar werd me ook verteld dat het 99% zeker is dat per 1-1-2012 alle WWIK uitkeringen vervallen. In omstreeks maart 2012 volgt er wederom een onderzoek door de gemeente of ik mijn uitkering moet terugbetalen ja of nee aan de hand van alle gegevens van 2011.  
**Februari:**  
 Nogmaals een formulier ingevuld ditmaals over mijn inhoudelijke werkzaamheden als kunstenaar/ontwerper. Deze gegevens worden gebruikt door Cultuur &amp; Ondernemen om een beroepsmatigheidsonderzoek te doen. Nu is het afwachten op de uitkomst van dit onderzoek en uiteindelijke conclusie of ik wel of niet de WWIK ontvang.  
  
***Wat ga ik doen met de WWIK als zelfstandig ondernemer?***  
  
 Aangezien ik nog maar een paar maanden fulltime zelfstandig onderneem na mijn voltijd studie, heb ik me de laatste maanden vooral beziggehouden met zaaien. Komende maanden is het voor mij belangrijke zaak dat ik ga oogsten. Ik weet nog niet hoeveel euro ga verdienen, dus ik weet ook niet of ik straks de WWIK moet terugbetalen ja of nee (dit wordt pas na 1 jaar getoetst na de aanvraag). Als ondernemer is dit een bepaald risico afwegen tegenover de baten. Bewust heb ik een aantal maanden afgewacht hoe mijn zaken gingen lopen, nu is het moment dat ik kies voor stuk inkomen waar ik vrijwel niets extra's voor hoef te doen. Als het oogsten straks gaat beginnen, zorg ik er in ieder geval voor dat ik de WWIK bedragen ergens parkeer zodat ik het kan terugbetalen. Dit is voor mij de meest ideale situatie en een luxe-probleem. Echter acht ik de kans groter dat ik in 2011 nog steeds te weinig verdien, dan weet ik ook dat de WWIK mag houden en deze 'lening' wordt omgezet in een soort 'schenking/gift/uitkering'. Ieder zal voor zichzelf zijn eigen situatie moeten uitlichten om tot de conclusie te kunnen komen hoe jij jouw WWIK voor jezelf kunt laten werken.  
  
**Ben ik wel een kunstenaar?**  
 Daar kun je lang over discussiëren, net zoals over de definitie van kunst. Ik hou van discussie, dus wees vrij om te reageren hieronder. Ik vind mezelf in ieder geval wel een kunstenaar. Een kunstenaar3.0 ;-)

  
- Meer informatie over de WWIK bij Cultureel Ondernemen / Kunstenaars&amp;Co: <http://www.cultuur-ondernemen.nl/wwik>
  
- Weten bij welke centrumgemeente jij de WWIK kunt aanvragen? <http://www.kunstenaarsenco.nl/wwik/wwik-aanvragen/>
  
- Ook handig: <http://www.ondersteuningwwik.nl/>
  
- Rijksoverheid aan het woord: <http://www.rijksoverheid.nl/onderwerpen/kunst-en-cultuur>
  
