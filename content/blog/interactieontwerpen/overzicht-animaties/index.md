---
title: "Overzicht animaties"
date: "2007-11-01T14:17:58.000Z"
description: "Hieronder vind je het overzicht van alle animaties die we hebben moeten maken voor de les animatie afgelopen kwartaal. H..."
categories: [internet]
comments: true
---

Hieronder vind je het overzicht van alle animaties die we hebben moeten maken voor de les animatie afgelopen kwartaal. Het is grotendeels allemaal stopmotion, doel van de opdrachten was om bepaalde bewegingen te begrijpen en deze op de juiste manier te visualiseren zodat je weet hoe een beweging werkt.  
  
  
**Alles in 1:**   
http://www.interactieontwerpen.nl/\_sebastian/animatie/stopmotion.mov  
  
**Schaar loopje:**   
http://www.interactieontwerpen.nl/\_sebastian/animatie/Schaar.mov  
  
**Slinger:**   
http://www.interactieontwerpen.nl/\_sebastian/animatie/slinger.avi  
  
**Versnelling:**   
http://www.interactieontwerpen.nl/\_sebastian/animatie/versnelling.swf  
  
**Sprong:**   
http://www.interactieontwerpen.nl/\_sebastian/animatie/sprong.avi  
  
**Klei 1:**   
http://www.interactieontwerpen.nl/\_sebastian/animatie/klei.avi  
  
**Klei 2:**   
http://www.interactieontwerpen.nl/\_sebastian/animatie/MeerKlei.avi  
  
**Stuiterbal:**  
http://www.interactieontwerpen.nl/\_sebastian/animatie/stuiterballetjes\_stopmotion.swf  
  
**Geld:**   
http://www.interactieontwerpen.nl/\_sebastian/animatie/Geldding.avi
