---
title: "De creatieve wijk"
date: "2009-05-02T12:51:05.000Z"
description: "Tussen al mijn werkzaamheden door ben ik ook al een tijdje op zoek naar een eigen werkruimte die ik kan huren. In de tus..."
categories: [internet]
comments: true
---

Tussen al mijn werkzaamheden door ben ik ook al een tijdje op zoek naar een eigen werkruimte die ik kan huren. In de tussentijd heb ik al verschillende dingen bekeken en de meeste locaties zijn vaak te duur die aan mijn eisen voldoen of voldoen niet aan mijn eisen als ze betaalbaar zijn.  
 Nu ben ik deze wijk in contact gekomen met De Creatieve Wijk, een initiatief welke net de stap heeft gemaakt van concept naar iets tijdelijks fysiek in de Molenstraat in Roosendaal. Zoals bekend, ik werk al in die straat op het Zinc en de Creatieve Wijk gaat creatieve ondernemers cq kunstenaars de mogelijkheid geven om daar hun eigen studio, atelier, werkruimte, kantoor te vestigen. Ik heb direct contact gezocht en zeker gezien de locatie en potentie die het in zich heeft, is dit tot nog toe de beste mogelijkheid die mij heeft voorgedaan om mijn werk te verhuizen naar een plek die daarvoor geschikt is.  
 Ik weet niet aan wat voor termijn ik moet denken, mocht ik daar aan de slag kunnen maar het zal nog wel een tijdje duren aangezien er verschillende belangen zijn met verschillende partijen. Ik heb in ieder geval mijn zegje daar liggen en het liefst zou ik natuurlijk zo snel mogelijk aan de slag gaan. En dat kan, samen met andere creatieve mensen. Dus ben je misschien geinteresseerd? Hier kun je meer informatie vinden;  
  
 Website: [http://www.creatievewijk.nl/](http://picasaweb.google.com/Crebucon/CreatieveWijk?feat=email#)  
 LinkedIn: <http://www.linkedin.com/groups?gid=1855809>  
  
 Persberichten:  
 2 mei 2009 - [Ruim baan voor cultuur (BN/deStem)](http://www.bndestem.nl/regio/roosendaal/4896672/Gezocht-voor-centrum-creatieve-ondernemers.ece?goback=.anh_1855809)  
 30 april 2009 - [Gezocht voor centrum creatieve ondernemers (BN/deStem)  ](http://www.bndestem.nl/regio/roosendaal/4896672/Gezocht-voor-centrum-creatieve-ondernemers.ece?goback=.anh_1855809)  
  
 Foto's:  
<http://picasaweb.google.com/Crebucon/CreatieveWijk?feat=email#>
