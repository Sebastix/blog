---
title: "SIARR - flop"
date: "2007-10-14T22:53:52.000Z"
description: "Gaande weg in mijn onderzoeks-, conceptvormings, en visualisatiefase kom ik er steeds meer achter dat een robot zoals mi..."
categories: [internet]
comments: true
---

Gaande weg in mijn onderzoeks-, conceptvormings, en visualisatiefase kom ik er steeds meer achter dat een robot zoals mijn SIARR wel eens helemaal geen toekomst kan hebben. Het is zo dat we momenteel nog niet over de kunstmatige intelligentie beschikken, maar wie zegt dat we deze wel hebben in de toekomst om de SIARR helemaal te laten functioneren net als ons? De mens handelt met onze handen en anticiperen met onze hersenen. Hand-oog coordinatie, een coordinatie die onmogelijk te vervangen is met techniek denk ik.  
  
  
 Toch brengen we de SIARR op de markt, ontwerpen we een prachtige reclame marketing en pompen er bakken met geld in om de vraag te creeren in het huis naar een robot die werkelijk alles voor u kan doen! De SIARR...uw toekomstige hulp in het huis om zelf helemaal tot rust te kunnen komen. De SIARR verkoopt, verkoopt matig en al snel blijkt dat de SIARR helemaal niet zo handig is. Sterker nog, SIARR creert alleen maar chaos, net als de wereld buiten het huis. Opnieuw het oorzaak-gevolg dat de techniek die een grote chaotische puinhoop creert.  
  
 Ik wil dus twee tegengestelde werelden laten zien;  
 1) het positieve, agressieve verkoopplaatje  
 2) de praktijk die niet werkt  
  
 Net zoals met al die Tell-Sell producten, zo'n product zou de SIARR ook kunnen gaan worden. Over de vorm ben ik nog niet helemaal uit, maar dit zijn mijn eerste gedachtes.  
  
\- reclame (Tell-Sell stijl)  
\- nieuwsberichten over rampscenario's; schuld aan de SIARR  
\- problemen met de SIARR website; ervaringen van gebruikers  
\- reviews  
\- SIARR massaal te koop voor spotprijzen op Marktplaats  
  
[http://youtube.com/watch?v=pK58Aws1D\_Q](http://youtube.com/watch?v=pK58Aws1D_Q)  
<http://forum.trosradar.nl/viewforum.php?f=38>  
  
**De laatste fase...***?*  
 Mijn gehele concept wil ik in vorm gieten waarin ik al mijn visualisaties kan laten zien. Het meest voor de hand liggende vorm is hiervoor een website die de SIARR keurt. Een objectieve website-pagina (**Wiki**) waar alle media wordt gelinked zoals naar de reclame filmpjes, reviews, 2e handse SIARR kopen, gebruiksaanwijzing, officiele website SIARR etc.  
 Het is de bedoeling dat ik nu de zwarte kant ga visualiseren van de SIARR om de tegenstelling duidelijk te maken met het verkooppraatje. SIARR gaat alleen werken binnen een omgeving die statisch is en elke dag hetzelfde blijft. Het is de bedoeling dat de gebruiker elke dag zijn omgeving zo achterlaat, zoals de gebruiker de SIARR het heeft geleerd. Want wanneer deze omgeving niet overeenkomt met de geleerde handeling, gaat het fout. In de gebruiksaanwijzing ga ik dit nog nadrukkelijk zeggen; de eigenaar moet de omgeving altijd identiek achter laten anders werkt de SIARR niet naar behoren.  
 Ik ga beginnen met enkele nieuwsberichten te maken over de SIARR en enkele rampscenario's die er zich voor hebben gedaan (met beeld).  
[  
 http://nl.wikipedia.org/wiki/Huisrobot](http://nl.wikipedia.org/wiki/Huisrobot)
