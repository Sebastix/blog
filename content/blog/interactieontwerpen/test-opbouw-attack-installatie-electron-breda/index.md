---
title: "Test opbouw ATTACK installatie Electron Breda"
date: "2009-01-17T17:30:41.000Z"
description: "Van 23 januari t/m 29 maart 2009 is mijn interactieve installatie te bezoeken in het Electron in Breda.

Aankondiging:..."
categories: [internet]
comments: true
---

Van 23 januari t/m 29 maart 2009 is mijn interactieve installatie te bezoeken in het Electron in Breda.  
  
 Aankondiging: <http://www.idfx.nl/interference/blz/home.php?tl=0>

> <div class="kop">**Interference**</div>  
> <div class="tekst">Interference is een internationale tentoonstelling georganiseerd door Stichting Idee-fixe in samenwerking met het Spaanse festival In-Sonora dat afgelopen jaar te zien was in Madrid. Zowel geluid als licht zijn golven die met elkaar en alle voorwerpen op hun weg interfereren. Bij deze reactie op elkaar maken de golven mooie en vaak onvoorspelbare patronen. Die patronen vormen de basis voor de expositie Interference. Laat je verrassen door uiteenlopende installaties die gebruik maken van licht en geluid, high tech 'cosmos ray collectors' en interactieve computerinstallaties of sfeervolle, kaarsverlichte bewegende schilderijen. Een keur aan licht- en geluidskunstenaars uit heel Europa presenteert zich in 17 verschillende installaties.</div>

  
![](../../../images/IMG_0077.JPG "opbouw")  
![](../../../images/IMG_0078.JPG "opbouw")  
![](../../../images/IMG_0079.JPG "opbouw")  
  
![](../../../images/IMG_0080.JPG "opbouw")  
![](../../../images/IMG_0081.JPG "opbouw")  
![](../../../images/IMG_0082.JPG "opbouw")  
![](../../../images/IMG_0083.JPG "opbouw")  
![](../../../images/IMG_0085.JPG "opbouw")
