---
title: "MSN tagclouds - visualisaties"
date: "2008-01-07T23:37:35.000Z"
description: "
Vriendin van vriend van me, wekelijks contact, gesprekken over de weekendplannen enzo



Vriend van me met Honda C..."
categories: [internet]
comments: true
---

[![](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/1.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/1.jpg)  
 Vriendin van vriend van me, wekelijks contact, gesprekken over de weekendplannen enzo  
  
  
[![](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/2.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/2.jpg)  
 Vriend van me met Honda Civic, wekelijks contact, gesprekken over meetings en plannen over onze auto's  
  
[![](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/3.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/3.jpg)  
 Kennis met dezelfde motor in de Civic, soms contact, gesprekken over mogelijkheden over onze motor, plannen en problemen  
  
[![](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/4.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/4.jpg)  
 Een van m'n beste maten, dagelijks contact, gesprekken over 't weekend en plannen  
  
[![](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/5.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/5.jpg)  
 Merlijn studiegenoot, af en toe contact, gesprekken over school en weekenden  
  
[![](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/6.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/clouds/6.jpg)  
 Goede vriendin, dagelijks contact, gesprekken over alles
