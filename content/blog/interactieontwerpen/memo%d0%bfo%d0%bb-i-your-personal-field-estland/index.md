---
title: "Memoпoл-I - your personal field (Estland)"
date: "2010-08-26T12:17:20.000Z"
description: "In mijn Vimeo subscriptions lijst, die ik regelmatig even check om even te ontspannen en geïnspireerd te worden, kwam i..."
categories: [internet]
comments: true
---

In mijn [Vimeo subscriptions lijst](http://vimeo.com/sebastix/subscriptions/videos/rss), die ik regelmatig even check om even te ontspannen en geïnspireerd te worden, kwam ik de volgende video tegen:  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="352" width="625"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=14332678&server=vimeo.com&show_title=1&show_byline=1&show_portrait=1&color=&fullscreen=1&autoplay=0&loop=0"></param><embed allowfullscreen="true" allowscriptaccess="always" height="352" src="http://vimeo.com/moogaloop.swf?clip_id=14332678&server=vimeo.com&show_title=1&show_byline=1&show_portrait=1&color=&fullscreen=1&autoplay=0&loop=0" type="application/x-shockwave-flash" width="625"></embed></object>  
  
 Een installatie van kunstenaar Timo Toots uit Estland die aan de hand van jouw identiteitskaart allemaal persoonlijke informatie ophaalt uit verschillende databases (je vrienden, je rijbewijs, je wijsheid, je leeftijd etc).  
 Na [een bericht van mij op Twitter](http://twitter.com/Sebastix/status/22081757870) om dit te delen, blijft het mij fascineren dat in Estland de burgers de keuze hebben om hun identiteitskaart te voorzien van een chip die het mogelijk maakt om toegang te krijgen tot jouw persoonlijke informatie.

> Memoпoл-I is a machine that maps the visitors information field. Inserting a Estonian national ID-card and PIN code, the machine starts collecting every piece of information about the visitor from national databases and internet. The schematics of data are visualized on large-scale custom display.  
> <http://timo.dart.ee/works/memopol/>

  
 Zodra een burger de installatie toestemming geeft (door middel van een pincode) om toegang te krijgen tot zijn informatie op de chip, kun je met die basis aan informatie natuurlijk veel breder en dieper gaan zoeken op bijvoorbeeld het internet. Zoals je Facebook profiel, je emails in Gmail of Twitter berichten. Met [mijn eindexamenwerk Root](http://www.myroot.me) heb ik al laten zien tot welke verbindingen en nieuwe mogelijkheden dit kan leiden.  
  
 Wat ik me nu afvraag na het zien van deze installatie en technische werking ervan, waarom hebben wij in Nederland nog niet zo'n dergelijke koppeling met een chip? Je kunt er ons recht van maken dat wij (als individu) toegang moeten hebben tot onze eigen informatie die bijvoorbeeld de overheid heeft.  
 Als je in je portemonnee kijkt, zul je zien dat een aantal pasjes zijn uitgerust met een chip. Een tweede vraag die ik hierbij krijg, kunnen wij deze chip uitlezen? Dus; kan ik mijn Rabobank pasje uitlezen en persoonlijke informatie terugkrijgen (naam, leeftijd, adres etc) of heeft de Rabobank dit geblokkeerd?
