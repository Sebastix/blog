---
title: "Tentoonstelling beeldcultuur"
date: "2007-10-12T15:14:06.000Z"
description: "
3D model van onze tentoonstelling voor beeldcultuur.
Beelden zijn gemaakt met Google Sketchup.


Na de les van don..."
categories: [internet]
comments: true
---

![Beeldcultuur](/images/preview_beeldcultuur.jpg)  
 3D model van onze tentoonstelling voor beeldcultuur.  
 Beelden zijn gemaakt met Google Sketchup.  
  
  
 Na de les van donderdag werd er nog een belangrijk punt gemeld waar we over moesten nadenken. Omdat we in principe alle beelden laten zien die er bestaan, is het belangrijk na te gaan wat voor ons het allereerste beeld is en het allerlaatste beeld. Als we deze twee tegenstellingen hebben, is het duidelijk welke beelden daar allemaal tussen horen die in onze tentoonstelling passen.  
 Dus wat is ieder voor zich het eerste beeld en het laatste beeld voor hem persoonlijk?  
 Voor mij zijn dat de eerste grotschilderingen uit de oertijd;  
  
[![](http://images.encarta.msn.com/xrefmedia/sharemed/targets/images/pho/t010/T010201A.jpg)](http://images.encarta.msn.com/xrefmedia/sharemed/targets/images/pho/t010/T010201A.jpg)  
