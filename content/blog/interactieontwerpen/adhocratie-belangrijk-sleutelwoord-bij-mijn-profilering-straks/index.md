---
title: "Adhocratie, belangrijk sleutelwoord bij mijn profilering straks?"
date: "2009-08-25T17:13:29.000Z"
description: "Bron: http://www.12manage.com/methods_mintzberg_configurations_nl.html
Organisatiestructuur theorie van Henry Mintzberg..."
categories: [internet]
comments: true
---

Bron: [http://www.12manage.com/methods\_mintzberg\_configurations\_nl.html](http://www.12manage.com/methods_mintzberg_configurations_nl.html)  
 Organisatiestructuur theorie van Henry Mintzberg

### 5. De Adhocratie (de innovatieve organisatie)

  
 In adhocratie, hebben wij een zeer organische structuur, met weinig formalisering van gedrag. De werkspecialisatie is gebaseerd op formele opleiding. Er bestaat een tendens om de specialisten te groeperen in functionele eenheden voor huishoudelijke doeleinden, maar hen op te stellen in kleine, marktprojectteams om hun werk te doen. Er is een afhankelijkheid van coördinatie mechanismen om wederzijdse aanpassing aan te moedigen. Dit is het belangrijkste coördinerende mechanisme, binnen en tussen deze teams.  
 Om te vernieuwen, moeten wij met gevestigde patronen breken. Daarom kan de innovatieve organisatie zich niet baseren op welke vorm van standaardisatie dan ook voor coördinatie. Van alle configuraties, toont de adhocratie het minste respect voor de klassieke principes van management, vooral eenheid van bevel. De adhocratie moet deskundigen inhuren en hen macht geven - Professionals wiens kennis en vaardigheden in trainingsprogramma's zeer hoog ontwikkeld zijn.  
  
In tegenstelling tot de professionele bureaucratie, kan de adhocratie zich niet op de gestandaardiseerde vaardigheden van deze deskundigen baseren om coördinatie te bereiken, omdat dat standaardisatie in plaats van innovatie zou veroorzaken. Eerder, moet het bestaande kennis en vaardigheden slechts als basissen behandelen waarop nieuwe kunnen worden gebouwend. Voorts vereist het bouwen van nieuwe kennis en vaardigheden de combinatie van verschillende soorten bestaande kennis. Dus eerder dan het toestaan van de specialisatie van de deskundige of de differentiatie van de functionele eenheid om zijn gedrag te overheersen, moet de adhocratie in plaats daarvan door de grenzen van conventionele specialisatie en differentiatie breken. Terwijl elke professional in de professionele bureaucratie autonoom kan werken, moeten in de adhocratie de professionals hun inspanningen samensmelten. In adhocratieën moeten de verschillende specialisten hun krachten in multidisciplinaire teams samenvoegen, elk gevormd rond een specifiek innovatieproject.  
  
 De managers zijn er te over in adhocratie - functionele managers, integrerende managers, projectmanagers. De laatstgenoemde zijn bijzonder talrijk, aangezien de projectteams klein moeten zijn om wederzijdse aanpassing onder hun leden aan te moedigen, en elk team een toegewezen leider nodig heeft, een „manager.“ De managers worden functionerend lid van projectteams, met speciale verantwoordelijkheid om coördinatie tussen hen te bereiken. Zodanig dat de directe supervisie en formele authoriteit in belang verminderen. Het onderscheid tussen lijn en personeel is niet duidelijk.  
  
 De adhocratie kan twee basisvormen aannemen:  
### 5A. De operationele Adhocratie

  
 De operationele adhocratie vernieuwt en lost problemen direct namens zijn cliënten op. Zijn multidisciplinaire teams van deskundigen werken vaak onder contract, zoals in de denktank consultancy firma, creatief reclamebureau, of fabrikant van technische prototypen.  
 Een zeer belangrijke eigenschap van de operationele adhocratie is dat zijn administratief en operationele werk neigt te mengen tot één enkele inspanning. Het is namelijk in het ad hoc projectwerk moeilijk om de planning en het ontwerp van het werk te scheiden van zijn uitvoering. Allebei vereisen zij dezelfde gespecialiseerde vaardigheden, per project. Daarom kan het moeilijk zijn om de middenniveaus van de organisatie te onderscheiden van zijn werkende kern, aangezien de lijnmanagers en stafmedewerkers hun plaats innemen naast de operationele specialisten op de projectteams.  
### 5B. De administratieve Adhocratie

  
 Het tweede type van adhocratie functioneert ook met projectteams, maar voor een ander doel. Terwijl de operationele adhocratie projecten onderneemt om zijn cliënten te dienen, onderneemt de administratieve adhocratie projecten om nieuwe faciliteiten of activiteiten actief te maken, zoals in de administratieve structuur van een ver geautomatiseerd bedrijf. En in scherp contrast tot de operationele adhocratie, maakt de administratieve adhocratie een duidelijk onderscheid tussen zijn administratieve component en zijn werkende kern. De kern is beknot - afgesneden van de rest van de organisatie - zodat de administratieve component die overblijft als adhocratie kan worden gestructureerd.  
 Deze beknotting kan op een aantal manieren plaatsvinden:  
  
- Ten eerste, wanneer de operaties als een machine moeten zijn en zo innovatie in het beleid zouden kunnen belemmeren (wegens de bijbehorende behoefte aan controle), kan het als een onafhankelijke organisatie worden gevestigd.
  
- Ten tweede, kan de werkende kern totaal worden afgeschaft - in feite uitbesteed worden aan andere organisaties.
  
- Een derde vorm van beknotting doet zich voor wanneer de opererende kern geautomatiseerd wordt. Dit maakt het mogelijk om zichzelf te besturen, grotendeels onafhankelijke van de behoefte aan directe controles door de administratieve component. De laatste wordt toegestaan om zichzelf als adhocratie te structureren om nieuwe faciliteiten actief te maken of oude te wijzigen. Met deze verandering in de operationele werknemersgroep ontstaat er een dramatische verandering in de structuur: de werkende kern overtreft een staat van bureaucratie - op een bepaalde manier wordt het totaal bureaucratisch, totaal gestandaardiseerd,… en het beleid verplaatst volledig zijn oriëntatie. De regels, de verordeningen, en de normen worden nu in machines ingebouwd, niet in arbeiders. En de machines hebben nooit ergens geen zin in, hoe verlagend ook hun werk is. Daarom is er geen behoefte meer aan directe supervisie en technocratische standaardisatie. De obsessie met controle eindigt eveneens. Een korps van technische specialisten doet zijn intrede, om het technische systeem te ontwerpen en dan het te onderhouden.
  
