---
title: "Master Animation website"
date: "2010-03-26T11:46:28.000Z"
description: "Afgelopen weken heb ik gewerkt aan de technische realisatie van de website voor de nieuwe master opleiding animatie aan ..."
categories: [internet]
comments: true
---

Afgelopen weken heb ik gewerkt aan de technische realisatie van de website voor de nieuwe master opleiding animatie aan de kunstacademie AKV|St.Joost; Professional Master Animation. In samenwerking met [Staynice](http://www.staynice.nl "Staynice") (verantwoordelijk voor de gehele identiteit van de nieuwe opleiding) is dit resultaat ontstaan:  
  
[![](../../../images/Master_Animation_identity_staynice.jpg "Master Animation")](http://www.master-animation.com)  
  
 Website: [www.master-animation.com](http://www.master-animation.com)  
  
 De samenwerking met Rob en Barry van Dijck van [Staynice](http://www.staynice.nl) heb ik als zeer enthousiast ervaren. Het werk van ze kende ik al, maar de personen erachter nog niet dus vooral in het begin levert dat een boel positieve energie op om een mooi resultaat neer te zetten. Ook voor Staynice was het spannend, aangezien ze normaal gesproken zich enkel bezighouden met print en niet met een website welke een heleboel dynamisch inhoud met zich meebrengt waardoor een grafisch ontwerp er op elk scherm en systeem iets anders uit komt te zien. Hierbij wil ik ook op deze manier Rob en Barry bedanken voor de fijne samenwerking voor deze opdracht en kijk uit naar de volgende samenwerking.  
  
 Nu is het tijd voor mij om echt aan de slag te gaan met mijn afstudeerproject waar ik momenteel ook de nodige -vooral grafische- hulp bij zoek, binnenkort meer hierover!
