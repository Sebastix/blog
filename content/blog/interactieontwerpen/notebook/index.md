---
title: "Notebook"
date: "2009-04-28T15:48:31.000Z"
description: "Eind vorig jaar heb ik meegeholpen aan de Notebook installatie van Evelien Lohbeck. De installatie is een flash applicat..."
categories: [internet]
comments: true
---

Eind vorig jaar heb ik meegeholpen aan de Notebook installatie van Evelien Lohbeck. De installatie is een flash applicatie die ik technisch heb gerealiseerd op een vrij eenvoudige manier. Deze installatie is een praktische vertaling van een filmpje waarin de installatie als demo wordt weergegeven. Evelien heeft met dit filmpje hiermee al een flink aantal prijzen gewonnen waaronder de juryprijs voor beste online film op het Nederlands Film Festival en de installatie reist heel het land door naar verschillende tentoonstellingen en festivals. Ook op internet is het een veelbekenen (honderduizenden views!) filmpje. Hieronder staat het welbekende filmpje:  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="375" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=4116727&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="375" src="http://vimeo.com/moogaloop.swf?clip_id=4116727&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[Noteboek](http://vimeo.com/4116727) from [Evelien Lohbeck](http://vimeo.com/evelienlohbeck) on [Vimeo](http://vimeo.com).  
  
 Dit is een filmpje van de installatie:  
<object height="375" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=4376575&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="375" src="http://vimeo.com/moogaloop.swf?clip_id=4376575&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[Interactive "Notebook" installation](http://vimeo.com/4376575) from [Evelien Lohbeck](http://vimeo.com/evelienlohbeck) on [Vimeo](http://vimeo.com).
