---
title: "Visualisaties"
date: "2007-09-25T11:47:09.000Z"
description: "Virtuality Continuum
Augmented Reality
Virtuel Reality
Mixed Reality
2 schermen; de realiteit en de virtuele wereld ..."
categories: [internet]
comments: true
---

Virtuality Continuum  
 Augmented Reality  
 Virtuel Reality  
 Mixed Reality  
 2 schermen; de realiteit en de virtuele wereld die erop aansluit  
 Gebruiksaanwijzing  
 ***Hoe reageert de robot op zijn omgeving?  
 Hoe reageert de robot op mensen?  
 Hoe leert de robot omgaan met zijn omgeving en mensen?***  
  
  
 Augmented Reality combineert de echte wereld met een digitale wereld. De echte waarneembare wereld nemen we waar met onze zintuigen (horen, zien, voelen, ruiken, proeven). De techniek creert een zesde zintuig die ons meer informatie geeft dan er werkelijk waarneembaar is.  
  
 De robot in het huis moet worden behandeld als een kind. Het zal eerst moeten leren hoe hij zal moeten interacteren met zijn omgeving. Zijn eigenaren zullen eerst voor moeten doen wat de robot moet doen in het huis. Dit proces begint bij 0 en is in principe een eindeloos proces. Alleen op deze manier kan de robot zo zelfstandig mogelijk worden.  
  
*De volgende stappen:*  
\- ontwerp gebruiksaanwijzing  
\- welke presentatievorm?  
\- de stappen visualiseren van de fases hoe een robot zich ontwikkelt  
  
*Meer onderzoeksmateriaal aansluitend op visualisaties en concept*  
[http://studierstube.icg.tu-graz.ac.at/handheld\_ar/](http://studierstube.icg.tu-graz.ac.at/handheld_ar/)  
[http://studierstube.icg.tu-graz.ac.at/handheld\_ar/signpost.php](http://studierstube.icg.tu-graz.ac.at/handheld_ar/signpost.php)  
[http://en.wikipedia.org/wiki/Augmented\_reality](http://en.wikipedia.org/wiki/Augmented_reality)  
<http://www.torpus.com/lifeclipper/>  
 http://www.torpus.com/lifeclipper/image/viewpoint.mov  
 http://www.torpus.com/lifeclipper/image/papermill.mov  
 http://marisil.org/mov/demo2001.mpeg  
<http://marisil.org/>  
<http://www.zookadz.com/zookpics.html>  
<http://www.se.rit.edu/~jrv/research/ar/video.html>  
  
 Eerste twee eigen visualisaties  
![Impression](http://www.interactieontwerpen.nl/_sebastian/imgHuisvandetoekomst/own_impr1.jpg)  
  
![Impression](http://www.interactieontwerpen.nl/_sebastian/imgHuisvandetoekomst/own_impr2.jpg)
