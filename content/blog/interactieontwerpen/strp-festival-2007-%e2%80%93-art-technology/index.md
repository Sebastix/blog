---
title: "STRP FESTIVAL 2007 – ART &#38; TECHNOLOGY"
date: "2007-11-30T14:06:43.000Z"
description: "
Twee jaar geleden was STRP een nieuw festival op het gebied van technologische kunst, muziek en robotica. Het was voor ..."
categories: [internet]
comments: true
---

![](http://interactieontwerpen.nl/_sebastian/mapping/temp/STRP_LOGO.jpg)  
Twee jaar geleden was STRP een nieuw festival op het gebied van technologische kunst, muziek en robotica. Het was voor mij ook het eerste bezoek aan een dergelijk festival op dat gebied en mijn kennis op dat gebied was toen nog lang niet zo ver als op de dag van vandaag (en dat is nog steeds te weinig). Het was mooi om te zien hoeveel er is veranderd in twee jaar tijd. De expo had een groter aanbod dan twee jaar geleden. Ook stak het geheel allemaal een stuk logischer in elkaar en was het meer gevuld met details.  
  
Voordat je naar STRP gaat, is het altijd goed om voor jezelf af te vragen wat je nu eigenlijk onder ‘interactive art’ verstaat. Zeker ook als je de tekst “Het virtuele object van interactieve kunst” hebt gelezen. Vanuit onze studie leren wij als interactie ontwerpers te werken vanuit een concept. Vanuit dat concept volgt een mediumvorm waarin dat concept wordt vertelt. Wat je op STRP aantreft, zijn vaak installaties die een technisch trucje laten zien. Er zal heus wel een idee achterzitten, maar deze staat vaak ondergeschikt aan de vorm. In een spelletjesvorm werkt een technisch trucje meestal wel goed; juist door deze handeling kun je alleen verder komen naar een hoger doel. Zoals het racespelletje waar je een helm om moet zetten. In deze helm moet je schreeuwen; hoe harder je schreeuwt, hoe harder je je verplaatst op het virtuele circuit.  
  
Er stond ook een prachtige installatie waar je jezelf in een pak moest verplaatsen en je middels lichamelijke bewegingen een “transformer” boven je kon verplaatsen. Die transformer bestond uit een paar laserarmen en benen. Heel interessant om te zien hoe deze reageerde op jouw bewegingen, maar wat was het doel? Dat miste ik nu weer net bij zo’n installatie. Het wordt nog vele malen leuker als je ook nog iets moet bereiken met die handelingen is mijn mening.  
  
Een andere installatie die me ook aansprak (net als vele andere) was een opstelling van ballonen die elk apart van hoogte konden worden veranderd. Het zag er gewoon indrukwekkend uit plus dat de muziek de lampen aanstuurde die in de ballonen zaten. Een mooi schouwspel waar je alleen maar onderuit hoeft te gaan zitten en alles tot je laat komen.  
  
\[flash http://www.youtube.com/watch?v=ipNNM1-ExXA\]  
  
Even verderop kwam ik ook de virtual reality bril tegen. Deze bril had ik vorige periode in mijn concept binnen het thema mens&amp;machine gebruikt. Indrukwekkend was om te zien hoe met een vrij lowbudget techniek jouw handeling kijken werd overgenomen door een kleine robot in een soort van schoenendoos. Het werkte uitstekend! Echter ontbrak hier ook weer een soort van doel die je moest vervullen met je handeling.  
  
Misschien ben ik te doelgericht en te kritisch bij elke installatie die ik tegenkom, maar als ik een dergelijk concept uitwerk moet het voor mij altijd wel duidelijk zijn waarom ik het doe en welk uitgangspunt ik wil innemen. Dat is wel wat ik vaak mis in nieuwe media installaties. Misschien moet ik het ook wel allemaal aannemen als experimenten die er worden tentoongesteld, in plaats van allerlei installaties die je uitdagen in je gedrag.  
  
In de tekst “Het virtuele object van interactieve kunst” wordt gezegd dat interactieve kunst doelt op reflectie middels je zintuiglijke ervaringen. Interactieve kunst probeert een ervaring op te wekken of een emotie op te roepen. Ik deel die mening, echter ben ik nog maar weinig interactieve kunst tegengekomen die mij echt hebben weten te verbazen en verwonderen. Het zou je moeten dwingen tot een bepaald gedrag waar je jezelf dan op dat moment bewust van wordt en iets heel anders laat ervaren door die bewustwording. Op STRP deed de ShameStation dit misschien nog wel het beste. De schaamte van de persoon die de ander iets aan wilt doen wordt verplaatst naar de bestuurder van die persoon. Echter is die bestuurder anoniem achter een scherm en heeft de dader het excuses dat het niet zijn keuze is om die handeling te verrichten.  
  
\[flash http://www.youtube.com/watch?v=qjREtUCcM3o\]
