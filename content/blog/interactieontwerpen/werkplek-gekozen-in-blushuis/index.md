---
title: "Werkplek gekozen in Blushuis"
date: "2009-10-26T18:11:00.000Z"
description: "16 werkplekken waar je uit kunt kiezen in het Blushuis dankzij Starterslift. Dat valt toch best mee zou je zeggen, maar ..."
categories: [internet]
comments: true
---

16 werkplekken waar je uit kunt kiezen in het Blushuis dankzij Starterslift. Dat valt toch best mee zou je zeggen, maar ik moest toch wel even goed nadenken voordat ik een keuze had gemaakt. Keuze genoeg, want inclusief mij zijn er nu nog 12 plekken over.  
 Ik ga het hoekje rechtsachter eigen maken.  
  
[![workplace Blushuis](http://farm3.static.flickr.com/2752/4047065382_647df6b1cc_m.jpg)](http://www.flickr.com/photos/sebastix/4047065382/ "workplace Blushuis")[![workplace Blushuis](http://farm3.static.flickr.com/2697/4046319919_01bb13a67c_m.jpg)](http://www.flickr.com/photos/sebastix/4046319919/ "workplace Blushuis")  
  
 Maak ook even kennis met mijn collega buren!  
  
 STRIPE | leert professionals hun (bedrijfs) doelen te bereiken via Social Networking  
<http://www.stripe.fm>  
  
 Amanda Butterworth | graphic designer  
<http://www.amandabutterworth.com/>
