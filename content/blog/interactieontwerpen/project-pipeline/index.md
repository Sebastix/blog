---
title: "Project Pipeline"
date: "2009-02-28T14:45:55.000Z"
description: "



Project Pipeline application demo from Sebastian Hagens on Vimeo.

Bekijk hier de Engelse Wiki handleiding pag..."
categories: [internet]
comments: true
---

<a name="top"></a>  
![](/images/projectPipeline.jpg "Project Pipeline")  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="314" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=4130226&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="314" src="http://vimeo.com/moogaloop.swf?clip_id=4130226&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[Project Pipeline application demo](http://vimeo.com/4130226) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
[Bekijk hier de Engelse Wiki handleiding pagina](http://wiki.animationlab.info/index.php?title=Main_Page)  
  
 In het tiende kwartaal van de opleiding hebben we voor het eerst een groepsproject gekregen waarin we onze krachten moesten bundelen. De opdracht was behoorlijk toegepast en productgericht binnen de context die we gewend waren. De periode na deze periode zou er een samenwerkingsproject komen tussen Engelse en Nederlandse animatie-studenten. Vorig jaar is er ook zo'n pilot geweest en daaruit kwam naar voren dat de studenten de technische middelen ontbreken om op een efficiënte manier te communiceren met de nodige digitale bestanden waarmee ze samen een animatie maakten. Voor ons was de opdracht om een applicatie te ontwikkelen om dit probleem op te lossen / vereenvoudigen. Kortom; er werd van ons verwacht een hulpmiddel te bedenken en ontwikkelen welke het mogelijk maakt tussen 2 animatoren samen te werken in de breedste zin van het woord.  
  
 Joost de Wert en Nick Koning waren onze twee belangrijkste begeleiders waar Joost het op zich nam om de boel organisatorisch aan te sturen en Nick ons te helpen met de techniek. Snel hadden we al een rolverdeling gemaakt in projectvorm. De eerste taak was iemand z'n hoofdtaak en dus ook eindverantwoordelijk op dat gebied. De taken erachter tussen haakjes, waren de subtaken.  
  
**Taakverdeling Project Pipeline**  
 Marjon Pennings: accountmanager (onderzoek)  
 Rob Hebing: projectleider (onderzoek)  
 Hans van Arken: interactie ontwerper (techniek grafisch ontwerp)  
 Merlijn van Eijk: grafisch ontwerper (onderzoek)  
 Sebastian Hagens: hoofd techniek (interactie ontwerp)  
 Victor Freriks: onderzoek (techniek)  
  
**Techniek**  
 Mijn rol in dit project was dus hoofdzakelijk de techniek. Er is gekozen om een applicatie te ontwikkelen in Adobe AIR. Waarom AIR? De kennis die hiervoor nodig was, was al grotendeels beschikbaar vanuit onze knowhow en ervaring: HTML, CSS, Flash AS3, Flex, Javascript, AJAX, PHP, MySQL. Al deze technieken zijn gebruikt in de Pipeline applicatie. Een stuk client-side geschreven in HTML, Flash en Javascript en een stuk server-side in PHP die praat met een MySQL database. Het communiceren tussen de client- en server-side gebeurt grotendeels met AJAX-requests met behulp van jQuery en Mootools.  
 Hieronder vind je een overzicht van hoe de applicatie technisch eigenlijk werkt tussen de verschillende eilandjes:  
![](../../../images/technische-werking.jpg "Pipeline technische werking")  
  
**Modules**  
 Er is gekozen om de applicatie modulair op te bouwen. Dat wil zeggen dat in de applicatie steeds modules worden toegevoegd die in principe elk een eigen functie hebben binnen de applictie. De modules die in onze ogen het belangrijkste waren voor een beta-versie:  
 - chat  
 - downloaden  
 - uploaden  
 - planning  
  
 Het meest fantastische aan een Adobe AIR applicatie is dat je ook kunt praten met de lokale computer waarop de applicatie draait. In een bestaande webtaal is het ineens mogelijk om bijvoorbeeld vanaf je bureaublad een bestand te slepen naar de applicatie, los te laten en het bestand wordt direct naar het internet verzonden. Deze drag-and-drop handeling is zo eenvoudig dat iedereen deze gelijk snapt als je het 1 keer voor doet.  
 Naast simpel chatten met elkaar en bestanden kunnen down- en uploaden wilden we meer doen. Een welbekend probleem is dat als je met meerdere bestanden gaat werken, al heel snel het overzicht kwijtraakt en je dus soms langer bezig bent om bepaalde bestanden te zoeken dan er aan te werken. Hiervoor hebben we de planning dus bedacht waarin je verschillende taken kunt uitzetten over een bepaalde tijd. Deze taken brengen vaak gespecificeerde werkbestanden met zich mee. Als je in de applicatie op een taak klikt op de planning, dan zie je ook welke bestanden er gekoppeld zijn aan deze taak. Als je een bestand wilt koppelen aan deze taak, hoef je nu ook alleen maar een bestand te uploaden en deze zal automatisch aan die taak worden gekoppeld. De applicatie weet namelijk dat jij die taak hebt geopend. Op deze manier willen we dus heel veel bestanden categoriseren aan de hand van een taakverdeling op een tijdas. Animatoren werken ook vaak een bepaalde 'pipeline' af (storyboarding, script, character design, background design etc) en de bedoeling vanuit ons was om deze pipeline zelf door de animatoren te laten plannen middels de applicatie.  
 Als er bijvoorbeeld op 'storyboard - scene 2' in de planning wordt geklikt, opent zich de lijst bestanden die alleen maar horen bij deze taak.  
  
 Helaas zijn we tijdens de technische realisatie niet ver genoeg gekomen om een echte BETA-versie te lanceren welke ook gebruikt zou worden tijdens het samenwerkingsproject. De applicatie bevindt zich in de alpha fase met wel een heleboel functionaliteiten die werken, maar nog altijd een boel kleine foutjes bevatten. Het project zal, als het goed is, door de volgende lichting Interactie Ontwerpers worden voortgezet.  
  
[Bekijk hier de Engelse Wiki handleiding pagina](http://wiki.animationlab.info/index.php?title=Main_Page)  
  
[Klik hier om terug naar boven te gaan](#top)
