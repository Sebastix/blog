---
title: "YouTube filmpje"
date: "2007-09-30T20:28:16.000Z"
description: "Dit filmpje is gemaakt van bestaande YouTube filmpjes. Mijn onderwerp was snelheid met een Japans tintje. De snelheidsbe..."
categories: [internet]
comments: true
---

Dit filmpje is gemaakt van bestaande YouTube filmpjes. Mijn onderwerp was **snelheid** met een Japans tintje. De snelheidsbeleving ontwikkelt door Japanners met hun autotechniek.  
  
<object height="344" width="425"><param name="movie" value="http://www.youtube.com/v/pcLaB0_eWmM&hl=nl&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed allowfullscreen="true" allowscriptaccess="always" height="344" src="http://www.youtube.com/v/pcLaB0_eWmM&hl=nl&fs=1" type="application/x-shockwave-flash" width="425"></embed></object>
