---
title: "1vs1 &#8216;control&#8217; concept - afgeschreven"
date: "2008-02-11T12:19:13.000Z"
description: "Dit concept gaat het niet worden; te weinig tijd en dus onhaalbaar.


- Spel 1vs1 ‘control’
In een spel is het de be..."
categories: [internet]
comments: true
---

Dit concept gaat het niet worden; te weinig tijd en dus onhaalbaar.  
  
  
**- Spel 1vs1 ‘control’**  
In een spel is het de bedoeling om de andere oncontroleerbaar te maken en de controle bij jou zelf te houden. De ander doet precies hetzelfde en gaat de strijd aan met jou acties. Je bestrijdt andermans acties met je eigen acties en daarom worden jouw acties reacties op acties en reacties van de ander.  
  
Het doel van de speler is om je eigen control in de spelwereld (virtueel) te behouden. Dat doel wordt verstoord door input en output van de andere speler. Dit kan natuurlijke op verschillende manieren en heb je in de techniek verschillende sensoren, in- en output mogelijkheden voor (concept):  
  
\- video (voor tracking)  
\- geluid  
\- licht  
\- drukgevoelige sensoren  
\- temperatuur  
\- afstand meten mbv een echo sensor (meet afstand tussen object en sensor mbv een zeer hoog signaal)  
\- hellingmeter  
\- infrarood (wii-remote)  
  
**Kritische vragen**  
\- Heb ik wel een scherm voor een virtuele wereld nodig?  
\- Is een extra (virtuele) wereld nodig?  
\- Hoe is het duidelijk te maken welke handeling er verricht moet worden?  
  
Mogelijke in- en output voorbeelden in de installatie:  
  
**- Beweegbare ondergrond**  
Op één vierkante meter kan je naar achter, voren, links en rechts kantelen om het zichtbeeld van de ander te veranderen en die ander te ‘laten vallen’. Je kunt tegen reageren om niet te vallen (meten met hellingmeter).  
  
**- Verblinden**  
Mogelijkheid om de tegenspeler te verblinden met een fel licht. De tegenspeler kan hierop weer reageren door het licht te dimmen. Dit werkt allebei de kanten op met dezelfde handeling. Welke handeling hoort hier bij?  
  
**- Lawaai**  
Je kunt een handeling verrichten om lawaai te creëren bij de ander. De ander kan daarop ook dezelfde handeling verrichten om dit lawaai te dempen.
