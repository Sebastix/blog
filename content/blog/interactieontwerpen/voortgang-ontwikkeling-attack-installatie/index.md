---
title: "Voortgang ontwikkeling ATTACK installatie"
date: "2008-12-09T23:10:18.000Z"
description: "Deze periode krijgen we het vak Sound Design waarin we worden uitgedaagd om een interactieve geluidsinstallatie te bouwe..."
categories: [internet]
comments: true
---

Deze periode krijgen we het vak Sound Design waarin we worden uitgedaagd om een interactieve geluidsinstallatie te bouwen die uiteindelijk minimaal een maand moet draaien in het Electron in Breda.  
  
 Ik kies er bewust voor om mijn interactieve installatie ATTACK verder door te ontwikkelen in deze lessen, omdat in mijn ogen de installatie nog lang niet af is en nog een heleboel potentie biedt om zich te ontwikkelen tot echt een stand-alone installatie. Vooral het geluid welke ik heb gebruikt in de installatie en op welke wijze deze wordt gegenereerd is op een eenvoudige manier bijgevoegd in een laat stadium van het ontwikkelen de installatie.  
  
  
  
 Daarom lijkt het mij heel zinvol om me vooral te concenteren wat het geluid nu echt precies doet in mijn installatie en op welke wijze deze wellicht beter kan worden gekoppeld aan de input van de gebruiker en de virtuele levensvorm.  
 Momenteel is de situatie zo dat het geluid alleen in volume toeneemt aan de hand van de variabele 'hoeveelheid kleur: niet wit'. Dit gebeurt dus vrij statisch met een simpel mp3 bestandje welke wordt ingeladen. Ik weet dat het mogelijk is om in Processing ook zelf geluid te generen en ook veel meer parameters hiervan te kunnen veranderen. Zelf zou ik het logischer vinden dat het geluid aan de hand van hoeveelheid agressiviteit van de virtuele levensvorm die wordt uitgedrukt in versnelling, vertraging en versnelling. Ik wil een breder onderzoek hiervoor opzetten en ook onderzoeken welke geluiden er mogelijk nog meer aansluiten hieraan.  
  
 Naast deze stappen is het ook noodzakelijk de installatie van nog meer body te gaan voorzien, zodat deze ook werkelijk minimaal 1 maand lang zonder onderhoud en toezicht van mijzelf kan functioneren. Hieronder vind je een aantal schetsjes van hoe mogelijk de installatie er dan uit komt te zien.  
  
![](../../../images/IMG_0064.JPG "schets attack")  
![](../../../images/IMG_0066.JPG "schets attack")
