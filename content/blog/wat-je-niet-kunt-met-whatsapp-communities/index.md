---
layout: blog
title: Wat je niet kunt met WhatsApp communities
date: 2023-03-07T12:00:00.266Z
description: Door de toename van het aantal chatgroepen in WhatsApp, probeert de app nu deze groepen samen te brengen in een nieuwe functie genaamd communities. Echter mis ik nog een boel dingen om er een waardevolle community mee op te bouwen.
categories: social media, whatsapp
comments: "true"
---

Vorig jaar april 2022 kondigde WhatsApp een nieuwe functie aan: communities. Zie [dit bericht](https://blog.whatsapp.com/sharing-our-vision-for-communities-on-whatsapp?lang=nl). 

> Als we kijken naar alle feedback die we hebben ontvangen, denken we dat we meer kunnen doen om het mensen gemakkelijker te maken deze drukke gesprekken in dit soort groepen te beheren.
> En hier komen Community's om de hoek kijken. Met Community's op WhatsApp kunnen mensen afzonderlijke groepen samenbrengen onder één overkoepelende structuur die bij hun behoeften aansluit.

Op diverse sites staat uitgelegd WhatsApp communities werken:

- https://www.iculture.nl/tips/whatsapp-community/ 
- https://www.guidingtech.com/whatsapp-groups-vs-communities-the-differences/
- https://respond.io/blog/whatsapp-communities 

Waar ik het in deze blog over wil hebben, zijn de dingen die je (nog) niet kunt met WhatsApp communities. Dit zijn functies die je nodig hebt als je een waardevolle online community wilt opbouwen. 

### Aanmaken van besloten / geheime / privé groepen

Ondanks dat WhatsApp zegt dat communities en groepen intrinsiek privé zijn, kan iedereen vrij eenvoudig via een uitnodigingslink lid worden. Wat ik mis is dat je een besloten, geheime of privé groep kunt aanmaken. Om nieuwe leden toe te laten, zou je als beheerder een lidmaatschapsverzoek moeten kunnen afkeuren of goedkeuren. Zo'n lidmaatschapsverzoek is bijvoorbeeld aan een unieke, één keer te gebruiken link of algemene link gekoppeld. 

### Een topic / onderwerp starten

Deze functie bestaat niet in WhatsApp, maar je vindt deze wel terug in andere messenger apps zoals Slack of Discord. Als lid zou je zelf een onderwerp kunnen starten binnen een groep. Anderen kunnen dan reageren met berichten in dit onderwerp. Deze berichten zijn niet zichtbaar in de groep. 

### Als lid een groep aanmaken

Als lid van een community zou ik zelf graag een groep voor leden willen aanmaken. Optioneel zouden beheerders zo'n aangemaakte groep kunnen goedkeuren voordat deze wordt gepubliceerd. 

### Eerdere berichten teruglezen in een groep

Als je lid wordt van een groep, dan kun je niet de chatgeschiedenis ervan teruglezen. Dit komt waarschijnlijk omdat deze geschiedenis niet op de servers van WhatsApp wordt opgeslagen. Als beheerder zou ik over de optie willen beschikken om de volledige chatgeschiedenis leesbaar te maken voor nieuwe groepsleden. Je kunt nieuwe leden dan ook verwijzen naar bestaande berichten. 

### Bestaande communities zoeken

Communities en groepen zijn intrinsiek privé volgens WhatsApp. In de praktijk kun je vrij eenvoudig lid worden van een groep of community via een uitnodigingslink. Zo'n link kan in de meeste gevallen door elk community-lid worden gedeeld. 

Waarom zou je een community niet openbaar kunnen maken? Deze kan dan doorzoekbaar worden gemaakt voor anderen die op zoek zijn naar specifieke communities of onderwerpen. Dat kan bijvoorbeeld wel op Telegram, zie mijn blog [Communities op Telegram](/communities-op-telegram/).

### Regels per groep

Als beheerder zou ik willen instellen:

* dat een nieuw lid zich eerst moet voorstellen of een bericht moet plaatsen in groep A voordat hij mag deelnemen of posten.

* dat een nieuw lid eerst een X aantal keer een bericht moet plaatsen in een groep voordat hij  mag deelnemen.

* dat een nieuw lid eerst een X aantal dagen lid moet zijn voordat hij mag deelnemen.

### Moderators

Er bestaan nu maar twee rollen in een community: een beheerder of een lid. Ik zou graag zou dat je iemand de rol moderator kunt geven. Een moderator heeft minder rechten dan een beheerder, maar kan wel berichten van leden modereren. Zo zou hij een bericht kunnen verbergen voor leden in afwachting op moderatie door andere moderators of beheerders. Idealiter zou je ook per groep aparte moderators willen aanstellen. 