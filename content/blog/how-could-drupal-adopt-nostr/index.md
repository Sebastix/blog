---
layout: blog
title: How could Drupal adopt Nostr?
date: 2023-12-30T13:13:32.76Z
description: For me this is quite a challenging topic to point out in this blog. In this blog I'm writing some stuff out loud.
categories: drupal, nostr, posse
comments: "true"
---

💜 - __In case you missed it, when the founder of Drupal Dries Buytaert discovered Nostr, it was love on first sight ([blog](https://dri.es/nostr-love-at-first-sight)).__ 

## Nostr & Drupal

Nostr is a protocol used for sending notes and other stuff to relays. All events containing those data are signed on behalf of an entity with public-key cryptography.  
Drupal is a state-of-the-art, free and open source content management system for building websites and applications.

These following questions I'm trying to answer without going deep into each topic.

* What type of data and information would we like to syndicate with Drupal?
* What Nostr features can we implement in Drupal?

## Open web manifesto of Drupal

Let's have a look at the recent published [open web manifesto of Drupal](https://www.drupal.org/association/open-web-manifesto) where I would like to point out the following points:

> **It’s built on freedom: You don’t need permission to learn, build, or advance open web technology. Anyone, anywhere can contribute to making it better.**

> **It’s defined by decentralization: No single person or entity controls the open web.**

> **To live up to that definition, an open web must not be built on proprietary technology.**  
> ..It must be designed to protect (not exploit) personal data and public discourse.  
> ..It must enable the next generation of innovators and entrepreneurs to compete effectively.  
> ..It must be resilient to a changing world and not controlled by a select few.

Nostr embodies this all! Just like Nostr, Drupal is freedom technology.

## Authentication (NIP-05 + NIP-07 + NIP-46)
Register / login / authenticate on a Drupal powered website with your Nostr identity keys.
Use Drupal as a custodial solution platform (key bunker) for managing Nostr keys. When connected, Drupal can be used an app for signing your events.
Additional, also an internet identifier can be set for the user ([see this module](https://www.drupal.org/project/nostr_id_nip05)). 

This authentication layer is needed to fully enable the Nostr experience with Drupal.

## Long-form content (NIP-23)
Publish blog content to your Nostr network by integrating NIP-23 in Drupal. In this way your (Markdown formatted) content can also be read on clients which support NIP-23 like Yakihonne, blogstack.io and habla.news.
[See this module](https://www.drupal.org/project/nostr_content_nip23) which I'm working on how this could work with Drupal.

## Drupal as relay management system (RMS) 

With Drupal you're able to connect to multiple and different types of databases for retrieving data. Most relay implementations are using a database which can be connected to Drupal.
When connected, you can use Drupal as a UI for managing the content stored on the relay.

## Run a relay with Drupal

Drupal is written in PHP, so technically it could act as a relay. As far as I know, there is only one (very basic) relay implementation written in PHP yet: [yar](https://github.com/1ma/yar).

## Chat / messaging (NIP-44)

For Drupal there are many contrib chat modules:
* https://www.drupal.org/project/drupalchat (Drupal 7 only / not actively maintained)
* https://www.drupal.org/project/rocket_chat
* https://www.drupal.org/project/dru_chat

I'm aware there is a lot of buzz around doing chat and messaging on Nostr because it's not fully private. With NIP-44 recently merged, I expect a growth of clients broadcasting more private data. 

## File and media storage (NIP-94 + NIP-98)

Use Drupal as a file and media storage backend where you can upload your files to. Many Nostr clients have the option to use your favorite media upload service for sharing media files among your notes and events.

## Groups / communities (NIP-72)

The Drupal module groups is a widely used module for creating groups with users ([12.000+ sites using it](https://www.drupal.org/project/group)) to let them access specific content.
With Drupal Groups + Nostr you have a very robust base for starting an online community platform around content. 

## Tagging content (NIP-32)

Within Drupal you can use taxonomies to classify content with tags. Those tags can be mapped with Nostr labels. This integration could be used with other modules (for short text notes, long-form content etc) when you publish to Nostr from Drupal. 

## Commerce (NIP-15)

When you create an e-commerce website with Drupal, NIP-15 could be integrated to syndicate your commerce products content to the Nostr network. In this way, your products will also show up in Nostr marketplace clients like Nostr Market ([LNbits extension](https://github.com/lnbits/nostrmarket)) and Plebeian Market. 

## Other stuff

In 2022 I was inspired by Stackers.news, Lobste.rs and HackerNews building [cchs.social](https://cchs.social) with Drupal. It's an online community for Honda car enthusiasts and an Instagram profile link aggregator.    
Soon after I published the first version, I've learned that Nostr could be the social layer on this platform and replace the site-only account registrations and interactions (rating and commenting).

With Drupal you could get a fullstack platform for building a Nostr client. These are some other ideas for clients copy-catting other existing platforms:
* A Calendly alternative for appointment scheduling 
* A UserVoice alternative for collecting user feedback on your product
* Todoist alternative for your to-do list
* A Strava alternative where you share your sport activities
* Drupal module for unlocking content with a zap
* Drupal module for liking content with zaps
* Drupal module providing a block with a zap me button

For bridging the gap between Drupal and Nostr, you need to write custom middleware solutions in the backend (PHP) and frontend (JavaScript / Twig). That's why I'm building and maintaining the [nostr-php helper library](https://github.com/swentel/nostr-php) and the Drupal modules [Nostr Simple Publish](https://www.drupal.org/project/nostr_simple_publish), [Nostr internet identifier NIP-05](https://www.drupal.org/project/nostr_id_nip05) and [Nostr long-form content NIP-23](https://www.drupal.org/project/nostr_content_nip23). 

### Resources

* [freeing-creators-with-nostr-and-bitcoin](https://yakihonne.com/article/naddr1qqwyummnw3ez64r9vd5z64m9v44kc7fdxgcryvedxycz6v3eqgszak7w562dzerznp222fvrgk8adkt9k9s783yt2lf6luqegp2c3pqrqsqqqa2898c92f#freeing-creators-with-nostr-and-bitcoin)
* [Long-form Content: nostr (Building a Blog using SvelteKit and Nostr as a CMS (Part 1)) at 2023-07-31 12:44](https://nostr.com/naddr1qqxnzd3exqurqdenxvmn2d3nqgs8a8yjftjj2hy4532d9zhgj4f2rcyplq526nwq8q2s3dgfrzxgvhsrqsqqqa28kvezwy)
* https://github.com/nostr-protocol/nips
* https://github.com/jowo-io/next-auth-lightning-provider
* https://github.com/fabianfabian/nostr-media