---
layout: blog
title: Privacy is binair
date: 2024-05-27T13:13:32.76Z
description: In de Satoshi Radio podcast aflevering 311 legt Bert Slagter met een paar woorden uit waarom privacy binair is. Hoe denk ik daar over?
categories: privacy
comments: "true"
---
Tot mijn verbazing heeft een Nederlandse rechtbank een vonnis uitgesproken dat een ontwikkelaar van een privacy-tool (Tornado cash) schuldig is bevonden aan witwassen. De uitspraak kun je [hier teruglezen](https://uitspraken.rechtspraak.nl/details?id=ECLI:NL:RBOBR:2024:2069). 
Dit is in mijn ogen een van de meest opmerkelijke conclusies in de uitspraak:

> De rechtbank stelt vast dat het team steeds heeft benadrukt dat het met het ontwikkelen van Tornado Cash , het behoud van privacy bij het doen van transacties op de Ethereum blockchain voor ogen had. Het team heeft bij het nastreven van dat doel steeds bewust de ideologie van maximale privacy laten prevaleren boven andere belangen, zoals de integriteit van het financiële verkeer, het recht op eigendom en het belang van opsporing van strafbare feiten. **Handelen vanuit een ideologie maakt je echter niet onaantastbaar voor wet- en regelgeving die voor iedereen geldt, ook niet wanneer daarbij in volledige openheid wordt gehandeld.**

Ergens verbaast me deze uitspraak ook weer niet, want in mijn beleving wordt privacy steeds meer gecriminaliseerd door overheden, diverse publieke instanties en de mainstream media. 
In de [podcast aflevering #311](https://overcast.fm/+oQ8VEx1tU/0:53:10) van Satoshi Radio wordt de uitspraak uitvoerig geanalyseerd en besproken door Bart Mol, Bert en Peter Slagter. Uit deze aflevering heb ik de volgende stelling geknipt van Bert Slagter waar ik me volledig in kan vinden.

<video src="https://sebastix.nl/blog/video/privacy-is-binair.mp4" controls="controls" style="max-width: 400px;">
</video>

Privacy is binair, zoals de heren ook [na dit stuk uitleggen](https://overcast.fm/+oQ8VEx1tU/1:34:00). Het is of één of het is nul. Als je ergens technisch (wiskundig) een concessie doet in een systeem of applicatie waar iemand niet kan kiezen wat hij/zij deelt én met wie, dan is er simpelweg geen sprake meer van privacy. Daarbij maakt het niet uit om wat voor data het gaat. Als je geen invloed hebt op dat er data wordt gedeeld en met wie, dan is de privacy nul. 
Over de rechterlijke uitspraak maak ik me deels zorgen. Deze schept namelijk een precedent waar developers die privacy-by-default en privacy-first systemen / tools ontwikkelen zich in een grijs gebied gaan bevinden.  Een gebied waar vanuit een top-down systeem willekeur toegepast kan worden. Indien partijen in zo'n systeem zich niet kunnen vinden in het handelen van zo'n developer, dan kunnen zij daar op acteren met boetes, sancties, arrestaties of andere middelen die alleen zij tot hun beschikking hebben. Een deel van dit precedent is dat een developer mogelijk aansprakelijk kan worden gehouden voor het aanhangen van een bepaalde ideologie (oa [A Cypherpunk's Manifesto](https://www.activism.net/cypherpunk/manifesto.html) of [FOSS](https://nieuwwestbrabant.nl/digitaal-en-verbinding/geschiedenis-open-source-en-free-software)). In hoeverre conflicteert dit met onze grondrechten ([artikel 10](https://www.denederlandsegrondwet.nl/id/vlxups19tky0/artikel_10_privacy)) en andere Europese verdragen ([artikel 7 + 8](https://www.europarl.europa.eu/factsheets/nl/sheet/157/bescherming-van-persoonsgegevens))?

Ik sluit deze blog af met een gedachtengang waarbij ik 'hardop nadenk'.

Stel, ik vind privacy binair. Maakt me dit dan een privacy maximalist? Als je over maximale privacy beschikt, ben je zelf in staat om te bepalen wat je deelt en met wie. Hier ben ik altijd groot voorstander van. Zodra ik besef dat ik zelf niet kan bepalen *wat* ik deel en met *wie*, dan overschrijdt je een grens bij mij. Er is dan sprake van nul privacy. Dit gezegd hebbende, kan ik stellen dat ik me aardig maximalistisch opstel betreft de hoeveelheid privacy die je kunt hebben. 

**Resources** 
- Privacy, human rights, and Tornado Cash - https://www.citationneeded.news/tornado-cash/
- https://www.patrick-breyer.de/en/conviction-of-tornado-cash-programmer-privacy-is-not-a-crime/
- https://freedom.tech/tornado-cash-developer-convicted/ 