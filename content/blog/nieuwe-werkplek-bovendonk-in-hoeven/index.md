---
layout: blog
title: Nieuwe werkplek Bovendonk in Hoeven
date: 2021-10-06T07:53:38.427Z
description: Vanaf 1 oktober heb ik mijn werkplek verhuisd naar Bovendonk in
  Hoeven. Sinds ik vanaf mei weer fulltime voor mezelf bezig ben, was altijd al
  het idee om binnen een paar maanden op een externe locatie te gaan werken.
  Werk en prive hou ik graag zoveel mogelijk gescheiden.
categories: kantoor, werkplek, bovendonk
comments: "true"
---
In 2017 ben ik verhuisd naar Sprundel maar ik ben geboren en getogen in Hoeven. De locatie Bovendonk is mij niet vreemd, maar toch is Bovendonk altijd in staat om te verrassen. Zo'n verrassing kreeg ik toen ik las dat je een eigen 'hub' kunt huren. Dit concept is ontstaan tijdens de covid 'lockdown' periode. Bovendonk biedt werkplekken aan voor mensen die niet thuis kunnen werken en niet op locatie bij hun werkgever terecht kunnen. Het is echter ook interessant voor eenmansbedrijven zoals mijzelf.

Meer informatie over het kantoorkamerconcept lees je hier: <https://www.bovendonk.nl/nl/kantoorruimte-west-brabant/bedrijfsruimte-breda/>

![](img_4306.jpg "Binnentuin")

![](img_4305.jpg "Binnentuin")

## Sebastix' hub

This is where the magic happens. Mijn werkplek is 12m2 groot, net groot genoeg voor twee bureau's en enkele kasten. Een wand is voor een groot gedeelte mooi oranje geverfd om de ruimte een frisse warme gloed te geven. Via een groot, hoog raam heb ik uitzicht op de gehele binnentuin en valt er veel daglicht naar binnen. 

![](img_4287.jpg)

![](img_4288.jpg)

![](db051e32-651f-42dd-becf-606ec6221ad2.jpg)

## Unieke locatie met veel historie

Bovendonk is misschien wel een van Nederlands mooiste monumentale panden. De historie van deze plek gaat helemaal terug naar de 13e eeuw. In die eeuw werd hier een uithof gesticht door de monniken van de Sint-Bernardusabdij te Hemiksem (Antwerpen). 

https://www.youtube.com/watch?v=OpaJ0ApZGtE

> De rijkdom van het klooster nam toe tot ver in de zestiende eeuw, toen het uitbreken van de [Tachtigjarige Oorlog](https://www.brabantserfgoed.nl/page/4672/tachtigjarige-oorlog "Tachtigjarige Oorlog") zorgde voor politieke onrust in de regio. Het klooster en de bijbehorende gronden brandden gedurende het conflict af en werden vervolgens in 1596 herbouwd, met een verdere renovatie in de jaren '40 van de zeventiende eeuw. Na juridisch getouwtrek aan het einde van de Tachtigjarige Oorlog kwam het kloostercomplex uiteindelijk in handen van het bisdom Antwerpen, waardoor de religieuze invulling van het complex kon worden hervat. De hogere rechtsmacht was echter in handen van het markiezaat van Bergen op Zoom, dat sinds 1533 het eerdere privilege uit 1458 met betrekking tot rechtmacht van de heren van Bergen op Zoom uitoefende.

Bron: <https://www.brabantserfgoed.nl/page/6319/bovendonk-te-hoeven>

![](65239047_493676828036537_1240074827145093113_n.jpg)

![](36161423_294296581141302_3441951688246689792_n.jpg)

Bron: <https://www.instagram.com/bovendonk_hoeven/>

Eind 19e eeuw werd er een nieuw klooster ontworpen door [Pierre Cuypers](https://nl.wikipedia.org/wiki/Pierre_Cuypers). In 1908 werd dit gebouw opgeleverd en nu nog steeds het huidige Bovendonk. Vanaf 1996 functioneert Bovendonk als multifunctionele locatie met een conferentiecentrum, hotelkamers, een priester- en diakenopleiding, een brasserie/grand cafe, kantoorruimtes, een trouwlocatie, spreek- en vergaderlocaties etc. 

Bovendonk heeft een eigen Wikipedia pagina: <https://nl.wikipedia.org/wiki/Bovendonk> en meer over de geschiedenis van Bovendonk en Hoeven lees je [hier](https://www.bovendonk.nl/nl/geschiedenis/).

![](img_4277.jpg)

![](img_4297.jpg "De hal met verschillende kantoorruimtes.")

## Kopje koffie? Of kom je een dag hier werken?

Je bent van harte welkom om langs te komen om de energie hier te proeven! Je bent zelfs welkom om hier een dag te komen werken aangezien ik hier een extra bureau heb staan. Het enige wat je zelf moet regelen, is een lunch. Ik ben vier dagen in de week aanwezig op dinsdag tot en met vrijdag. Stuur me gerust een berichtje of geef me een belletje!