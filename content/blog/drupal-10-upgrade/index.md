---

layout: blog
title: Drupal 10 upgrade
date: 2022-12-19T14:08:31.328Z
description: Version 10 of Drupal has been released on December 15. With this major release Drupal sets a big future-proof step.
categories: drupal 10 upgrade
comments: "true"

---

Drupal version 7 and 9 will become obsolute as of November 2023. It's important to upgrade your Drupal website to get the latest bug fixes and security updates. 
Drupal 10 comes with new exciting features which are already highlighted by others:

* [Drupal 10 is worth a fresh look | Opensource.com](https://opensource.com/article/22/12/drupal-10-fresh-look)

* [Drupal 10 has Arrived. Here’s What to Expect.](https://evolvingweb.com/blog/drupal-10-has-arrived-heres-what-expect) 

* [Drupal 10: This is what we’re so excited about | Annertech Digital Agency](https://www.annertech.com/blog/drupal-10-what-were-so-excited-about) 

* [The Next Frontier: Top 10 New Features of Drupal 10](https://www.prometsource.com/blog/Drupal-10-Whats-New)

### Get your Drupal website ready for version 10

Last couple of weeks I prepared all my Drupal websites for Drupal 10, as far it was possible. 

1. Install [upgrade status](https://www.drupal.org/project/upgrade_status) module
   After enabling this module you will get a nice overview of items you need to manage before your site is Drupal 10 ready.

These are the steps I've taken:

- Update PHP to version 8.1

- Upgraded all contrib modules to the latest major (table) release
  Check outdated module with `composer outdated drupal/*`
  *Please check the documentation of the module if there is handwork involved when upgrading to a new major release.*

- Uninstall deprecated core modules
  Some core module in Drupal 9 are deprecated. See https://www.drupal.org/docs/core-modules-and-themes/deprecated-and-obsolete

- Update code in custom modules and themes
  You can check on deprecated code and in most cases automatically fix it with [rector](https://www.drupal.org/project/rector).

When you done all the above, you probably still have some contrib modules left which don’t have a Drupal 10 compatible release yet. This is the case for most of the projects I’m maintaining. You can look up the Drupal 10 issue in the issue tracker list of the module when you can expect a release which is compatible. Hint: subscribe / follow the issue to receive the latest changes.

### My kickstart project is on Drupal 10

My template / kickstart Drupal project doesn't have a lot of dependencies of contrib modules, so it was quite easy to upgrade this project to 10.

1. If enabled, uninstall and remove upgrade status module `drush pmu upgrade_status && composer remove drupal/upgrade_status`

2. Export your config `drush cex`

3. Change version number in your composer.json to 10.0.0 of the Drupal core-* packages

4. Run `composer update drupal/* -W`

5. Update your database `drush update-db`

For the next coming months I hope I can upgrade every Drupal website to version 10 when every contrib module has a Drupal 10 compatible release. 
