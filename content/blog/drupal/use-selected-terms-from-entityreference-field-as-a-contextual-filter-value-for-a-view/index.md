---
title: "[Drupal] - Use selected terms from entityreference field as a contextual filter value for a view"
date: "2020-12-10T21:00:13.000Z"
description: "Use selected terms from entityreference field as a contextual filter value for a view snippet"
categories: [drupal, twig]
comments: true
---

Twig snippet. The `drupal_view` function comes from the [Twig Tweak](https://www.drupal.org/project/twig_tweak) module. Used in a Drupal 8.9.x project.  
The `contextual_filter_value` gives a string output for example 5,6,7,8. These numbers are the taxonomy term ID's.

```javascript
{% set contextual_filter_value = '' %}
    {% for item in content.field_view_filtered_solutions['#items'] %}
        {% set contextual_filter_value = contextual_filter_value ~ (loop.first ? '' : ',') ~ item.target_id %}
    {% endfor %}
{{ drupal_view('view_name', 'display_name', contextual_filter_value) }}
```

[https://gitlab.com/-/snippets/2049119](https://gitlab.com/-/snippets/2049119)