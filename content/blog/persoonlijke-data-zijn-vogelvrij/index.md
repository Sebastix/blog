---
layout: blog
title: Persoonlijke data zijn vogelvrij
date: "2011-10-02T08:40:26.000Z"
description: 
categories: social media, personal graph, social graph
comments: "true"
---


In een vorig artikel ‘[We geven alles weg](../we-geven-alles-weg)’ staat beschreven dat ieder van ons een product is binnen diverse social media services. We ruilen onze digitale identiteit in voor het gratis gebruik van deze diensten. We betalen als het ware met een stukje privacy. Met dit principe hoeft op zich niets mis te zijn. Maar het blijft wel vaak onduidelijk wat er precies gebeurt met die data van ons. Algemeen bekend is dat veel organisaties hun best doen om zoveel mogelijk gegevens van alles en iedereen te verzamelen. Met name grote marketingbureaus slagen hier bijzonder goed in en weten met de verkoop van data substantiële omzetten te realiseren. De gedachte erachter is: veel en goede informatie leiden tot de beste match van vraag en aanbod. Het eindresultaat is dan een interessante aanbieding voor de klant. Maar zeg eens eerlijk: hoe vaak krijgen we nu echt een interessante aanbieding die voorziet in een actieve behoefte? In het onderstaande artikel zoomt Sebastian Hagens in op verzamelingen van persoonlijke gegevens op het internet. Hij houdt een pleidooi voor transparantie en beheersbaarheid van persoonlijke gegevens en ziet uiteindelijk zakelijke voordelen voor zowel burger, klant en internetbezoeker èn adverteerders en aanbieders!

## Geld verdienen door te matchen

Google verdient al jaren miljarden met Adwords advertenties die zich op websites bevinden. Google is heer en meester in het indexeren van websites en pagina’s op het internet. Dankzij hun link graph (de index van al die pagina’s en onderlinge relaties) vinden we tegenwoordig vrijwel alle informatie die we nodig hebben. Google is de eigenaar van de relaties in deze
graph en dit vormt de basis van hun verdienmodel: matchen van
vraag en aanbod. De relaties classificeren de onderwerpen van
webpagina’s, hoeveel bezoekers ze krijgen en welke websites naar
elkaar verwijzen. De Adwords advertenties sluiten dus in principe
altijd aan bij het onderwerp van de webpagina. Dit betekent
overigens niet vanzelf dat een advertentie ook goed aansluit bij de
behoeftes van de bezoeker van die pagina. Dat gaat niet altijd goed.
Denk bijvoorbeeld aan advertenties over vakanties in Frankrijk,
terwijl je net een vakantie hebt geboekt. Het is dus voor Google van
belang om meer te weten te komen over de bezoeker als aanvulling op website informatie alleen.
Juist op dit sociale vlak heeft Facebook een voorsprong op Google
met ongeveer 800 miljoen gebruikers. Het bestaat uit een
gigantisch netwerk van personen die zowel passief als actief met
elkaar in contact zijn. Facebook is dus op zijn beurt heer en
meester in het indexeren van bepaald gedrag van personen.
Dankzij de bekende ‘vind ik leuk’ knop die op veel websites staat,
traceert Facebook ook daar persoonlijke, digitale sporen (ook als
je geen Facebook profiel hebt!). Facebook beheert dus een enorm
rijke social graph die een schat aan informatie ontsluit over
persoonlijk gedrag en voorkeuren. En adverteerders weten wel
hoe ze deze informatie te gelde kunnen maken. Toch lijken de
winsten van Facebook relatief klein vergeleken met die van Google, zoals blijkt uit onderstaande tabel.

Naar de echte waarde van Facebook kun je alleen nog gissen, hoewel sommigen die taxeren op 80 miljard dollar. We zullen pas echt een beeld krijgen van de waarde van Facebook wanneer het bedrijf naar de beurs gaat.
Ook Twitter maakt winst, al zijn daar de meningen nog over verdeeld. Twitter vormt een derde voorbeeld hoe dankzij onze digitale sporen, een interest graph kan worden samengesteld, die de basis vormt voor een (toekomstig) krachtig verdienmodel.

## Big (data) brother is watching you
De link graph van Google, de social graph van Facebook en de interest graph van Twitter hebben een ding met elkaar gemeen: onze persoonlijke data zitten erin. Dat zijn dus gegevens die we hebben geruild voor gratis diensten. Andere internetbedrijven zien hun kans en raken geïnspireerd door de verzamelwoede van de drie grote namen. Dus wordt ons gedrag massaal vastgelegd met behulp van (super)cookies, beacons, Facebook Connect, remarketing, spiders, bots en flash cookies. Des te meer data van een persoon zijn vastgelegd, des te beter kunnen adverteerders die persoon segmenteren. Een gevolg daarvan is dat de internet bezoeker steeds vaker dezelfde advertenties tegenkomt, gepersonaliseerde zoekresultaten ontvangt en steeds minder toevallige informatie aantreft. Voorbeelden van de achterliggende techniek zijn IQNOMY en PersuasionAPI. De Filter Bubble is al enige tijd werkelijkheid en hierdoor lijkt het internet ook steeds saaier te worden. De bezoeker ziet simpelweg steeds vaker dezelfde content langskomen.
Voorlopig lijkt aan deze ontwikkeling nog geen einde aan te komen. Voorspeld wordt dat er steeds meer filters gaan komen die gepersonaliseerde informatie en diensten zullen leveren. Het artikel ‘Big data is watching you’ uit Management Team, februari 2011 schetst hierover een duidelijk beeld. Steeds vaker kom je content tegen, geselecteerd door een systeem. In het zelfde artikel wordt ook gesteld dat het bedrijf Experian voor de grote uitdaging staat alle Nederlandse huishoudens te koppelen aan miljoenen online profielen van personen die zijn verzameld via het internet (ze kochten in juni nog de startup SafetyWeb die het online gedrag van kinderen analyseert). Waarom dit wordt gedaan door deze bedrijven is duidelijk: ze zien ‘serious money’ voor zich. Maar al die ambities van bedrijven roepen wel de volgende vragen op: waarom ben je zelf eigenlijk niet in staat om de informatie naar je toe te halen waar je werkelijk behoefte aan hebt? Zijn al deze koppelingen die voor mij gemaakt zijn wel in mijn belang of in het belang van consumenten? Waarom zou ik mijn data beschikbaar stellen zonder te weten wat er in welke context mee gebeurt? En hoeveel bedrijven beschikken al over zo’n database waarin verschillende profielen aan elkaar zijn gekoppeld? Het angstige gevoel komt op dat zij al veel meer denken te weten over mij dan ikzelf. Help! Big (data) brother is watching me!

## Vogelvrij
Onze persoonlijke data zijn dus vogelvrij. Zodra je deze op het internet weggeeft, verlies je de controle erover. Data gaan van hand tot hand en belanden in databases van organisaties waar je nooit contact mee hebt gehad en ook niet zult hebben. Het zogenaamd anonimiseren van al deze informatie biedt ook al geen garanties. Zodra deze data worden gekoppeld aan andere databases groeit namelijk weer het risico dat een profiel zó rijk gevuld wordt, dat deze relatief eenvoudig te herleiden is tot een persoon. Toen bijvoorbeeld zoekmachine AOL in 2006 per ongeluk 20 miljoen zoekwoorden van 650.000 mensen op straat verloor, gingen verschillende media aan de slag om met die data personen te identificeren. En met succes, er werden verschillende gebruikers geïdentificeerd aan de hand van door hen gebruikte zoekwoorden. Dat is meer dan vijf jaar geleden, toen de techniek nog niets was in vergelijking met de techniek van nu. Hoeveel eenvoudiger zou het vandaag de dag zijn om personen te identificeren met soortgelijke data?

## Data honger
Gemiddeld bevindt elke Nederlander zich in 250 tot 500 databases. De Nederlandse overheid is koploper in de wereld als het gaat om verzamelen van zoveel mogelijk persoonlijke gegevens. Nederland vormt dus ook de meest uitdagende omgeving om deze situatie ter discussie te stellen. Burgers, gebruikers van internet, zouden het recht moeten hebben om persoonlijke gegevens te kunnen inzien, terug te halen en/of te kunnen wijzigen. Een sterk begin kan zijn dat elke organisatie een API zou inrichten, net zoals populaire social media services dat hebben gedaan. Via een API is het eenvoudiger om persoonlijke gegevens op te halen die bekend zijn bij een bepaalde organisatie. Het wordt tijd dat er aandacht komt voor de belangen van burgers en dat kwesties van ‘privacy’ serieus
op de agenda worden geplaatst. Af en toe is een herbezinning op dit vlak in alle stormachtige technische ontwikkelingen wel noodzakelijk. Voordat het te laat is.
Voor bovengenoemde organisaties zal dit al een bijzonder grote stap zijn en aanvankelijk zal men vinden dat dit niet past binnen de filosofie en verdienmodellen. Er zal dus veel weerstand tegen zijn. Maar je kunt ook veel voordelen vaststellen voor zowel aanbieder als consumenten wanneer matching van vraag en aanbod en inzicht van persoonlijke gegevens op internet transparant en beheersbaar zijn. Een grotere inbreng van de consument/internetgebruiker zelf zal veel waarde kunnen toevoegen aan de frequentie en volume van transacties via internet.

## Personal graph
Vanuit de link graph, social graph en interest graph kan iedere persoon een personal graph opbouwen. In principe heeft iedereen al zo’n personal graph, maar in de werkelijkheid is die nog behoorlijk chaotisch gestructureerd. Het is een groot splinterweb: al je digitale sporen staan op verschillende locaties op zowel het web als op de apparaten waarmee je surft. Jouw personal graph is de verzameling van je digitale sporen.
In een volgend artikel zal Sebastian Hagens dieper ingaan op deze personal graph; vanuit welke belangen je die kunt gebruiken, welke gegevens daar bij horen, hoe je aan de slag kunt om jouw digitale sporen op te halen en hoe het landschap eruit ziet als jouw personal graph informatie kan uitwisselen met anderen.