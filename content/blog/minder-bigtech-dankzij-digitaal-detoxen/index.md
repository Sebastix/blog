---
layout: blog
title: Minder BigTech dankzij digitaal detoxen
date: 2022-02-02T15:46:40.341Z
description: Ik heb de ambitie om minder (en het liefst geen) BigTech te
  gebruiken. De laatste weken kom ik erachter dat digitaal detoxen hiervoor een
  prima middel is.
categories: digitale detox, bigtech
comments: "true"
---
Ik geloof dat minder BigTech mijn (digitale) welzijn verbetert. BigTech is namelijk niet ginteresseerd in mijn welzijn en daarom is het mijn eigen verantwoordelijkheid om hiervoor te zorgen. De grote techreuzen willen namelijk zoveel mogelijk aandacht van jou ontvangen. Ik ben me ervan bewust dat tijd schaars is en je maar aan 1 ding tegelijk aandacht kunt geven. Dat wat je aandacht geeft, zal uiteindelijk groeien. Wat mij betreft groeit BigTech niet nog meer en daarom verdienen ze mijn aandacht niet (meer).

Wil jij bewuster omgaan met alle BigTech, social media en andere digitale platformen? Wil jij jouw aandacht ook beter verdelen? Bekijk dan onderstaande bronnen eens.

**[Tegenlicht aflevering 'Digitale detox' met Hans Shnitzler](https://www.vpro.nl/programmas/tegenlicht/kijk/afleveringen/2022-2023/digitale-detox.html)**

![Aflevering Tegenlicht](schermafbeelding-2022-02-03-om-11.30.04.png "Aflevering Tegenlicht")

**[Artikel over hoe we langzaam veranderen in nerds](https://www.vpro.nl/programmas/tegenlicht/lees/artikelen/2022/langzaam-veranderen-we-allemaal-in-mark-zuckerberg.html)**

> Het onvermogen van de nerd om de absurditeit en veranderlijkheid van het bestaan te accepteren – inclusief het irrationale en onlogische karakter van menselijke interacties –, voedt zijn onvrede over het functioneren van de mens en zijn leefwereld. Uit die kloof tussen wil en werkelijkheid, tussen wat de nerd als wenselijk en onwenselijk percipieert, ontstaat een ‘mindset’ die erop gericht is om die afstand ongedaan te maken. Hoe? Door alles wat het bestaan ongemakkelijk en weerbarstig maakt met technologische snufjes weg te poetsen.
>
> Daarmee zegt men in feite ‘nee’ tegen de pijn en het ongemak die meekomen met wat het betekent om ‘in leven te zijn’. In plaats van het leven te omarmen, wordt het verworpen. Dit nee-zeggen is een vorm van levensontkenning; men beziet dat wat de mens tot mens maakt in een negatief licht en wijst het af. Dat maakt een pessimistische grondhouding, een droefgeestig soort misantropie die Nietzsche eigen achtte aan wat hij het ‘passieve nihilisme’ van ‘laatste mensen’ noemde. Mensen die zichzelf en de ander moe zijn geworden.

**[Artikel over humane technologie, digitaal welzijn en onze kernwaarden](https://www.vpro.nl/programmas/tegenlicht/lees/artikelen/2021/hoe-vinden-we-onze-levenslust-terug-in-een-digitale-wereld.html)**

> Hoe bestrijden we de hegemonie van het digitale denken dat ons leven reduceert tot een getal gebaseerd op je browsegeschiedenis, je hoeveel likes en de bestedingruimte van je creditcard? Met wederom een knipoog naar Nietzsche pleit Schnitzer voor de herwaardering van mens als vitaal wezen. Zo schrijft hij: 'De kille doelrationaliteit van het dataïsme bevragen en uitdagen vraagt om een gepaste dosis levenslustige gretigheid.'

[](https://todoist.com/nl/templates/apps/digital-detox-self-care)Dit is een [handige lijst in Todoist](https://todoist.com/nl/templates/apps/digital-detox-self-care) met tips om je (digitale) welzijn te verbeteren.

### Maak je smartphone dom

Onze smartphones (en andere apparaten) zijn tegenwoordig zo slim, dat ze ons dom maken en houden. We hoeven immers steeds minder zelf na te denken en te doen. Door je telefoon dommer te maken, maak je jezelf slimmer. Het zal meer moeite kosten om dingen te doen. We kennen allemaal de uitdrukking 'de moeite waard'. Ergens moeite voor doen, is wat ons menselijk maakt. De volgende dingen kunnen je hier misschien bij helpen:

* Zet de kleuren van je beeldscherm op zwart-wit
* Zet alle pushnotificaties uit
* Zet gezichtsherkenning uit
* Stel tijdslimieten in op apps die je vaak gebruikt
* Leg je telefoon 's nachts niet op je nachtkastje
* Gebruik social media in je browser
* Lees het nieuws in je browser
* Heel radicaal en eenvoudig: [gebruik een dumbphone](https://duckduckgo.com/?t=ffab&q=dumbphone&atb=v231-1&ia=web)

Het boek ['wij nihilisten'](https://www.debezigebij.nl/boek/wij-nihilisten/) heb ik deze week aangeschaft en toegevoegd aan mijn readlist[](https://www.debezigebij.nl/boek/wij-nihilisten/).

Ik ben benieuwd wat jouw ervaring is met digitaal detoxen. Wat zijn jouw tips voor een beter digitaal welzijn?