---
layout: blog
title: Communities op Telegram
date: 2021-01-11T15:00:53.919Z
description: Steeds meer communities verplaatsen zich naar Telegram.
categories: Telegram, communities
comments: false
---
Communities lijken zich steeds meer te verplaatsen naar de chat applicatie Telegram. In vergelijking met Whatsapp biedt Telegram veel slimmere functies aan om een community te onderhouden. Je kunt bijvoorbeeld je eigen bots realiseren die mensen kunnen gebruiken. 

Veel nieuwe digitale diensten of producten hebben vaak een een link naar een Telegram groep. Telegram groepen worden bijvoorbeeld veel gebruikt om support te verlenen. Niet alleen door medewerkers die in de groep zitten, maar ook andere leden in de groep bieden vaak een helpende hand. Telegram heeft een openbare lijst met groepen die je kunt doorzoeken. Je kunt dus zoeken naar je favoriete hobby en mogelijk vind je een groep mensen met diezelfde interesse. Mijn zoekopdracht naar 'Honda Civic':

![](schermafbeelding-2021-01-11-om-14.22.27.png)

Je kunt bijvoorbeeld ook zoeken op plaatsnaam. Per plaatsnaam zie je in Telegram vaak groepen wat gaat over handel (een virtuele marktplaats groep). Dan verwacht je dat daar allerlei handige spullen te koop worden gezet, maar in de praktijk zie ik nu vooral minder legale spullen worden aangeboden... Dit gebeurt ongetwijfeld ook op Whatsapp of Facebook, maar het is bijzonder dat dit openbaar vindbaar is (handig voor justitie). 

In de groepen waar ik zelf actief ben doet veel me denken aan de tijd waarin ik zelf actief was op diverse forums. Op deze fora bracht ik avonden door om berichten te lezen, onderwerpen te starten, reacties te plaatsen, modereren van content en ook het ontwikkelen van features in forums (phpBB). Het meest actief was ik op het Civic Club Holland forum ([staat nog altijd hier online](http://www.hondacommunity.nl/forum/)).

Het grote verschil tussen Telegram en forums is hoe berichten zich verplaatsen in de tijd. In een actieve Telegram groep, is de kans groot dat er meerdere onderwerpen tegelijk worden besproken. Aangezien berichten chronologisch onder elkaar worden gezet -het is immers gewoon een chat- raak je snel het overzicht kwijt. Dat is jammer. Ik zie in veel groepen interessante berichten, linkjes, afbeeldingen en video's voorbij komen. In mijn ogen verdient zo'n discussie een betere plek dan alleen in een chat. Al die berichten in een chat verdwijnen heel snel en zijn bovendien lastig terug te vinden. 

In deze post wil ik niet zeggen dat forums beter zijn dan Telegram, maar Telegram lijkt op dit gebied nog veel te kunnen verbeteren. Ze zouden bijvoorbeeld eens moeten kijken naar Slack hoe je daar een discussie voert na aanleiding van één chatbericht. Deze discussie is dan eigenlijk een subchat waar één specifiek onderwerp besproken kan worden. Als je op deze manier verschillende subchats zou kunnen verzamelen, kunnen deze vervolgens worden gecategoriseerd. Net zoals er op een forum ook verschillende categoriën staan met daarin de onderwerpen.