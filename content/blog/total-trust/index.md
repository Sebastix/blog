---
layout: blog
title: Total Trust
date: 2024-05-06T10:13:32.76Z
description: What if the government is watching you 24/7? For China citizens fighting for human freedom rights this is the reality. This documentary shows how mass surveillance could look like anywhere in any future for discerning citizens. In my opinion this this already exists in our Western democracy in the form of BigTech co-working with many governments.
categories: privacy, surveillance, bigtech, china
comments: "true"
---
![Total Trust](Screen-Shot-2024-05-06-13-15-46.59.png)
[2Doc: Total Trust](https://www.2doc.nl/documentaires/2024/04/total-trust.html), this documentary only viewable from The Netherlands with Dutch subtitles.  
Official website: https://total-trust.org/

Some recent examples how BigTech is surveilling us in cooperation with governments:
* [HOW BIG TECH AND SILICON VALLEY ARE TRANSFORMING THE MILITARY-INDUSTRIAL COMPLEX](https://watson.brown.edu/costsofwar/papers/2024/SiliconValley)
* [Feds Ordered Google To Unmask Certain YouTube Users. Critics Say It’s ‘Terrifying.’](https://www.forbes.com/sites/thomasbrewster/2024/03/22/feds-ordered-google-to-unmask-certain-youtube-users-critics-say-its-terrifying/) 