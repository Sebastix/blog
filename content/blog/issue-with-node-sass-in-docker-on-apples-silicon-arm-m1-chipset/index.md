---
layout: blog
title: Issue with node-sass in Docker on Apple's Silicon ARM M1 chipset
date: 2021-05-26T08:14:31.679Z
description: "My local development setup for compiling SCSS to CSS was broken on
  my new Macbook Pro M1. It took me some time before I found a solution. In this
  post I will share my insights and the solution for this issue. "
categories: Apple, ARM, M1, Macbook Pro, Node, SASS
comments: "true"
---
I recently upgraded to a 13" Macbook Pro with the new M1 processor of Apple. For many projects I'm using Docker for having a local development environment. Such as a Drupal 9 project where Node.js and Gulp is used for compiling SCSS files to CSS. For these kind of projects I'm using an self build Docker image. 

In an other project with [NodeBB](https://nodebb.org/) I discovered the LTS version (v14) of Node.js is not compatible with the arm64 based M1 chip of Apple. You have to use version 15 or 16 as mentioned in [this issue](https://github.com/nodejs/TSC/issues/886).

![](schermafbeelding-2021-05-26-om-10.57.18.png)

While Node.js was upgraded to v15 or v16, things were still stuck when executing a Gulp task: `Error: Node Sass does not yet support your current environment: OS X Unsupported architecture (arm64) with Unsupported runtime (88)`.

For compiling SCSS to CSS files, [node-sass](https://www.npmjs.com/package/node-sass) is used as a dependeny on [gulp-sass](https://www.npmjs.com/package/gulp-sass). 

Now things are getting a bit quirky. The solution that worked for me:

* Install Node.js v15 instead of v14
* Upgrade the npm dependency node-sass package to `^6.0.0`

In the running Docker container:

* Run `npm install`
* Run `npm rebuild node-sass`

This last step was crucial! Don't ask me why, but after this rebuild process the Gulp task are being executes without errors. 

With Node.js v16 installed, it's broken. The error `Error: Node Sass does not yet support your current environment: OS X Unsupported architecture (arm64) with Unsupported runtime (93)` shows up when I execute a Gulp task. With v15 installed, the solution above works.\
As you can read in the documentation of node-sass, version 6.0.0 works with v16 of Node.js. In the solution I've found version 15 is used. I would expect that version 6.0.0 is not backwards compatible. Well...it works and it took me some hours to fix it. For now I'm happy, I can focus again on writing SASS for the project. 

If you have any questions or other information regarding this issue, please drop me a message!