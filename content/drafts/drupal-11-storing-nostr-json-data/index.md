---
layout: blog
title: 
date: 2021-01-02T13:13:32.76Z 2024-05-16T13:05:18
description: 
categories: 
comments: "true"
---

https://www.drupal.org/project/drupal/issues/3215207

Drupal 11 will require MySQL 8.0 and MariaDB 10.6 as minimum versions. This means we can store JSON objects in the database tables. 

https://mariadb.com/resources/blog/using-json-in-mariadb/

As for Nostr, almost all the data is transmitted in the JSON format. 