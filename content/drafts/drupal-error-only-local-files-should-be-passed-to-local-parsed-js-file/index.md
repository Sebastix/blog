# Drupal error: only local files should be passed to _locale_parse_js_file()

In my Drupal sandbox I got this error occasionally. Quite annoying and for now I ignored this error by just clearing the Drupal cache (`drush cr`).

```php
Uncaught PHP Exception Exception: "Only local files should be passed to _locale_parse_js_file()."
```


