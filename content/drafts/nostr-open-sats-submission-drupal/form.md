Project Name *
The name of the project. Abbreviations are fine too.

nostr-php
---
Project Description *
A great description will help us to evaluate your project more quickly.

Please open this Notion page where I worked out the full description of the project: https://sebastix.notion.site/OpenSats-application-for-Nostr-helper-library-for-PHP-8fb6bd7ac4214fbaaa397b8671170160

---
Potential Impact *
Why is this project important to Bitcoin or the broader free and open-source community?

Please read the Notion page here: https://sebastix.notion.site/OpenSats-application-for-Nostr-helper-library-for-PHP-8fb6bd7ac4214fbaaa397b8671170160

---
Project Timelines and Potential Milestones *
This will help us evaluate overall scope and potential grant duration.

There is a section called Deliverables / roadmap in the Notion page: https://sebastix.notion.site/OpenSats-application-for-Nostr-helper-library-for-PHP-8fb6bd7ac4214fbaaa397b8671170160.
I expect most work is finished for nostr-php after 6 months (allocated with 2 workdays a week) I will move over the focus to developing Nostr contrib modules for Drupal where the helper library is used as a package.
Building these modules with Drupal and is more complex than developing the helper library. I'm sure the remaining time for the asked grant duration I can fill up with a lot of development work on this.
More on how Drupal can benefit from a Nostr integration and vice versa, please read the section 'Looking forward: Nostr empowered Drupal initiative' on the Notion page =). 

---
Project Github (or similar, if applicable)

nostr-php helper library
https://github.com/swentel/nostr-php 
---
Project Budget
Costs & Proposed Budget *
Current or estimated costs of the project. If you're applying for a grant from the general fund, please submit a proposed budget around how much funding you are requesting and how it will be used.

€ 43.500 for one year development on nostr-php (I will allocate 2 days a week for this project)
aka ~9 000 000 sats a month
---
Applicant Details
Your Name *
Feel free to use your nym.

Sebastian Hagens
---
E-mail

info@sebastix.nl
---
Personal Github (or similar, if applicable)

Gitlab: https://gitlab.com/Sebastix 
Github: https://github.com/Sebastix
---
Other Contact Details (if applicable)
Please list any other relevant contact details you are comfortable sharing in case we need to reach out with questions. These could include nostr pubkeys, social media handles, emails, phone numbers, etc.

E-mail: info@sebastix.nl
Phone: +31 6 4171 2778
Telegram: @s3b0st1x
---
Prior Contributions
Please list any prior contributions to other open-source or Bitcoin-related projects. 

Drupal contribs: https://www.drupal.org/u/Sebastian%20Hagens/issue-credits and the modules I maintain are visible at https://www.drupal.org/u/sebastian-hagens
Other opensource projects you can find on my Gitlab: https://gitlab.com/sebastix-group or GitHub: https://github.com/Sebastix?tab=repositories&q=&type=source&language=&sort=
---
References *
Please list any references from the Bitcoin community or open-source space that we could contact for more information on you or your project.

Drupal: https://www.drupal.org/u/sebastian-hagens
https://github.com/Sebastix/crypto-dca
https://github.com/Sebastix/nostr-how
https://github.com/Sebastix/trtn.sebastix.dev

---
Open Sats may require each recipient to sign a Grant Agreement before any funds are disbursed. 
Using the reports and presentations required by the Grant Agreement, Open Sats will monitor and evaluate the expenditure of funds on a quarterly basis. 
Any apparent misuse of grant funds will be promptly investigated. 
If OpenSats discovers that the funds have been misused, the recipient will be required to return the funds immediately, and be barred from further distributions. 
Open Sats will maintain the records required by Revenue Ruling 56-304, 1956-2 C.B. 306 regarding distribution of charitable funds to individuals.