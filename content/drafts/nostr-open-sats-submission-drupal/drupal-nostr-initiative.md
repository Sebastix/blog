# Decentralizing content and publishing acceleration with PHP and Drupal

## *My submission for an OpenSats grant to start a Drupal Nostr initiative*

PHP is dead right? Long live PHP!  
If PHP was really terrible, 76.5% of all the websites wouldn't use it. PHP is almost 30 years old and is likely to stay here the next 30 years.
If Nostr would like to grow, you can't ignore PHP. Many platform are using PHP, with WordPress as a worldwide leader for websites and CMSs. The overall market share of WordPress is 42.8% ([w3techs](https://w3techs.com/technologies/details/cm-wordpress)). 

Drupal is at 1.1% of the overall market share of websites, but it's worth mentioning that Drupal is a strong leader of serving high traffic websites within the CMS market.

## Problem

What is the "problem"? Nostr needs integration in existing well-build, solid tools. Drupal is one of those tools where Nostr can really shine. If we look even broader, there are many existing PHP powered applications on the web which can benefit with a Nostr integration. 
Just look at all the WordPress powered websites alone, when those starting to integrate Nostr the adoption of the protocol and growth of the network will increase rapidly.

When there is a friendly Nostr integration for the many PHP developers and platform builder, this will create a springboard towards more Nostr adoption when more PHP powered applications will embrace Nostr.

**Most content is always centralized**

When content is published on a Drupal powered platform, it only exists there. With a Nostr integration the goal is to syndicate this content elsewhere on the Nostr network. This is called Publish (on your) Own Site, Syndicate Elsewhere (POSSE - source: https://indieweb.org/POSSE) aka [cross-posting](https://indieweb.org/cross-posting).

**Distributing content across platform is hard**

We had to use API's (with all the downsides, like permission) to distribute content across different platforms. With Nostr we don't need API's because interoperability is integrated on protocol level. This means we don't need permission to distribute content which can be read from any client. 

Drupal is able to handle many types of content thanks to the famous flexible fieldable entity model. When we combine to features of Nostr to distribute the content made in Drupal, the result could be

## This initiative

Why should we integrate Nostr in Drupal? For me Drupal stands for the traditional web. To let the adoption grow of Nostr, we need to integrate the protocol in the status-quo / traditional web.

We can use Drupal as a tool for achieving this. This also applies for other similiar FOSS CMS tools like WordPress, Joomla, TYPO3 or BackdropCMS.

Also Drupal is always looking for new ways to accelerate with new technology, so I’m convinced also the Drupal community will welcome Nostr with a open mindset.

**Inspired by the work of Swentel (who started to build nostr-php)**

In 2019 Swentel (Kristof) build several Drupal contrib modules for integrating ActivityPub and some [IndieWeb standards](https://spec.indieweb.org/). His work was partly funded with a NGI grant by NLnet foundation ([source](https://www.thedroptimes.com/feed/indigenous-ios-indieweb-and-activitypub-drupal)).

* https://realize.be/blog/personal-reader-drupal
* https://www.drupal.org/project/reader
* [IndieWeb | Drupal.org](https://www.drupal.org/project/indieweb) 
* https://www.drupal.org/project/activitypub 

**What have Drupal and Nostr in common?**

What does Nostr have in common with Drupal?
There is a strong critical community and both are open web champions.

**Come for the software, stay for the community**

A popular meme within the Drupal community.

**The open web manifesto of Drupal**

> The open web is more than a technology — it’s a cause.

[Open Web Manifesto | Drupal.org](https://www.drupal.org/association/open-web-manifesto)

In case you've missed it: Dries (founder of Drupal) is a fan of Nostr!
Read his article [Nostr, love at first sight](https://dri.es/nostr-love-at-first-sight).

**Freedom tech**

More than FOSS:

- Free from subscriptions
- Free from proprietary software dependencies
- Free with your own terms on conditions how you would like use Drupal
- Free to customize and modify

**Future of Drupal**

Mention some pivot points: Drupal 7 to Drupal 8 migration with the adoption of Symfony in core. This was also the moment were the love for Drupal began to increase. Downside was that Drupal was only usable for developers instead of people who like to build an awesome website.
Now we're at version 10 with Drupal and it's faster and easy-to-use than ever. But it's still not user-friendly enough to build your website without knowing how to write code.

This will change. And if core is not changing this, I will do it with building a recipe / distribution which will accomplish this.

See these strategic (core) and community initiatives where Drupal is heading:

[Strategic initiatives | TheDropTimes](https://www.thedroptimes.com/drupal/strategic-initiatives) [Community initiatives | TheDropTimes](https://www.thedroptimes.com/drupal/community-initiatives)

- Automated updates
- Marketing
- Project browser for more easy-to-install modules
- Distributions and recipes
- [API Client](https://www.thedroptimes.com/drupal/33325/drupal-api-client)With this package to connect your Javascript to your Drupal website: https://www.npmjs.com/package/@drupal-api-client/json-api-client
- [Single Directory Components](https://www.thedroptimes.com/35914/component-based-design-using-single-directory-components-sdc-in-drupal) Familiar approach for building frontend components for Javascript framework developers (React, Svelte, Vue)

My view on the future of Drupal is bright and optimistic. Drupal will pivot from an enterprise solution to an easy-to-use content managing tool. It will become be a WordPress competitor again, but with a lot more flexibility what you can do with it.

## Build for who?

- Online content creators and publishers who care about content ownership and their self-owned social graph
  Content creators should use something they own by theirselves. They should store their content they create on a safe, self-owned place which can be accesible by their own terms and conditions. 

- Business owners who're building a Nostr business
  With Drupal they can build the website they need with Nostr integrated. 

- Nostr communities 
  Community holders looking for 

- Drupal site builders & developers

- PHP framework developers (Symfony / Laravel / WordPress / BackdropCMS / NextCloud)
  They can use the nostr-php library in their project to integrate Nostr. 

- Nostr client developers

Some potential cases for a Nostr powered Drupal website:

* NostReport
* BitcoinBrabant webshop
  [] Mention the thread where a Nostr account can be used in the checkout and the benefits. 

## Deliverables / roadmap

I'm applying for a grant to cover development for one year. There a two main objectives is to build an ecosystem containing multiple modules to integrate Nostr in Drupal.

1. PHP helper library
2. Building a Drupal distribution with basic Nostr components 

**PHP helper library**

This library is used in every contrib module for Drupal. Because the library is build agnostic, it can be used by any PHP application.

- Github: [nostr-php](https://github.com/swentel/nostr-php) - see roadmap and at this moment I'm the only active maintainer
- Further development:
  - NIP-19
  - Reading data from relays

**Drupal distribution / recipe**

This will include all the configuration, a frontend theme and modules to get started with Drupal as a Nostr client. 

* Full user integration with Nostr keys
* Posting events from the Drupal CMS
* Support different forms of feedback (Nostr reaction events) on content 
* Full media support with NIP-94 and NIP-98 integration so Drupal can be used as a file hosting server for Nostr clients to make file sharing interoperable. 

###### The baseline for the distribution with these contrib modules

- nostr_auth  
  Register / login with your Nostr account. A new user entity will be created with your keys attached. The provided password will be used to encrypt sensitive data. At least one of the following submodules must be activated.  
  Submodules:
  
  - nostr_nsec
  
  - nostr_nip07
  
  - nostr_nip46

- nostr_content_nip23 (in development and has an alpha release)

- nostr_media  
  Provides NIP-94 and NIP-98 for your media entities. With this module you could provide a Nostr media service just like [nostr.build](https://nostr.build/) or similiar to 

- nostr_id_nip05 (in development and has an alpha release)
  Publish your markdown formatted content from Drupal to the Nostr network. Republishing (editing) is provided and saving the content as a draft as well. 

- nostr_notes (similiar to [nostr_simple_publish](https://www.drupal.org/project/nostr_simple_publish), in development and has an alpha release )

- nostr_reactions

- nostr_zap_button

- nostr_comments

- nostr_tags
  Drupal provides a solid way of building fieldable taxonomies which can be used as references to other content types. With this module you can map these taxonomies to Nostr tags which will be added to the events you're publishing. 

- zap_to_unlock
  Send a zap to view the full content. 

###### Nice to have (to be build later)

- nostr_ndk  
  This module will add the NDK to your current frontend theme.

- nostr_commerce  
  Crosspost your commerce content. This will also require certain commerce contrib module.

- nostr_dvm
  Module which provides functionality to interact with a data vending machine. 
  
  - nostr_dvm_image_field
  
  - nostr_dvm_translation

- nostr_zaps  
  A module for fetching zaps. View your sent and received zaps.

- nostr_resync  
  Republish a piece of content with an event to your chosen relays on the Nostr network. 

- nostr_index
  A module which put Nostr events in a index so they become searchable. Drupal provides a very robust and solid way of building search with the search api and solr. 

- nostr_geo
  Locations.

- nostr_events
  Events where other gather. 

- nostr_lists

- nostr_api  
  Provide and configure Nostr specific API endpoints through the JSON:API. 

- nostr_relay  
  Configure a relay written in PHP. All events will be available as Drupal entities.

- nostr_feeds
  Build your own feed with your own configured filters. 

- nostr_inbox
  A notification center with the latest activiteit around your events. 

- nostr_connect
  This module will provide a signer app and a interface for managing the registered accounts (NIP-46 integration). 

- nostr_storage

**Weekly reports**

On https://nostrver.se (build with Drupal) I will publish a weekly report. In this reports I'll share the achievements of last week and a plan with todo's to work on coming week.

When this content is created on the site, also events with kind 1 and 30023 will be published.

**Drupal community initiative**

One of the objectives is to get this initiative listed as a community initiative for generating exposure. I will send a proposal as documented [here](https://www.drupal.org/community-initiatives/proposing-initiatives).  
When approved, it will be listed on several places like https://www.drupal.org/community-initiatives and https://www.thedroptimes.com/drupal/community-initiatives. It's also to be expected the initiative will get attention on [DrupalCon events](https://www.drupal.org/association/drupalcon) worldwide. 
When declined, I will just start a contrib module Nostr Initative (similiar to [Drupal artificial intelligence (AI) community initiative](https://www.drupal.org/project/artificial_intelligence_initiative) for example).  

## The team

**Sebastian Hagens**

Sebastian Hagens, 37 years old living and working in The Netherlands as a fullstack webdeveloper and tech consultant. Father of two little daughters. Working 4 days in a week, self-employed from a small office in a old klooster. I have a set of loyal own clients but I also works as a freelancer from time to time. Started working with Drupal in 2014 and this tool became his most favorite tool for building solutions for ambitious and complex goals. Drupal is like a Swiss knife for building many types of web-based solutions.

We need more developers for this initative, so I already started some places where we can gather:

- Telegram group nostr-php

- Drupal Slack channel #nostr

## Other stuff and projects

These are some other project I'm working on as well, which are Nostr related in someway. 

##### Nuxstr

Starter template for building a Nostr client with the Nuxt Vue framework and Nostr Dev Kit. The main objectives of Nuxstr are:

1. Provide a starting point to make a Nostr client as a Vue / Nuxt developer with several examples with NDK.
2. Provide an easy to integrate and use Nostr register / login flow in a PWA.
3. Provide a way to use push notifications in a PWA.
4. Provide Vue components for the NDK project (similiar to the React and Svelte components).

##### RootApp

Based on a proof-of-concept I've developed in 2012. The new version will be a Drupal website where you can login with your Nostr account to get an overview with all your published Nostr events. I don;t know how open the API's are nowadays of the popular social media, but the original plan was to connect every popular social media. When connected, you could download all your data and view it in the app. A next step could be to map different fields with data of each social. In this way RootApp would create the option (interopability) to move your data between different socials (pull and push).  

###### nSafe

A Drupal powered website with a key signer app with an interface for managing your account / keys. This project is build around the NIP-46 Nostr Connect. 

This service could turn into a business for profit, providing a custodial service for managing Nostr accounts. When successfull, it would create a sustainable way of income with a subscription model. 

###### cchs.social

Turn this Drupal website into a fully Nostr based client / social platform. My goal is to build an online community with Honda car enthousiams (like me) inspired by the success of Stacker.news and with the learned lessons of building Proudwheels (a social media app). For Proudwheels I've done a lot of work as a product owner during development. A lot of people are creating carprofiles on Instagram and I would like to transform this into topics with a social "rating" system where people can hot with zaps (like) and comment on. At this moment I'm the only one who is posting links which reference to these Instagram profiles. 

Monthly requested fund for 1 year (12 months): € 5.250
